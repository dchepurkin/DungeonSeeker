//Created by Dmirtiy Chepurkin. All right reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class DungeonSeekerTarget : TargetRules
{
	public DungeonSeekerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_3;
		ExtraModuleNames.Add("DungeonSeeker");
	}
}
