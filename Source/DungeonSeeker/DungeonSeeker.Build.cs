//Created by Dmirtiy Chepurkin. All right reserved

using UnrealBuildTool;

public class DungeonSeeker : ModuleRules
{
	public DungeonSeeker(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "AIModule", "NavigationSystem", "AnimGraphRuntime", "AbilitySystem",
			"UMG", "Niagara", "NiagaraCore", "PhysicsCore"
		});

		PublicIncludePaths.AddRange(new string[]
		{
			"DungeonSeeker/",
			"DungeonSeeker/Public/",

			"DungeonSeeker/Public/InventorySystem/",

			"DungeonSeeker/Public/SaveGame/",
			"DungeonSeeker/Public/SaveGame/Interfaces/",

			"DungeonSeeker/Public/Ability/",
			"DungeonSeeker/Public/Actors/",

			"DungeonSeeker/Public/UI/",
			"DungeonSeeker/Public/UI/Widgets/",
			"DungeonSeeker/Public/UI/Widgets/Inventory/",
			"DungeonSeeker/Public/UI/Widgets/Gameplay/",

			"DungeonSeeker/Public/Utils/",
			"DungeonSeeker/Public/Controllers/",
			"DungeonSeeker/Public/Interfaces/",
			"DungeonSeeker/Public/Components/",
			"DungeonSeeker/Public/Core/",
			"DungeonSeeker/Public/Characters/",
		});
	}
}