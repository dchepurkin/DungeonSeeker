// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInteractionInterface.h"

#define LOCTEXT_NAMESPACE "InteractionInterface"

FText IDSInteractionInterface::GetInteractionActorName_Implementation()
{
	return LOCTEXT("DefaultInteractionActorName", "NoneName");
}

FText IDSInteractionInterface::GetInteractionName_Implementation()
{
	return LOCTEXT("DefaultInteractionName", "NoneName");
}

#undef LOCTEXT_NAMESPACE
