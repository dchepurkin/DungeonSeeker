// Created by Dmirtiy Chepurkin. All right reserved

#include "DSPlayerSaveData.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSPlayerSaveData, All, All);

bool UDSPlayerSaveData::Save(const FPlayerSaveData& InPlayerData)
{
    CharactersData.Emplace(InPlayerData.PlayerCharacterName, InPlayerData);
    return UGameplayStatics::SaveGameToSlot(this, DefaultCharacterDataSlotName, 0);
}

bool UDSPlayerSaveData::GetData(const FName& InCharacterName, FPlayerSaveData& OutData) const
{
    if (!CharactersData.Contains(InCharacterName)) { return false; }

    OutData = CharactersData[InCharacterName];
    return true;
}

bool UDSPlayerSaveData::Remove(const FName& InCharacterName)
{
    if (CharactersData.Contains(InCharacterName))
    {
        CharactersData.Remove(InCharacterName);
        CharactersData.CompactStable();
        return UGameplayStatics::SaveGameToSlot(this, DefaultCharacterDataSlotName, 0);
    }

    return false;
}

bool UDSPlayerSaveData::IsCharacterExist(const FName& InCharacterName) const
{
    return CharactersData.Contains(InCharacterName);
}

void UDSPlayerSaveData::GetSavedCharacters(TArray<FPlayerSaveData>& OutCharactersNames)
{
    for (const auto& [Name, Data] : CharactersData)
    {
        OutCharactersNames.Add(Data);
    }
}
