// Created by Dmirtiy Chepurkin. All right reserved

#include "DSHomeSaveData.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSHomeSaveData, All, All);

bool UDSHomeSaveData::Save(const FName& InCharacterName, const FHomeSaveData& InHomeData, const FAccountSaveData& InAccountData)
{
    HomesData.Add(InCharacterName, InHomeData);
    AccountData = InAccountData;
    return UGameplayStatics::SaveGameToSlot(this, DefaultHomeDataSlotName, 0);
}

bool UDSHomeSaveData::Remove(const FName& InCharacterName)
{
    if (HomesData.Contains(InCharacterName))
    {
        HomesData.Remove(InCharacterName);
        if (HomesData.IsEmpty())
        {
            AccountData = {};
        }
        return UGameplayStatics::SaveGameToSlot(this, DefaultCharacterDataSlotName, 0);
    }

    return false;
}

bool UDSHomeSaveData::GetHomeData(const FName& InCharacterName, FHomeSaveData& OutData)
{
    if (HomesData.Contains(InCharacterName))
    {
        OutData = HomesData[InCharacterName];
        return true;
    }

    return false;
}
