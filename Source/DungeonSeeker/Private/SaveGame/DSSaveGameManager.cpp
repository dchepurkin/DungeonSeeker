// Created by Dmirtiy Chepurkin. All right reserved

#include "DSSaveGameManager.h"

#include "DSAccountSavableActor.h"
#include "DSSavableActor.h"
#include "DSSavablePlayer.h"
#include "EngineUtils.h"
#include "Serialization/ObjectAndNameAsStringProxyArchive.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSSaveGameManager, All, All);

void UDSSaveGameManager::Initialize(FSubsystemCollectionBase& Collection)
{
    Super::Initialize(Collection);
    Manager = this;
}

bool UDSSaveGameManager::SaveGame()
{
    if (SavePlayerData() && SaveHomeData())
    {
        OnGameSaved.Broadcast();
        return true;
    }

    return false;
}

bool UDSSaveGameManager::RemoveSaveGame(const FName& InCharacterName)
{
    const auto SaveData = GetPlayerSaveData();
    return SaveData ? SaveData->Remove(InCharacterName) : false;
}

bool UDSSaveGameManager::SavePlayer()
{
    if (SavePlayerData())
    {
        OnGameSaved.Broadcast();
        return true;
    }
    return false;
}

bool UDSSaveGameManager::SaveHomeData()
{
    if (!GetHomeSaveData()) { return false; }

    const auto PlayerPawn = GetPlayerPawn();
    if (!(PlayerPawn && PlayerPawn->Implements<UDSSavablePlayer>())) { return false; }

    FHomeSaveData HomeData;
    FAccountSaveData AccountData;

    for (FActorIterator It(GetWorld()); It; ++It)
    {
        const auto Actor = *It;
        if (!IsValid(Actor)) continue;

        if (Actor->Implements<UDSSavableActor>())
        {
            FActorSaveData ActorSaveData;
            GetActorData(Actor, ActorSaveData);
            HomeData.Data.Add(ActorSaveData);
        }
        else if (Actor->Implements<UDSAccountSavableActor>())
        {
            FActorSaveData ActorSaveData;
            GetActorData(Actor, ActorSaveData);
            AccountData.Data.Add(ActorSaveData);
        }
    }

    return GetHomeSaveData()->Save(IDSSavablePlayer::Execute_GetCharacterName(PlayerPawn), HomeData, AccountData);
}

void UDSSaveGameManager::LoadGame(const FName& InCharacterName)
{
    LoadedObjects.Empty();

    LoadPlayerData(InCharacterName);
    LoadHomeData(InCharacterName);
    DispatchOnGameLoaded();
}

void UDSSaveGameManager::LoadPlayer(const FName& InCharacterName)
{
    LoadedObjects.Empty();

    LoadPlayerData(InCharacterName);
    DispatchOnGameLoaded();
}

void UDSSaveGameManager::GetSavedCharacters(TArray<FPlayerSaveData>& OutCharactersData) const
{
    if (const auto SaveData = GetPlayerSaveData())
    {
        SaveData->GetSavedCharacters(OutCharactersData);
    }
}

bool UDSSaveGameManager::IsCharacterExist(const FName& InCharacterName) const
{
    const auto SaveData = GetPlayerSaveData();
    return SaveData ? SaveData->IsCharacterExist(InCharacterName) : false;
}

void UDSSaveGameManager::GetPlayerData(APawn* InPawn, FPlayerSaveData& OutData)
{
    if (InPawn && InPawn->Implements<UDSSavablePlayer>())
    {
        GetByteData(InPawn, OutData);
        GetComponentsData(InPawn, OutData);

        OutData.PlayerCharacterName = IDSSavablePlayer::Execute_GetCharacterName(InPawn);
    }
}

void UDSSaveGameManager::GetActorData(AActor* InActor, FActorSaveData& OutData)
{
    GetByteData(InActor, OutData);
    GetComponentsData(InActor, OutData);

    OutData.Class = InActor->GetClass();
    OutData.Name = InActor->GetFName();
    OutData.Transform = InActor->GetActorTransform();
}

bool UDSSaveGameManager::SavePlayerData()
{
    if (!GetPlayerSaveData()) { return false; }

    if (const auto PlayerPawn = GetPlayerPawn())
    {
        FPlayerSaveData Data;
        GetPlayerData(PlayerPawn, Data);

        return GetPlayerSaveData()->Save(Data);
    }

    return false;
}

void UDSSaveGameManager::LoadPlayerData(const FName& InCharacterName)
{
    if (!GetPlayerSaveData()) { return; }

    if (const auto PlayerPawn = GetPlayerPawn(); PlayerPawn->Implements<UDSSavablePlayer>())
    {
        if (FPlayerSaveData Data; GetPlayerSaveData()->GetData(InCharacterName, Data))
        {
            LoadByteData(PlayerPawn, Data);
            LoadComponentsData(PlayerPawn, Data);

            LoadedObjects.Add(PlayerPawn);
            IDSSavablePlayer::Execute_OnPlayerLoaded(PlayerPawn);
        }
    }
}

void UDSSaveGameManager::LoadHomeData(const FName& InCharacterName)
{
    if (!GetHomeSaveData()) { return; }

    FHomeSaveData HomeData;
    if (!GetHomeSaveData()->GetHomeData(InCharacterName, HomeData)) { return; }

    const FAccountSaveData& AccountData = GetHomeSaveData()->GetAccountData();

    TArray<AActor*> SavableActors;

    for (FActorIterator It(GetWorld()); It; ++It)
    {
        const auto Actor = *It;
        if (!IsValid(Actor)) continue;

        if (Actor->Implements<UDSSavableActor>() || Actor->Implements<UDSAccountSavableActor>())
        {
            SavableActors.Add(Actor);
        }
    }

    //��������� ��������� ���� �������
    LoadActors(SavableActors, HomeData.Data);
    LoadActors(SavableActors, AccountData.Data);

    //���� ����� �������� � ������� �������� ������ �� ������� ��
    for (const auto Actor : SavableActors)
    {
        Actor->Destroy();
    }
}

void UDSSaveGameManager::LoadActors(TArray<AActor*>& AllActors, const TArray<FActorSaveData>& SaveData)
{
    for (const auto& Data : SaveData)
    {
        auto FoundActor = AllActors.FindByPredicate([&Data](const AActor* InActor)
        {
            return InActor->GetFName() == Data.Name;
        });

        AActor* LoadedActor = nullptr;
        if (FoundActor) //���� ������ ����� �� ����� �� ������� ��� �� �������
        {
            LoadedActor = *FoundActor;
            AllActors.RemoveSingle(*FoundActor);
        }
        else //���� � ����������� ���� ����� �������� ��� �� ����� �� ������� ���
        {
            LoadedActor = GetWorld()->SpawnActor(Data.Class);
        }

        LoadActorData(LoadedActor, Data);
    }
}

void UDSSaveGameManager::LoadActorData(AActor* InActor, const FActorSaveData& InData)
{
    if (!InActor) return;

    LoadByteData(InActor, InData);
    LoadComponentsData(InActor, InData);
    InActor->SetActorTransform(InData.Transform);

    LoadedObjects.Add(InActor);

    if (InActor->Implements<UDSSavableActor>()) { IDSSavableActor::Execute_OnActorLoaded(InActor); }
    else if (InActor->Implements<UDSAccountSavableActor>()) { IDSAccountSavableActor::Execute_OnActorLoaded(InActor); }
}

void UDSSaveGameManager::GetByteData(UObject* InObject, FSaveData& OutData) const
{
    FMemoryWriter MemoryWriter{OutData.ByteData};
    FObjectAndNameAsStringProxyArchive Archive{MemoryWriter, true};
    Archive.ArIsSaveGame = true;

    InObject->Serialize(Archive);
}

void UDSSaveGameManager::LoadByteData(UObject* InObject, const FSaveData& InData) const
{
    FMemoryReader MemoryReader(InData.ByteData);
    FObjectAndNameAsStringProxyArchive Archive(MemoryReader, true);
    Archive.ArIsSaveGame = true;

    InObject->Serialize(Archive);
}

void UDSSaveGameManager::DispatchOnGameLoaded()
{
    for (const auto Object : LoadedObjects)
    {
        if (Object && Object->Implements<UDSSavableObject>())
        {
            IDSSavableObject::Execute_OnGameLoaded(Object);
        }
    }

    OnGameLoaded.Broadcast();
}

APawn* UDSSaveGameManager::GetPlayerPawn() const
{
    const auto World = GetWorld();
    if (!World) { return nullptr; }

    const auto PlayerController = World->GetFirstPlayerController();
    return PlayerController ? PlayerController->GetPawn() : nullptr;
}
