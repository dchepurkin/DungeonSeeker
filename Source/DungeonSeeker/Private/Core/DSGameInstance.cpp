// Created by Dmirtiy Chepurkin. All right reserved

#include "Core/DSGameInstance.h"
#include "DSHUD.h"
#include "DSSaveGameManager.h"
#include "Kismet/GameplayStatics.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSGameInstance, All, All);

void UDSGameInstance::Init()
{
    Super::Init();

    FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &ThisClass::PreLoadMap_Callback);
    FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &ThisClass::PostLoadMap_Callback);

    /*if(const auto Settings = UGameUserSettings::GetGameUserSettings())
    {
        Settings->SetFullscreenMode(Settings->GetFullscreenMode() == EWindowMode::Windowed ? EWindowMode::Windowed : EWindowMode::Fullscreen);
    }*/
}

void UDSGameInstance::StartGame(const FName& InCharacterName)
{
    if (InCharacterName.IsNone()) { return; }

    SetCharacterName(InCharacterName);
    OpenLevel(HomeLevelName);
}

void UDSGameInstance::SetCharacterName(const FName& InName)
{
    CharacterName = InName;
}

void UDSGameInstance::OpenLevel(const FName& InLevelName)
{
    if (InLevelName.IsNone()) { return; }

    if (const auto HUD = GetHUD())
    {
        HUD->ShowLoadingScreen(GetLoadingScreenImage(InLevelName));
        HUD->OnLoadingScreenFinished.AddLambda([this, InLevelName]() { UGameplayStatics::OpenLevel(this, InLevelName); });
    }
}

UTexture2D* UDSGameInstance::GetLoadingScreenImage(const FName& MapName) const
{
    if (!LoadingScreensInfoDataTable)
    {
        UE_LOG(LogDSGameInstance, Error, TEXT("LoadingScreensInfoDataTable is NULL"));
        return nullptr;
    }

    const auto FoundRow = LoadingScreensInfoDataTable->FindRow<FLoadingScreenInfo>(MapName, "");
    return FoundRow ? FoundRow->LoadingScreenImage : nullptr;
}

void UDSGameInstance::PreLoadMap_Callback(const FString& String)
{
    if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
    {
        //���� ���� �� ��������� ��, � ��������� ������� ������ ������
        UGameplayStatics::GetCurrentLevelName(this) == HomeLevelName ? SaveGameManager->SaveGame() : SaveGameManager->SavePlayer();
    };
}

void UDSGameInstance::PostLoadMap_Callback(UWorld* World)
{
    if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
    {
        SaveGameManager->LoadGame(CharacterName);
    }
}

ADSHUD* UDSGameInstance::GetHUD() const
{
    const auto PlayerController = GetFirstLocalPlayerController();
    return PlayerController ? PlayerController->GetHUD<ADSHUD>() : nullptr;
}
