// Created by Dmirtiy Chepurkin. All right reserved

#include "DSTypes.h"

const FDSBodyPartParams FDSBodyPartParams::ShowAll{false, false, false, false};
const FDSBodyPartParams FDSBodyPartParams::HideAll{true, true, true, true};
const FDSBodyPartParams FDSBodyPartParams::HideHairOnly{false, false, false, true};
const FDSBodyPartParams FDSBodyPartParams::HideBeardOnly{false, false, true, false};
const FDSBodyPartParams FDSBodyPartParams::HideHairAndBeard{false, false, true, true};
