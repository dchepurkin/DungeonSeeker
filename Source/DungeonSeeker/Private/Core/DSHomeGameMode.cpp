// Created by Dmirtiy Chepurkin. All right reserved

#include "Core/DSHomeGameMode.h"
#include "DSPlayerController.h"
#include "DSPlayerCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSHomeGameMode, All, All);

ADSHomeGameMode::ADSHomeGameMode()
{
	DefaultPawnClass = ADSPlayerCharacter::StaticClass();
	PlayerControllerClass = ADSPlayerController::StaticClass();
}
