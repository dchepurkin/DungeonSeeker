//Created by Dmirtiy Chepurkin. All right reserved

#include "DSGameModeBase.h"
#include "DSSaveGameManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSGameModeBase, All, All);

ADSGameModeBase::ADSGameModeBase()
{
    PrimaryActorTick.bTickEvenWhenPaused = true;
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = false;
}

void ADSGameModeBase::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    TimerManager.Tick(DeltaSeconds);
}

void ADSGameModeBase::SaveGame(bool SavePlayerOnly)
{
    SetActorTickEnabled(true);
    TimerManager.SetTimer(SaveGameTimerHandle, [this, SavePlayerOnly]()
    {
        if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
        {
            SavePlayerOnly ? SaveGameManager->SavePlayer() : SaveGameManager->SaveGame();
            SetActorTickEnabled(false);
        }
    }, SaveGameDelay, false);
}
