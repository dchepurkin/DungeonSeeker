// Created by Dmirtiy Chepurkin. All right reserved

#include "Characters/DSPlayerCharacter.h"

#include "DSAbilityComponent.h"
#include "DSCharacterMovementComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "DSCameraComponent.h"
#include "DSInteractionComponent.h"
#include "DSPlayerInventoryComponent.h"
#include "DSRotationComponent.h"
#include "NavigationInvokerComponent.h"
#include "Algo/ForEach.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSPlayerCharacter, All, All);

ADSPlayerCharacter::ADSPlayerCharacter(const FObjectInitializer& Initializer)
    : Super(Initializer.SetDefaultSubobjectClass<UDSCharacterMovementComponent>(CharacterMovementComponentName))
{
    GetCapsuleComponent()->InitCapsuleSize(35.f, 89.0f);
    GetCapsuleComponent()->SetCollisionProfileName("Player");

    BaseEyeHeight = 70.f;

    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    CameraBoom = CreateDefaultSubobject<USpringArmComponent>("CameraBoom");
    CameraBoom->SetupAttachment(GetRootComponent());
    CameraBoom->TargetArmLength = 750.f;
    CameraBoom->SetRelativeLocation(FVector(0.f, 0.f, 80.f));
    CameraBoom->bUsePawnControlRotation = true;
    CameraBoom->bEnableCameraLag = true;
    CameraBoom->CameraLagSpeed = 20.f;
    CameraBoom->bEnableCameraRotationLag = true;
    CameraBoom->CameraRotationLagSpeed = 15.f;
    CameraBoom->SocketOffset = FVector(0.f, 0.f, 100.f);

    FollowCamera = CreateDefaultSubobject<UDSCameraComponent>("FollowCamera");
    FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    FollowCamera->SetRelativeRotation(FRotator(-5.f, 0.f, 0.f));
    FollowCamera->bUsePawnControlRotation = false;
    FollowCamera->FieldOfView = 75.f;

    NavigationInvokerComponent = CreateDefaultSubobject<UNavigationInvokerComponent>("NavigationInvokerComponent");
    NavigationInvokerComponent->SetGenerationRadii(600.f, 600.f);

    InteractionComponent = CreateDefaultSubobject<UDSInteractionComponent>("InteractionComponent");
    InteractionComponent->SetupAttachment(GetRootComponent());
    InteractionComponent->SetRelativeLocation(FVector(70.f, 0.f, -86.f));
    InteractionComponent->SetSphereRadius(70.f);
    InteractionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

    InventoryComponent = CreateDefaultSubobject<UDSPlayerInventoryComponent>("InventoryComponent");

    GetMesh()->SetRelativeLocation(FVector{0.f, 0.f, -89.f});
    GetMesh()->SetRelativeRotation(FRotator{0.f, -90.f, 0.f});

    Algo::ForEach(BodyPartsMeshes, [](USkeletalMeshComponent* MeshComponent) { MeshComponent->SetCollisionProfileName("PlayerMesh"); });
}

void ADSPlayerCharacter::BeginPlay()
{
    Super::BeginPlay();

    SetInputMapping(DefaultMappingContext);

    if (FollowCamera)
    {
        FollowCamera->OnCameraBeginOverlap.AddUObject(this, &ThisClass::OnCameraBeginOverlap_Callback);
        FollowCamera->OnCameraEndOverlap.AddUObject(this, &ThisClass::OnCameraEndOverlap_Callback);
    }

    if (InteractionComponent)
    {
        //������������ ��������� � ���� ��������������
        InteractionComponent->OnInteractionStarted.AddDynamic(RotationComponent, &UDSRotationComponent::RotateToActor);
    }

    if (AbilityComponent)
    {
        AbilityComponent->OnAbilityStarted.AddDynamic(this, &ThisClass::OnAbilityStarted);
    }
}

void ADSPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    if (const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
    {
        EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ThisClass::Jump);

        EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Started, this, &ThisClass::StartMoving);
        EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::Move);

        EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ThisClass::Look);

        EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &ThisClass::InteractionInput);

        EnhancedInputComponent->BindAction(InventoryAction, ETriggerEvent::Started, this, &ThisClass::InventoryInput);

        EnhancedInputComponent->BindAction(CancelAction, ETriggerEvent::Started, this, &ThisClass::CancelInput);
    }
}

void ADSPlayerCharacter::Move(const FInputActionValue& Value)
{
    if (!Controller) { return; }

    FVector2D MovementVector = Value.Get<FVector2D>();

    const FRotator Rotation = Controller->GetControlRotation();
    const FRotator YawRotation(0, Rotation.Yaw, 0);

    const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

    const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

    AddMovementInput(ForwardDirection, MovementVector.Y);
    AddMovementInput(RightDirection, MovementVector.X);
}

void ADSPlayerCharacter::Look(const FInputActionValue& Value)
{
    if (!Controller) { return; }

    FVector2D LookAxisVector = Value.Get<FVector2D>();

    AddControllerYawInput(LookAxisVector.X);
    AddControllerPitchInput(LookAxisVector.Y);
}

void ADSPlayerCharacter::OnCameraBeginOverlap_Callback()
{
    if (GetMesh()) GetMesh()->SetVisibility(false, true);
}

void ADSPlayerCharacter::OnCameraEndOverlap_Callback()
{
    if (GetMesh()) GetMesh()->SetVisibility(true, true);
}

void ADSPlayerCharacter::Jump()
{
    if (StopAllActions())
    {
        Super::Jump();
    }
}

void ADSPlayerCharacter::StartMoving()
{
    if (StopAllActions())
    {
    }
}

void ADSPlayerCharacter::OnAbilityStarted(UDSAbilityBase* InAbility, AActor* InAbilityTarget)
{
    //������������ ��������� � ���� ������
    if (RotationComponent && !RotationComponent->IsRotated())
    {
        RotationComponent->RotateToActor(InAbilityTarget);
    }
}

void ADSPlayerCharacter::InteractionInput()
{
    if (CanInteract())
    {
        InteractionComponent->Interact();
    }
}

bool ADSPlayerCharacter::StopAllActions() const
{
    if (InteractionComponent)
    {
        InteractionComponent->StopInteraction();
    }

    return AbilityComponent && AbilityComponent->InterruptAbility() != EInterruptAbilityResult::CantInterrupt;
}

void ADSPlayerCharacter::CancelInput()
{
    if (StopAllActions())
    {
        //todo show mainmenu
    }
}

void ADSPlayerCharacter::InventoryInput()
{
    StopAllActions();
    OnSwitchToUI.Broadcast(EDSUIType::Inventory);
}

bool ADSPlayerCharacter::CanInteract() const
{
    return !GetCharacterMovement()->IsFalling()
        && !InteractionComponent->IsInteracted()
        && !AbilityComponent->IsActiveAbility();
}

void ADSPlayerCharacter::SetCharacterName(const FName& InName)
{
    CharacterName = InName;
}
