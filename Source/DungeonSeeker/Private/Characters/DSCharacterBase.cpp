//Created by Dmirtiy Chepurkin. All right reserved

#include "DSCharacterBase.h"

#include "DSAbilityComponent.h"
#include "DSRotationComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCharacterBase, All, All);

ADSCharacterBase::ADSCharacterBase(const FObjectInitializer& Initializer)
    : Super(Initializer)
{
    PrimaryActorTick.bCanEverTick = false;
    PrimaryActorTick.bStartWithTickEnabled = false;

    AbilityComponent = CreateDefaultSubobject<UDSAbilityComponent>("AbilityComponent");

    RotationComponent = CreateDefaultSubobject<UDSRotationComponent>("RotationComponent");
}

void ADSCharacterBase::BeginPlay()
{
    Super::BeginPlay();
}
