// Created by Dmirtiy Chepurkin. All right reserved

#include "Characters/DSSavableModularCharacter.h"
#include "DSConstants.h"
#include "Data/DSCharacterCustomization.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSSavableModularCharacter, All, All);

void ADSSavableModularCharacter::SetGender(EDSGender InGender)
{
    Gender = InGender;
    if (const auto CharacterCustomization = GetCharacterCustomization())
    {
        const auto& DefaultBodyParts = CharacterCustomization->GetDefaultBodyParts(Gender);
        for (const auto& BodyPart : DefaultBodyParts)
        {
            SetBodyPart(BodyPart, FDSBodyPartParams::ShowAll);
        }
    }
}

void ADSSavableModularCharacter::SetHead(int32 HeadIndex)
{
    if (HeadIndex < 0) { return; }

    if (const auto CharacterCustomization = GetCharacterCustomization())
    {
        DefaultHeadIndex = HeadIndex;
        HeadMesh->SetSkeletalMesh(CharacterCustomization->GetHead(Gender, DefaultHeadIndex));
    }
}

void ADSSavableModularCharacter::SetEyebrow(int32 EyebrowIndex)
{
    if (EyebrowIndex < 0) { return; }

    if (const auto CharacterCustomization = GetCharacterCustomization())
    {
        DefaultEyebrowIndex = EyebrowIndex;
        EyebrowMesh->SetSkeletalMesh(CharacterCustomization->GetEyebrow(Gender, DefaultEyebrowIndex));
    }
}

void ADSSavableModularCharacter::SetHair(int32 HairIndex)
{
    if (HairIndex < 0) { return; }

    if (const auto CharacterCustomization = GetCharacterCustomization())
    {
        DefaultHairIndex = HairIndex;
        const bool SetColor = !HairMesh->GetSkeletalMeshAsset();
        HairMesh->SetSkeletalMesh(CharacterCustomization->GetHair(Gender, DefaultHairIndex));
        if (SetColor)
        {
            HairMesh->SetVectorParameterValueOnMaterials(HairColorParamName, FVector{DefaultHairColor});
        }
    }
}

void ADSSavableModularCharacter::SetBeard(int32 BeardIndex)
{
    if (BeardIndex < 0) { return; }

    if (const auto CharacterCustomization = GetCharacterCustomization())
    {
        DefaultBeardIndex = BeardIndex;
        const bool SetColor = !BeardMesh->GetSkeletalMeshAsset();
        BeardMesh->SetSkeletalMesh(CharacterCustomization->GetBeard(Gender, DefaultBeardIndex));
        if (SetColor)
        {
            BeardMesh->SetVectorParameterValueOnMaterials(HairColorParamName, FVector{DefaultHairColor});
        }
    }
}

void ADSSavableModularCharacter::SetHairColor(const FLinearColor& InColor)
{
    DefaultHairColor = InColor;
    const FVector ColorVector{DefaultHairColor};
    HairMesh->SetVectorParameterValueOnMaterials(HairColorParamName, ColorVector);
    BeardMesh->SetVectorParameterValueOnMaterials(HairColorParamName, ColorVector);
    EyebrowMesh->SetVectorParameterValueOnMaterials(HairColorParamName, ColorVector);
}

void ADSSavableModularCharacter::SetScarColor(const FLinearColor& InColor)
{
    DefaultScarColor = InColor;
    const FVector ColorVector{DefaultScarColor};
    HeadMesh->SetVectorParameterValueOnMaterials(ScarColorParamName, ColorVector);
}

void ADSSavableModularCharacter::SetBodyArtColor(const FLinearColor& InColor)
{
    DefaultBodyArtColor = InColor;
    const FVector ColorVector{DefaultBodyArtColor};
    HeadMesh->SetVectorParameterValueOnMaterials(BodyArtColorParamName, ColorVector);
}

void ADSSavableModularCharacter::SetSkinColor(const FLinearColor& InSkinColor, const FLinearColor& InStubbleColor)
{
    DefaultSkinColor = InSkinColor;
    DefaultStubbleColor = InStubbleColor;
    const FVector SkinColorVector{DefaultSkinColor};
    const FVector StubbleColorVector{DefaultStubbleColor};

    for (const auto BodyPartMesh : GetBodyParts())
    {
        BodyPartMesh->SetVectorParameterValueOnMaterials(SkinColorParamName, SkinColorVector);
        BodyPartMesh->SetVectorParameterValueOnMaterials(StubbleColorParamName, StubbleColorVector);
    }
}

void ADSSavableModularCharacter::OnPlayerLoaded_Implementation()
{
    SetGender(Gender);

    SetHead(DefaultHeadIndex);
    SetEyebrow(DefaultEyebrowIndex);
    SetBeard(DefaultBeardIndex);
    SetHair(DefaultHairIndex);

    SetHairColor(DefaultHairColor);
    SetScarColor(DefaultScarColor);
    SetBodyArtColor(DefaultBodyArtColor);
    SetSkinColor(DefaultSkinColor, DefaultStubbleColor);
}
