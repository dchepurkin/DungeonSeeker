// Created by Dmirtiy Chepurkin. All right reserved

#include "Characters/DSModularCharacter.h"

#include "DSGameInstance.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "Algo/ForEach.h"
#include "Data/DSCharacterCustomization.h"
#include "DSConstants.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSModularCharacter, All, All);

ADSModularCharacter::ADSModularCharacter(const FObjectInitializer& Initializer)
    : Super(Initializer)
{
    //������ ����� ��������� �������� �������� ��� ��� ���� �������� ��������������� �������� ��� �������� �����
    GetMesh()->ComponentTags.Add(CostumeTag);
    GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;

    HelmMesh = CreateBodyPartMesh("HelmMesh", HelmTag);
    HeadMesh = CreateBodyPartMesh("HeadMesh", HeadTag);
    HairMesh = CreateBodyPartMesh("HairMesh", HairTag);
    BeardMesh = CreateBodyPartMesh("BeardMesh", BeardTag);
    EyebrowMesh = CreateBodyPartMesh("EyebrowMesh", EyebrowTag);
    HandsMesh = CreateBodyPartMesh("HandsMesh", HandsTag);
    TorsMesh = CreateBodyPartMesh("TorsMesh", TorsTag);
    LegsMesh = CreateBodyPartMesh("LegsMesh", LegsTag);
    ShouldersMesh = CreateBodyPartMesh("ShouldersMesh", ShouldersTag);

    BodyPartsMeshes.Add(GetMesh());
}

void ADSModularCharacter::BeginPlay()
{
    Super::BeginPlay();
    GetMesh()->SetHiddenInGame(true);
}

USkeletalMeshComponent* ADSModularCharacter::CreateBodyPartMesh(const FName& InBodyPartName, const FName& InTag)
{
    const auto BodyPartMesh = CreateDefaultSubobject<USkeletalMeshComponent>(InBodyPartName);
    BodyPartMesh->SetupAttachment(GetMesh());
    BodyPartMesh->SetLeaderPoseComponent(GetMesh());
    BodyPartMesh->ComponentTags.Add(InTag);

    BodyPartsMeshes.Add(BodyPartMesh);

    return BodyPartMesh;
}

void ADSModularCharacter::SetBodyPart(const FDSBodyPart& InBodyPart, const FDSBodyPartParams& Params)
{
    if (InBodyPart.Tag == CostumeTag)
    {
        SetCostume(InBodyPart.Mesh, Params);
    }
    else if (InBodyPart.Tag == HelmTag)
    {
        SetHelm(InBodyPart.Mesh, Params);
    }
    else
    {
        /*if (!InBodyPart.Mesh)
        {
            //TODO SetDefault Mesh
            return;
        }*/

        const auto FoundBodyPartPtr = GetBodyParts().FindByPredicate([&InBodyPart](USkeletalMeshComponent* MeshComponent)
        {
            return MeshComponent && MeshComponent->ComponentTags.Contains(InBodyPart.Tag);
        });

        if (FoundBodyPartPtr)
        {
            if (const auto FoundBodyPart = *FoundBodyPartPtr)
            {
                (*FoundBodyPartPtr)->SetSkeletalMesh(InBodyPart.Mesh);
            }
        }
    }


    OnChangeBodyPart.Broadcast(InBodyPart, Params);
}

void ADSModularCharacter::SetCostume(USkeletalMesh* CostumeMesh, const FDSBodyPartParams& Params)
{
    const auto bInCostume = CostumeMesh != nullptr;
    CostumeParams = bInCostume ? Params : FDSBodyPartParams{};
    if (bInCostume) { GetMesh()->SetSkeletalMesh(CostumeMesh); }
    GetMesh()->SetHiddenInGame(!bInCostume);

    UpdateHeadHidden();

    Algo::ForEach(BodyPartsMeshes, [this, bInCostume](USkeletalMeshComponent* InMesh)
    {
        if (IsCostumeComponent(InMesh) || IsHeadComponent(InMesh)) { return; }
        InMesh->SetHiddenInGame(bInCostume);
    });
}

void ADSModularCharacter::SetHelm(USkeletalMesh* InMesh, const FDSBodyPartParams& Params)
{
    HelmMesh->SetSkeletalMesh(InMesh);
    HelmParams = InMesh ? Params : FDSBodyPartParams{};

    UpdateHeadHidden();
}

void ADSModularCharacter::UpdateHeadHidden()
{
    HeadMesh->SetHiddenInGame(CostumeParams.bHideHead || HelmParams.bHideHead);
    EyebrowMesh->SetHiddenInGame(CostumeParams.bHideEyebrow || HelmParams.bHideEyebrow);
    BeardMesh->SetHiddenInGame(CostumeParams.bHideBeard || HelmParams.bHideBeard);
    HairMesh->SetHiddenInGame(CostumeParams.bHideHair || HelmParams.bHideHair);
}

bool ADSModularCharacter::IsCostumeComponent(USkeletalMeshComponent* InMeshComp)
{
    return InMeshComp && InMeshComp->ComponentTags.Contains(CostumeTag);
}

bool ADSModularCharacter::IsHeadComponent(USkeletalMeshComponent* InMeshComp)
{
    return InMeshComp && (InMeshComp->ComponentTags.Contains(HeadTag)
        || InMeshComp->ComponentTags.Contains(EyebrowTag)
        || InMeshComp->ComponentTags.Contains(BeardTag)
        || InMeshComp->ComponentTags.Contains(HairTag));
}

UDSCharacterCustomization* ADSModularCharacter::GetCharacterCustomization() const
{
    if (!CharacterCustomization_Cache)
    {
        const auto GameInstance = GetGameInstance<UDSGameInstance>();
        CharacterCustomization_Cache = GameInstance ? GameInstance->GetCharacterCustomization() : nullptr;
    }

    return CharacterCustomization_Cache;
}

void ADSModularCharacter::SetInputMapping(UInputMappingContext* InMappingContext) const
{
    if (const auto PlayerController = GetController<APlayerController>())
    {
        if (const auto InputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
        {
            InputSubsystem->ClearAllMappings();
            InputSubsystem->AddMappingContext(InMappingContext, 0);
        }
    }
}


