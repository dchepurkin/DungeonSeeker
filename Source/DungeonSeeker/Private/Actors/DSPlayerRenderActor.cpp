// Created by Dmirtiy Chepurkin. All right reserved

#include "Actors/DSPlayerRenderActor.h"

#include "Components/SceneCaptureComponent2D.h"
#include "Components/SpotLightComponent.h"
#include "GameFramework/SpringArmComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSPlayerRenderActor, All, All);

ADSPlayerRenderActor::ADSPlayerRenderActor(const FObjectInitializer& Initializer)
    : Super(Initializer)
{
    PrimaryActorTick.bCanEverTick = false;
    PrimaryActorTick.bStartWithTickEnabled = false;

    GetMesh()->SetTickableWhenPaused(true);

    SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
    SpringArm->SetupAttachment(GetRootComponent());
    SpringArm->SetRelativeLocation(FVector{0.f, 0.f, -10.f});
    SpringArm->SetRelativeRotation(FRotator{-10.f, -110.f, 0.f});
    SpringArm->TargetArmLength = 300.f;
    SpringArm->SocketOffset = FVector{0.f, 0.f, 20.f};

    CaptureComponent = CreateDefaultSubobject<USceneCaptureComponent2D>("CaptureComponent");
    CaptureComponent->SetupAttachment(SpringArm);
    CaptureComponent->FOVAngle = 45.f;
    CaptureComponent->PrimitiveRenderMode = ESceneCapturePrimitiveRenderMode::PRM_RenderScenePrimitives;
    CaptureComponent->SetTickableWhenPaused(true);

    SpotLight = CreateDefaultSubobject<USpotLightComponent>("SpotLight");
    SpotLight->SetupAttachment(SpringArm);
    SpotLight->SetRelativeLocation(FVector{100.f, 0.f, 0.f});
    SpotLight->CastShadows = false;
    SpotLight->AttenuationRadius = 400.f;
}

void ADSPlayerRenderActor::BeginPlay()
{
    Super::BeginPlay();

    if (!CaptureComponent->TextureTarget)
    {
        UE_LOG(LogDSPlayerRenderActor, Warning, TEXT("Target render texture is NULL"));
    }

    if (!GetWorld()) return;

    if (const auto PlayerController = GetWorld()->GetFirstPlayerController())
    {
        PlayerCharacter = PlayerController->GetPawn<ADSModularCharacter>();
        if (PlayerCharacter)
        {
            PlayerCharacter->OnChangeBodyPart.AddDynamic(this, &ThisClass::OnChangeBodyPart_Callback);
            UpdateBodyParts(PlayerCharacter->GetBodyParts());
        }
    }
}

void ADSPlayerRenderActor::UpdateBodyParts(const TArray<USkeletalMeshComponent*>& PlayerBodyParts)
{
    for (const auto SKMeshComp : BodyPartsMeshes)
    {
        if (SKMeshComp->ComponentTags.IsEmpty()) { continue; }

        const auto Tag = SKMeshComp->ComponentTags[0];

        const auto FoundPlayerMeshPtr = PlayerBodyParts.FindByPredicate([&Tag](const USkeletalMeshComponent* SKMesh)
        {
            return SKMesh->ComponentTags.Contains(Tag);
        });

        if (FoundPlayerMeshPtr)
        {
            if (const auto FoundPlayerMesh = *FoundPlayerMeshPtr)
            {
                SKMeshComp->SetSkeletalMesh(FoundPlayerMesh->GetSkeletalMeshAsset());
                SKMeshComp->SetMaterial(0, FoundPlayerMesh->GetMaterial(0));
                SKMeshComp->SetHiddenInGame(FoundPlayerMesh->bHiddenInGame);
            }
        }
    }
}

void ADSPlayerRenderActor::OnChangeBodyPart_Callback(const FDSBodyPart& InBodyPart, const FDSBodyPartParams& Params)
{
    UpdateBodyParts(PlayerCharacter->GetBodyParts());
}
