// Created by Dmirtiy Chepurkin. All right reserved

#include "Actors/DSCustomizeCharacterActor.h"

#include "EnhancedInputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCustomizeCharacterActor, All, All);

ADSCustomizeCharacterActor::ADSCustomizeCharacterActor(const FObjectInitializer& Initializer)
    : Super(Initializer)
{
    GetCapsuleComponent()->InitCapsuleSize(35.f, 89.0f);

    GetMesh()->SetRelativeLocation(FVector{0.f, 0.f, -89.f});
    GetMesh()->SetRelativeRotation(FRotator{0.f, -90.f, 0.f});

    SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
    SpringArm->SetRelativeRotation(FRotator{0.f, 180.f, 0.f});
    SpringArm->SetupAttachment(GetRootComponent());

    Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
    Camera->SetupAttachment(SpringArm);
}

void ADSCustomizeCharacterActor::BeginPlay()
{
    Super::BeginPlay();

    SetInputMapping(InputMapping);
}

void ADSCustomizeCharacterActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    if (const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
    {
        EnhancedInputComponent->BindAction(RotateAction, ETriggerEvent::Triggered, this, &ThisClass::Rotate);
        EnhancedInputComponent->BindAction(SetUIInputModeAction, ETriggerEvent::Triggered, this, &ThisClass::SwitchInputMode);
    }
}

void ADSCustomizeCharacterActor::Rotate(const FInputActionValue& Value)
{
    GetMesh()->AddRelativeRotation(FRotator{0.f, Value.Get<float>(), 0.f});
}

void ADSCustomizeCharacterActor::SwitchInputMode(const FInputActionValue& Value)
{
    if (const auto PlayerController = GetController<APlayerController>())
    {
        const auto IsButtonPressed = Value.Get<bool>();
        if (IsButtonPressed)
        {
            PlayerController->GetMousePosition(CursorPosition.X, CursorPosition.Y);
        }
        else
        {
            PlayerController->SetMouseLocation(CursorPosition.X, CursorPosition.Y);
        }

        PlayerController->SetShowMouseCursor(!IsButtonPressed);
    }
}
