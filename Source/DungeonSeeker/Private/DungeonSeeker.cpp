//Created by Dmirtiy Chepurkin. All right reserved

#include "DungeonSeeker.h"

#include "DSInventoryManager.h"
#include "Developer/Settings/Public/ISettingsModule.h"
#include "Modules/ModuleManager.h"

#define LOCTEXT_NAMESPACE "FDungeonSeeker"

void FDungeonSeeker::StartupModule()
{
    if (const auto Settings = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
    {
        Settings->RegisterSettings("Project", "Game", "DungeonSeeker",
                                   LOCTEXT("RuntimeSettingsName", "Inventory"),
                                   LOCTEXT("RuntimeSettingsDecs", "InventorySettings"),
                                   GetMutableDefault<UDSInventoryManager>());
    }
}

void FDungeonSeeker::ShutdownModule()
{
    if(const auto Settings = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
    {
        Settings->UnregisterSettings("Project", "Game", "DungeonSeeker");
    }
}

#undef LOCTEXT_NAMESPACE
IMPLEMENT_PRIMARY_GAME_MODULE(FDungeonSeeker, DungeonSeeker, "DungeonSeeker");
