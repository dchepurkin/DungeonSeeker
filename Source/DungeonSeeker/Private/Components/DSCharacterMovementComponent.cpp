// Created by Dmirtiy Chepurkin. All right reserved

#include "Components/DSCharacterMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCharacterMovementComponent, All, All);

UDSCharacterMovementComponent::UDSCharacterMovementComponent()
{
	PrimaryComponentTick.bStartWithTickEnabled = false;

	bUseControllerDesiredRotation = true;
	bOrientRotationToMovement = true;
	RotationRate = FRotator(0.f, 350.f, 0.f);
	JumpZVelocity = 450.f;
	GravityScale = 1.5f;
	MaxAcceleration = 1024.f;
}

void UDSCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}
