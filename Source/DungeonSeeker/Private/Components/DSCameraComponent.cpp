// Created by Dmirtiy Chepurkin. All right reserved

#include "Components/DSCameraComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCameraComponent, All, All);

UDSCameraComponent::UDSCameraComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
	PrimaryComponentTick.TickInterval = 0.1f;
}

void UDSCameraComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CheckCameraCollision();
}

void UDSCameraComponent::CheckCameraCollision()
{
	if(!GetWorld()) return;

	const auto StartPoint = GetComponentLocation();
	const auto Direction = GetForwardVector() * FadeDistance;
	const auto EndPoint = StartPoint + Direction;

	FHitResult HitResult;

	FCollisionObjectQueryParams CollisionObjectQueryParams;
	CollisionObjectQueryParams.AddObjectTypesToQuery(GetOwner()->GetRootComponent()->GetCollisionObjectType());

	GetWorld()->LineTraceSingleByObjectType(HitResult, StartPoint, EndPoint, CollisionObjectQueryParams);

	if(!HitResult.bBlockingHit && bIsOverlaped)
	{
		bIsOverlaped = false;
		OnCameraEndOverlap.Broadcast();
	}
	else if(HitResult.bBlockingHit && !bIsOverlaped && HitResult.GetActor() == GetOwner())
	{
		bIsOverlaped = true;
		OnCameraBeginOverlap.Broadcast();
	}
}
