// Created by Dmitriy Chepurkin. All rights reserved

#include "Components/DSRotationComponent.h"

UDSRotationComponent::UDSRotationComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UDSRotationComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (!IsRotationEqualTarget())
    {
        const auto RotationYaw = FMath::RInterpConstantTo(GetOwner()->GetActorRotation(), TargetRotation, DeltaTime, RotationRate).Yaw;
        GetOwner()->SetActorRotation(FRotator(0.f, RotationYaw, 0.f));
    }
    else
    {
        OnRotationFinished.Broadcast();
        SetComponentTickEnabled(false);
    }
}

void UDSRotationComponent::RotateToActor(AActor* RotateTo)
{
    if (!RotateTo) return;

    RotateToPoint(RotateTo->GetActorLocation());
}

void UDSRotationComponent::RotateToPoint(const FVector& RotetTo)
{
    if (!GetOwner()) return;
    TargetRotation = FRotationMatrix::MakeFromX(RotetTo - GetOwner()->GetActorLocation()).Rotator();

    if (!IsRotationEqualTarget())
    {
        OnRotationStarted.Broadcast();
        SetComponentTickEnabled(true);
    }
}

void UDSRotationComponent::StopRotation()
{
    SetComponentTickEnabled(false);
}

bool UDSRotationComponent::IsRotated() const
{
    return IsComponentTickEnabled();
}

bool UDSRotationComponent::IsRotationEqualTarget() const
{
    return GetOwner() && FMath::IsNearlyEqual(GetOwner()->GetActorRotation().Yaw, TargetRotation.Yaw, 0.5f);
}
