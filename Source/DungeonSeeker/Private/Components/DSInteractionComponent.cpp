// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInteractionComponent.h"
#include "DSInteractionInterface.h"
#include "DSPlayerController.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSInteractionComponent, All, All);

UDSInteractionComponent::UDSInteractionComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    PrimaryComponentTick.TickInterval = 0.1f;
}

void UDSInteractionComponent::BeginPlay()
{
    Super::BeginPlay();

    PawnOwner = GetOwner<APawn>();
    Controller = PawnOwner->GetController<ADSPlayerController>();

    SetCollisionProfileName("Interaction");

    constexpr auto PlayerCollisionChannel = ECC_GameTraceChannel1;
    SetCollisionResponseToChannel(PlayerCollisionChannel, ECR_Ignore);
}

void UDSInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (GetWorld())
    {
        FVector StartTrace;
        FVector CameraForwardVector;
        GetCameraInfo(StartTrace, CameraForwardVector);
        const auto EndTrace = StartTrace + CameraForwardVector * TraceDistance;

        FHitResult HitResult;
        FCollisionQueryParams QueryParams;
        QueryParams.AddIgnoredActor(PawnOwner);

        GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, ECC_Visibility, QueryParams);

        const auto TracedActor = HitResult.GetActor();

        //TODO � ��� ����� ����������� ������ Trace
        const auto NewFocusedActor = CanPlayerSee(TracedActor) ? TracedActor : GetFirstOverlapedActor();

        if (NewFocusedActor)
        {
            SetActorFocused(NewFocusedActor);
        }
        else
        {
            ClearFocus();
        }
    }
}

bool UDSInteractionComponent::Interact()
{
    if (InteractedActor) return false; //���� ��� ��������������� �� ������ �� ������

    if (FocusedActor && FocusedActor->Implements<UDSInteractionInterface>())
    {
        const auto InteractionType = IDSInteractionInterface::Execute_GetInteractionType(FocusedActor);
        if (InteractionType == EDSInteractionType::Item || InteractionType == EDSInteractionType::NPC)
        {
            InteractedActor = FocusedActor;

            if (Controller)
            {
                const float InteractionDistance = IDSInteractionInterface::Execute_GetMinInteractionDistance(InteractedActor);
                const auto TargetLocation = IDSInteractionInterface::Execute_GetInteractionLocation(InteractedActor);

                Controller->OnMoveFinished.AddDynamic(this, &ThisClass::OnMoveFinished_Callback);
                Controller->MoveTo(TargetLocation, InteractionDistance);
            }

            return true;
        }
    }

    return false;
}

void UDSInteractionComponent::StopInteraction()
{
    Controller->StopMovement();
    Controller->OnMoveFinished.RemoveAll(this);
    InteractedActor = nullptr;
}

void UDSInteractionComponent::SetInteractionEnabled(const bool IsEnabled)
{
    SetComponentTickEnabled(IsEnabled);
    if (!IsEnabled)
    {
        ClearFocus();
    }
}

void UDSInteractionComponent::SetActorFocused(AActor* InActor)
{
    if (InActor == FocusedActor && CanFocused(FocusedActor)) return;

    ClearFocus();

    if (CanFocused(InActor))
    {
        FocusedActor = InActor;
        SetOutlineEnable(FocusedActor, true); // �������� �� ���������� ��������� ����������� ������ �������

        OnActorFocused.Broadcast(FocusedActor, IDSInteractionInterface::Execute_GetInteractionType(FocusedActor));
    }
}

void UDSInteractionComponent::SetOutlineEnable(AActor* InActor, const bool IsEnable) const
{
    if (!bEnableOutlineOnFocusedActor || !InActor) return;

    TArray<UMeshComponent*> Meshes;
    InActor->GetComponents<UMeshComponent>(Meshes);

    for (const auto Mesh : Meshes)
    {
        if (!Mesh) continue;
        Mesh->SetRenderCustomDepth(IsEnable);
    }
}

bool UDSInteractionComponent::CanFocused(AActor* InActor) const
{
    return InActor && InActor->Implements<UDSInteractionInterface>() ? IDSInteractionInterface::Execute_CanFocus(InActor) : false;
}

void UDSInteractionComponent::ClearFocus()
{
    SetOutlineEnable(FocusedActor, false);

    if (FocusedActor)
    {
        OnClearFocus.Broadcast();
        FocusedActor = nullptr;
    }
}

void UDSInteractionComponent::GetCameraInfo(FVector& OutCameraLocation, FVector& OutCameraForwardVector) const
{
    FRotator CameraRotation;
    Controller->GetPlayerViewPoint(OutCameraLocation, CameraRotation);
    OutCameraForwardVector = CameraRotation.Vector();
}

AActor* UDSInteractionComponent::GetFirstOverlapedActor()
{
    UpdateOverlaps(nullptr, false);

    TArray<AActor*> OverlapedActors;
    GetOverlappingActors(OverlapedActors);

    for (const auto Actor : OverlapedActors)
    {
        if (CanPlayerSee(Actor))
        {
            return Actor;
        }
    }

    return nullptr;
}

bool UDSInteractionComponent::CanPlayerSee(AActor* InActor) const
{
    if (!InActor || !InActor->Implements<UDSInteractionInterface>() || !IDSInteractionInterface::Execute_CanFocus(InActor)) return false;

    const auto MinDistance = IDSInteractionInterface::Execute_GetMinFocusDistance(InActor);
    if (FVector::Distance(PawnOwner->GetActorLocation(), InActor->GetActorLocation()) >= MinDistance) return false;

    const auto EndTrace = InActor->GetActorLocation();

    FCollisionQueryParams QueryParams;
    QueryParams.AddIgnoredActor(PawnOwner);

    FHitResult HitResult;
    GetWorld()->LineTraceSingleByChannel(HitResult, PawnOwner->GetPawnViewLocation(), EndTrace, ECC_Visibility, QueryParams);

    return HitResult.bBlockingHit && HitResult.GetActor() == InActor;
}

void UDSInteractionComponent::OnMoveFinished_Callback(const bool IsSuccess)
{
    if (!IsSuccess || !InteractedActor)
    {
        StopInteraction();
        return;
    }

    //����� ���� ��� �������� �� ������ ��������� ����� �� �����������������
    if (FText Message; IDSInteractionInterface::Execute_CanInteract(InteractedActor, PawnOwner, Message))
    {
        OnInteractionStarted.Broadcast(InteractedActor);
        IDSInteractionInterface::Execute_Interact(InteractedActor, PawnOwner);
    }
    else
    {
        OnInteractionFailed.Broadcast(Message);
    }

    StopInteraction();
}
