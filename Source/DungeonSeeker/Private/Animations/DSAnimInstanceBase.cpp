// Created by Dmirtiy Chepurkin. All right reserved

#include "Animations/DSAnimInstanceBase.h"

#include "KismetAnimationLibrary.h"
#include "KismetAnimationLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

void UDSAnimInstanceBase::NativeInitializeAnimation()
{
    PawnOwner = TryGetPawnOwner();

    bUseMultiThreadedAnimationUpdate = true;
}

void UDSAnimInstanceBase::NativeThreadSafeUpdateAnimation(float DeltaSeconds)
{
    if (!PawnOwner) return;

    const auto Velocity = PawnOwner->GetVelocity();
    const auto RightVector = PawnOwner->GetActorRightVector();
    LeanAxis = FVector::DotProduct(RightVector, Velocity);

    Speed = Velocity.Size2D();
    bIsFalling = GetIsFalling();

    Direction = UKismetAnimationLibrary::CalculateDirection(Velocity, PawnOwner->GetActorRotation());
}

bool UDSAnimInstanceBase::GetIsFalling() const
{
    const auto MovementComponent = PawnOwner->FindComponentByClass<UCharacterMovementComponent>();
    return MovementComponent && MovementComponent->IsFalling();
}
