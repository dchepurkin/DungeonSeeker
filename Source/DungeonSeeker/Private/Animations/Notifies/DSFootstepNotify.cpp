// Created by Dmirtiy Chepurkin. All right reserved

#include "Animations/Notifies/DSFootstepNotify.h"

#include "DSConstants.h"
#include "Kismet/GameplayStatics.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSFootstepsNotify, All, All);

void UDSFootstepNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    Super::Notify(MeshComp, Animation, EventReference);

    if (FHitResult HitResult; BoneTrace(MeshComp, HitResult))
    {
        if (const auto PhysMat = HitResult.PhysMaterial; PhysMat.IsValid())
        {
            const auto SurfaceType = HitResult.PhysMaterial->SurfaceType;
            USoundBase* Sound = SoundsMap.Contains(SurfaceType) ? SoundsMap[SurfaceType] : nullptr;
            UGameplayStatics::PlaySoundAtLocation(MeshComp, Sound, HitResult.ImpactPoint, FRotator::ZeroRotator);
        }
    }
}


FString UDSFootstepNotify::GetNotifyName_Implementation() const
{
    return FString{"Footstep"};
}

bool UDSFootstepNotify::BoneTrace(USkeletalMeshComponent* MeshComp, FHitResult& OutResult) const
{
    if (!MeshComp) { return false; }

    if (const auto World = MeshComp->GetWorld())
    {
        const auto BoneLocation = MeshComp->GetBoneLocation(BoneName);
        const auto EndTrace = BoneLocation - FVector{0.f, 0.f, TraceLength};
        FCollisionQueryParams QueryParams;
        QueryParams.bReturnPhysicalMaterial = true;
        QueryParams.AddIgnoredActor(MeshComp->GetOwner());
        return World->LineTraceSingleByChannel(OutResult, BoneLocation, EndTrace, IKTraceChannel, QueryParams);
    }

    return false;
}
