// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/DSBorderSwitcherBase.h"

#include "DSClickBorder.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSBorderSwitcherBase, All, All);

void UDSBorderSwitcherBase::NativeConstruct()
{
    Super::NativeConstruct();
    InitBorders(Borders);
    InitBindings();
}


void UDSBorderSwitcherBase::InitBorders(TArray<UDSClickBorder*>& OutBorders)
{
    UE_LOG(LogDSBorderSwitcherBase, Error, TEXT("InitBorders member function is not implemented"));
}

void UDSBorderSwitcherBase::InitBindings()
{
    for (const auto Border : Borders)
    {
        if (!Border) { continue; }
        Border->OnBorderFocused.AddDynamic(this, &ThisClass::OnBorderFocused);
    }
}

void UDSBorderSwitcherBase::Init()
{
    Borders[0]->SetBorderFocused(true);
}

void UDSBorderSwitcherBase::OnBorderFocused(UDSClickBorder* InFocusedBorder)
{
    if (CurrentFocusedBorder && CurrentFocusedBorder != InFocusedBorder)
    {
        CurrentFocusedBorder->SetBorderFocused(false);
    }

    CurrentFocusedBorder = InFocusedBorder;
    OnSelectionChanged.Broadcast(CurrentFocusedBorder);
}
