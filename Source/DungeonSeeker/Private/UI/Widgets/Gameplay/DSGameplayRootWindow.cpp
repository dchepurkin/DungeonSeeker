#include "DSGameplayRootWindow.h"

#include "DSAimWidget.h"
#include "DSInteractionComponent.h"
#include "DSInteractionWidget.h"

void UDSGameplayRootWindow::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    OnVisibilityChanged.AddDynamic(this, &ThisClass::OnVisibilityChanged_Callback);

    if (const auto PlayerPawn = GetOwningPlayerPawn())
    {
        if (const auto InteractionComponent = PlayerPawn->FindComponentByClass<UDSInteractionComponent>())
        {
            InteractionComponent->OnActorFocused.AddDynamic(this, &ThisClass::OnActorBeginFocused_Callback);
            InteractionComponent->OnClearFocus.AddDynamic(this, &ThisClass::OnClearFocus_Callback);
        }
    }
}

void UDSGameplayRootWindow::NativeConstruct()
{
    Super::NativeConstruct();

    InteractionWidget->SetRenderOpacity(0.f);
}

void UDSGameplayRootWindow::OnVisibilityChanged_Callback(ESlateVisibility InVisibility)
{
    if (InVisibility == GetInitVisibility())
    {
        InteractionWidget->Update();
    }
}

void UDSGameplayRootWindow::OnActorBeginFocused_Callback(AActor* FocusedActor, EDSInteractionType InteractionType)
{
    if (InteractionType != EDSInteractionType::None)
    {
        AimWidget->SetAimFocus();

        if (InteractionType == EDSInteractionType::Item || InteractionType == EDSInteractionType::NPC)
        {
            InteractionWidget->Show(FocusedActor);
        }
    }
}

void UDSGameplayRootWindow::OnClearFocus_Callback()
{
    AimWidget->ClearAimFocus();
    InteractionWidget->Hide();
}

void UDSGameplayRootWindow::UpdateWidget()
{
    InteractionWidget->Update();
}
