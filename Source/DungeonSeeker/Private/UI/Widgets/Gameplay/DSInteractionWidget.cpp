// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInteractionWidget.h"
#include "DSInteractionInterface.h"
#include "Components/TextBlock.h"

void UDSInteractionWidget::Show(AActor* InActor)
{
    if(InActor && InActor->Implements<UDSInteractionInterface>())
    {
        FocusedActor = InActor;

        Update();
        PlayAnimationForward(ShowAnimation);

        SetUpdateTimerEnabled(true);
    }
}

void UDSInteractionWidget::Hide()
{
    PlayAnimationReverse(ShowAnimation);
    SetUpdateTimerEnabled(false);
}

void UDSInteractionWidget::Update()
{
    if(!IsValid(FocusedActor) || !IDSInteractionInterface::Execute_CanFocus(FocusedActor)) return;

    ActorNameTextBlock->SetText(IDSInteractionInterface::Execute_GetInteractionActorName(FocusedActor));
    InteractionNameTextBlock->SetText(IDSInteractionInterface::Execute_GetInteractionName(FocusedActor));
}

void UDSInteractionWidget::OnChangeKey_Callback(const FName& InActionName, const FKey& InNewKey)
{
    if(InActionName == "Interaction")
    {
        InteractionKeyName->SetText(InNewKey.GetDisplayName());
    }
}

void UDSInteractionWidget::SetUpdateTimerEnabled(const bool IsEnabled)
{
    IsEnabled
        ? GetWorld()->GetTimerManager().SetTimer(UpdateTimerHandle, this, &ThisClass::Update, UpdateTimerRate, true)
        : GetWorld()->GetTimerManager().ClearTimer(UpdateTimerHandle);
}
