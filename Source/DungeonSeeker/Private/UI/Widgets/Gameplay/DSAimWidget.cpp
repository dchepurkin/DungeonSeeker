// Created by Dmirtiy Chepurkin. All right reserved

#include "DSAimWidget.h"
#include "Components/Border.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSAimWidget, All, All);

void UDSAimWidget::SetNormalColor() const
{
    AimBox->SetContentColorAndOpacity(NormalColor);
}

void UDSAimWidget::SetBattleColor() const
{
    AimBox->SetContentColorAndOpacity(BattleColor);
}

void UDSAimWidget::SetAimFocus()
{
    if(!bIsFocused)
    {
        bIsFocused = true;
        PlayAnimationForward(FocusAnimation);
    }
}

void UDSAimWidget::ClearAimFocus()
{
    PlayAnimationReverse(FocusAnimation);
    bIsFocused = false;
}
