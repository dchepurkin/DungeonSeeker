// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/DSInputModeSwitcherWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSInputModeSwitcherWidget, All, All);

void UDSInputModeSwitcherWidget::NativeConstruct()
{
    Super::NativeConstruct();

    PlayerController = GetOwningPlayer();
}

void UDSInputModeSwitcherWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    PlayerController->SetInputMode(FInputModeGameAndUI{});
    Super::NativeOnMouseEnter(InGeometry, InMouseEvent);
}

void UDSInputModeSwitcherWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
    if (InMouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton)) { return; }
    PlayerController->SetInputMode(FInputModeUIOnly{});

    Super::NativeOnMouseLeave(InMouseEvent);
}
