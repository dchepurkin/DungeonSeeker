// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventoryCategoryBorder.h"

#include "DSInventoryManager.h"
#include "DSPlayerInventoryComponent.h"
#include "DSTooltipWidget.h"
#include "Components/TextBlock.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSItemCategoryBorder, All, All);

void UDSInventoryCategoryBorder::SynchronizeProperties()
{
    Super::SynchronizeProperties();

    const auto Childs = GetAllChildren();
    if (Childs.IsEmpty()) return;

    NotViewedIcon = Cast<UTextBlock>(Childs[0]);
}

void UDSInventoryCategoryBorder::OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
    Super::OnMouseEnter_Callback(InGeometry, InEvent);
    SetToolTip(GetTooltipWidget());
}

UWidget* UDSInventoryCategoryBorder::GetTooltipWidget()
{
    if(!TooltipWidgetClass)
    {
        UE_LOG(LogDSItemCategoryBorder, Warning, TEXT("TooltipWidgetClass is NULL"));
        return nullptr;
    }

    const auto Result = CreateWidget<UDSTooltipWidget>(this, TooltipWidgetClass);
    if(!Result) return nullptr;

    Result->SetText(DSTooptipText);
    return Result;
}

void UDSInventoryCategoryBorder::UpdateNotViewedIconVisibility() const
{
    bool bHaveNotViewedItems = false;
    if (const auto PlayerController = GetOwningPlayer())
    {
        const auto InventoryComponent = UDSInventoryManager::GetPlayerInventory(PlayerController);
        bHaveNotViewedItems = InventoryComponent ? InventoryComponent->HaveNotViewedItems(InventoryCategory) : false;
    }

    NotViewedIcon->SetVisibility(bHaveNotViewedItems ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Hidden);
}
