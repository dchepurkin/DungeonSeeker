// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/Inventory/DSInventoryWidget.h"

#include "DSInventoryManager.h"
#include "DSPlayerInventoryComponent.h"
#include "DSInventorySlotWidget.h"
#include "DSItemsCategorySwitcherWidget.h"
#include "DSPlayerRenderActor.h"
#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"
#include "Components/UniformGridPanel.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSInventoryWidget, All, All);

void UDSInventoryWidget::NativeConstruct()
{
    Super::NativeConstruct();
    if (!GetOwningPlayerPawn()) return;

    //����� ������ ������� ����� ����������� � ���������
    if (CharacterRenderClass)
    {
        CharacterRender = GetWorld()->SpawnActor<ADSPlayerRenderActor>(CharacterRenderClass, FTransform(FVector(0.f, 0.f, 3000000.f)));
    }
    else
    {
        UE_LOG(LogDSInventoryWidget, Warning, TEXT("CharacterRenderClass is NULL"));
    }

    InventoryComponent = GetOwningPlayerPawn()->FindComponentByClass<UDSPlayerInventoryComponent>();
    if (!InventoryComponent) return;

    InventoryComponent->OnGoldChanged.AddDynamic(this, &ThisClass::UpdateGold);
    InventoryComponent->OnSlotsAmountChanged.AddDynamic(this, &ThisClass::OnSlotsAmountChanged_Callback);
    InventoryComponent->OnRemoveItem.AddDynamic(this, &ThisClass::OnRemoveItem_Callback);
    InventoryComponent->OnAddItem.AddDynamic(this, &ThisClass::OnAddItem_Callback);
    InventoryComponent->OnIdentifyItem.AddDynamic(this, &ThisClass::OnIdentifyItem_Callback);
    CategorySwitcher->OnInventorySwitch.AddDynamic(this, &ThisClass::InventorySwitch_Callback);

    InitInventory();
}

void UDSInventoryWidget::Close()
{
    Super::Close();

    if (CharacterRender)
    {
        CharacterRender->Destroy();
    }

    if (InventoryComponent)
    {
        InventoryComponent->OnGoldChanged.RemoveDynamic(this, &ThisClass::UpdateGold);
        InventoryComponent->OnRemoveItem.RemoveDynamic(this, &ThisClass::OnRemoveItem_Callback);
        InventoryComponent->OnAddItem.RemoveDynamic(this, &ThisClass::OnAddItem_Callback);
        InventoryComponent->OnIdentifyItem.RemoveDynamic(this, &ThisClass::OnIdentifyItem_Callback);
    }
}

void UDSInventoryWidget::OnRemoveItem_Callback(EInventoryCategory InCategory)
{
    CategorySwitcher->UpdateCategoryViewedIcon(InCategory);
    UpdateInventory(CategorySwitcher->GetCategory());
}

void UDSInventoryWidget::OnAddItem_Callback(const FInventoryItem& InItem)
{
    if (const auto ItemInfo = InItem.GetInfo(this))
    {
        CategorySwitcher->UpdateCategoryViewedIcon(ItemInfo->Category);
    }

    UpdateInventory(CategorySwitcher->GetCategory());
}

void UDSInventoryWidget::InventorySwitch_Callback(EInventoryCategory InCategory)
{
    UpdateInventory(InCategory);
    InventorySlotsScrollBox->ScrollToStart();
}

void UDSInventoryWidget::OnSlotsAmountChanged_Callback(EInventoryCategory Category, int Amount)
{
    if (Category == CategorySwitcher->GetCategory())
    {
        UpdateInventory(Category);
    }
}

void UDSInventoryWidget::UpdateGold(const int32 InGoldAmount)
{
    GoldTextBlock->SetText(FText::FromString(FString::FromInt(InGoldAmount)));
}

void UDSInventoryWidget::OnIdentifyItem_Callback(const FName& InItemID)
{
    if (const auto InventoryManager = UDSInventoryManager::GetInventoryManager(this))
    {
        if (const auto ItemInfo = InventoryManager->GetItemInfo(InItemID))
        {
            CategorySwitcher->UpdateCategoryViewedIcon(ItemInfo->Category);
        }
    }

    UpdateInventory(CategorySwitcher->GetCategory());
}

void UDSInventoryWidget::InitInventory() const
{
    CategorySwitcher->Init();
}

void UDSInventoryWidget::UpdateInventory(EInventoryCategory InCategory)
{
    if (!InventorySlotWidgetClass)
    {
        UE_LOG(LogDSInventoryWidget, Warning, TEXT("InventorySlotWidgetClass is NULL"));
        return;
    }

    UpdateGold(InventoryComponent->GetGold());

    SlotsWidgets.Empty();
    InventorySlotsGrid->ClearChildren();

    const auto& Inventory = InventoryComponent->GetContainer(InCategory);
    const auto SlotsAmount = Inventory.SlotsAmount;

    //������� �����;
    for (int32 Index = 0; Index < SlotsAmount; ++Index)
    {
        if (const auto NewSlot = CreateSlot(Index))
        {
            if (Inventory.Slots.IsValidIndex(Index))
            {
                NewSlot->InitSlot(Index, Inventory.Slots[Index]);
            }
        }
    }
}

UDSInventorySlotWidget* UDSInventoryWidget::CreateSlot(const int32 InIndex)
{
    const auto NewSlot = CreateWidget<UDSInventorySlotWidget>(GetOwningPlayer(), InventorySlotWidgetClass);
    if (NewSlot)
    {
        SlotsWidgets.Add(NewSlot);
        InventorySlotsGrid->AddChildToUniformGrid(NewSlot, GetRowByIndex(InIndex), GetColumnByIndex(InIndex));
    }

    return NewSlot;
}

int32 UDSInventoryWidget::GetRowByIndex(const int32 InIndex) const
{
    return InIndex / SlotsInRow;
}

int32 UDSInventoryWidget::GetColumnByIndex(const int32 InIndex) const
{
    return InIndex % SlotsInRow;
}
