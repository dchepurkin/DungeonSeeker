// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/Inventory/DSTooltipWidget.h"
#include "Components/TextBlock.h"

void UDSTooltipWidget::NativeConstruct()
{
    Super::NativeConstruct();
    PlayAnimation(ShowAnimation);
}

void UDSTooltipWidget::SetText(const FText& InText) const
{
    TooltipTextBlock->SetText(InText);
}