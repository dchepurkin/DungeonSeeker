// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventorySlotWidget.h"

#include "DSInventoryManager.h"
#include "DSPlayerInventoryComponent.h"
#include "Components/Image.h"
#include "Components/Overlay.h"
#include "Components/TextBlock.h"
//#include "DSItemToolTipWidget.h"
#include "DSHoverBorder.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSInventorySlotWidget, All, All);

void UDSInventorySlotWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    AmountOverlay->SetVisibility(ESlateVisibility::Hidden);
    ItemIcon->SetVisibility(ESlateVisibility::Hidden);
    UnidentifiedIcon->SetVisibility(ESlateVisibility::Hidden);
    FocusBorder->SetCanHover(false);

    ToolTipWidgetDelegate.BindDynamic(this, &ThisClass::GetToolTipWidget);
}

void UDSInventorySlotWidget::RemoveItem() const
{
    /*if (const auto InventoryComponent = UDSInventoryManager::GetInventoryComponent(GetOwningPlayerPawn()))
    {
        InventoryComponent->RemoveItemByIndex(SlotIndex, Item.Amount);
    }*/
}

void UDSInventorySlotWidget::DropItem() const
{
    /*if (const auto InventoryComponent = UDSInventoryManager::GetInventoryComponent(GetOwningPlayerPawn()))
    {
        InventoryComponent->DropItemByIndex(SlotIndex, Item.Amount);
    }*/
}

void UDSInventorySlotWidget::InitSlot(const int32 InSlotIndex, const FInventoryItem& InSlotInfo)
{
    if (InSlotIndex == INDEX_NONE) return;

    SlotIndex = InSlotIndex;
    Item = InSlotInfo;

    UpdateSlot();
}

FReply UDSInventorySlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    //todo show context menu on right mouse button click
    return IsEmpty() ? FReply::Unhandled() : Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}

FReply UDSInventorySlotWidget::NativeOnMouseButtonDoubleClick(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    //Todo Use Item
    return IsEmpty() ? FReply::Unhandled() : Super::NativeOnMouseButtonDoubleClick(InGeometry, InMouseEvent);
}

void UDSInventorySlotWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

    if (IsEmpty()) return;

    //TODO broadcast event to parent widget
    if (const auto InventoryComponent = UDSInventoryManager::GetPlayerInventory(this))
    {
        if (const auto ItemInfo = Item.GetInfo(this))
        {
            InventoryComponent->SetItemViewed(SlotIndex, ItemInfo->Category);
        }
    }
}

void UDSInventorySlotWidget::UpdateSlot()
{
    if (IsEmpty()) return;

    FocusBorder->SetDefaultColor();
    FocusBorder->SetCanHover(true);

    if (Item.bNotViewed)
    {
        FocusBorder->SetBorderColor(NotViewedColor);
    }

    if (const auto ItemInfo = Item.GetInfo(this))
    {
        ItemIcon->SetBrushFromTexture(ItemInfo->Icon);
        ItemIcon->SetVisibility(ItemInfo->Icon ? ESlateVisibility::HitTestInvisible : ESlateVisibility::Hidden);

        const auto IsIdentified = UDSInventoryManager::IsIdentified(this, Item.ID);
        UnidentifiedIcon->SetVisibility(IsIdentified ? ESlateVisibility::Hidden : ESlateVisibility::SelfHitTestInvisible);

        if (ItemInfo->bIsStackable)
        {
            AmountTextBlock->SetText(FText::FromString(FString::FromInt(Item.Amount)));
            AmountOverlay->SetVisibility(ESlateVisibility::HitTestInvisible);
        }
        else
        {
            AmountOverlay->SetVisibility(ESlateVisibility::Hidden);
        }
    }
}

bool UDSInventorySlotWidget::CanInteract()
{
    if (IsEmpty()) return false;

    if (const auto ItemInfo = Item.GetInfo(this))
    {
        return false; //ItemInfo->bIsUsable || ItemInfo->bCanDestroy;
    }
    return false;
}

UWidget* UDSInventorySlotWidget::GetToolTipWidget()
{
    /*if(!ToolTipWidgetClass)
    {
        UE_LOG(LogWSInventorySlotWidget, Warning, TEXT("ToolTipWidgetClass is NULL"));
        return nullptr;
    }

    return IsEmpty()
               ? nullptr
               : UWSItemToolTipWidget::CreateToolTip(GetOwningPlayer(), ToolTipWidgetClass, Item);*/

    return nullptr;
}
