// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/Inventory/DSItemsCategorySwitcherWidget.h"
#include "DSInventoryManager.h"
#include "DSPlayerInventoryComponent.h"
#include "DSInventoryCategoryBorder.h"
#include "Components/HorizontalBox.h"

void UDSItemsCategorySwitcherWidget::NativeConstruct()
{
    Super::NativeConstruct();

    if (const auto InventoryComponent = UDSInventoryManager::GetPlayerInventory(this))
    {
        InventoryComponent->OnItemViewed.AddDynamic(this, &ThisClass::ItemViewed_Callback);
    }
}

void UDSItemsCategorySwitcherWidget::InitBorders(TArray<UDSClickBorder*>& OutBorders)
{
    if (ButtonsBox)
    {
        const auto Buttons = ButtonsBox->GetAllChildren();
        for (const auto Button : Buttons)
        {
            if (const auto CategoryBorder = Cast<UDSInventoryCategoryBorder>(Button))
            {
                OutBorders.Add(CategoryBorder);
                CategoryBorder->UpdateNotViewedIconVisibility();
            }
        }
    }
}

void UDSItemsCategorySwitcherWidget::NativeDestruct()
{
    if (const auto InventoryComponent = UDSInventoryManager::GetPlayerInventory(this))
    {
        InventoryComponent->OnItemViewed.RemoveDynamic(this, &ThisClass::ItemViewed_Callback);
    }

    Super::NativeDestruct();
}

void UDSItemsCategorySwitcherWidget::OnBorderFocused(UDSClickBorder* InFocusedBorder)
{
    Super::OnBorderFocused(InFocusedBorder);
    ChangeCategory(Cast<UDSInventoryCategoryBorder>(InFocusedBorder));
}

void UDSItemsCategorySwitcherWidget::ItemViewed_Callback(int32 InItemIndex, EInventoryCategory InCategory)
{
    UpdateCategoryViewedIcon(InCategory);
}

void UDSItemsCategorySwitcherWidget::UpdateCategoryViewedIcon(const EInventoryCategory InCategory)
{
    const auto ClickBorder = Borders.FindByPredicate([InCategory](UDSClickBorder* InBorder)
    {
        const auto ItemCategoryBorder = Cast<UDSInventoryCategoryBorder>(InBorder);
        return ItemCategoryBorder ? ItemCategoryBorder->GetCategory() == InCategory : false;
    });

    if (ClickBorder)
    {
        if (const auto ItemCategoryBorder = Cast<UDSInventoryCategoryBorder>(*ClickBorder))
        {
            ItemCategoryBorder->UpdateNotViewedIconVisibility();
        }
    }
}

void UDSItemsCategorySwitcherWidget::ChangeCategory(UDSInventoryCategoryBorder* InItemTypeBorder) const
{
    if (!InItemTypeBorder) { return; }
    OnInventorySwitch.Broadcast(InItemTypeBorder->GetCategory());
}

EInventoryCategory UDSItemsCategorySwitcherWidget::GetCategory() const
{
    const auto ItemCategoryBorder = Cast<UDSInventoryCategoryBorder>(CurrentFocusedBorder);
    return ItemCategoryBorder ? ItemCategoryBorder->GetCategory() : EInventoryCategory::Other;
}
