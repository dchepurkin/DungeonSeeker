// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/NewCharacter/DSNewCharacterWidget.h"

#include "DSButton.h"
#include "DSGameInstance.h"
#include "DSSavableModularCharacter.h"
#include "DSSwitcher.h"
#include "DSConstants.h"
#include "DSHUD.h"
#include "DSSaveGameManager.h"
#include "Components/EditableText.h"
#include "Components/HorizontalBox.h"
#include "DSDialogWindowWidget.h"
#include "Data/DSCharacterCustomization.h"
#include "NewCharacter/DSColorSelectorWidget.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSNewCharacterWidget, All, All);

void UDSNewCharacterWidget::NativeConstruct()
{
    Super::NativeConstruct();

    CreateCharacterButton->OnClicked.AddDynamic(this, &ThisClass::OnCreateCharacter);
    CreateCharacterButton->SetIsEnabled(false);

    CharacterNameText->OnTextChanged.AddDynamic(this, &ThisClass::OnCharacterNameChanged);

    ModularCharacter = GetOwningPlayerPawn<ADSSavableModularCharacter>();

    GenderSwitcherWidget->OnOptionChanged.AddDynamic(this, &ThisClass::SwitchGender);
    HairSwitcherWidget->OnOptionChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetHair);
    EyebrowsSwitcherWidget->OnOptionChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetEyebrow);
    HeadsSwitcherWidget->OnOptionChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetHead);
    BeardsSwitcherWidget->OnOptionChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetBeard);

    HairColorSelector->OnColorChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetHairColor);
    ScarColorSelector->OnColorChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetScarColor);
    BodyArtColorSelector->OnColorChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetBodyArtColor);
    SkinColorSelector->OnColorsChanged.AddDynamic(ModularCharacter, &ADSSavableModularCharacter::SetSkinColor);

    SwitchGender(0); //0 - Male, 1 - Female

    if (const auto PawnOwner = GetOwningPlayerPawn())
    {
        PawnOwner->SetActorHiddenInGame(false);
    }
}

void UDSNewCharacterWidget::SwitchGender(int32 OptionIndex)
{
    const auto NewGender = static_cast<EDSGender>(OptionIndex + 1);
    ModularCharacter->SetGender(NewGender);
    UpdateCharacterEditor(NewGender);
}

void UDSNewCharacterWidget::OnCreateCharacter()
{
    if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
    {
        if (SaveGameManager->IsCharacterExist(FName{CharacterNameText->GetText().ToString()}))
        {
            ShowFailMessage();
        }
        else
        {
            bIsCharacterCreated = SaveGameManager->SavePlayer();
            Close();
        }
    }
}

void UDSNewCharacterWidget::OnCharacterNameChanged(const FText& Text)
{
    ModularCharacter->SetName(FName{CharacterNameText->GetText().ToString()});
    CreateCharacterButton->SetIsEnabled(Text.ToString().Len() >= MinCharacterNameLength);
}

void UDSNewCharacterWidget::UpdateCharacterEditor(EDSGender InGender) const
{
    BeardBox->SetVisibility(InGender == EDSGender::Male ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed);

    const auto GameInstance = GetGameInstance<UDSGameInstance>();
    if (const auto CharacterCustomization = GameInstance ? GameInstance->GetCharacterCustomization() : nullptr)
    {
        if (InGender == EDSGender::Male)
        {
            UpdateSwitcher(BeardsSwitcherWidget, CharacterCustomization->GetBeardsAmount());
        }
        else
        {
            BeardsSwitcherWidget->SelectOption(0);
        }

        UpdateSwitcher(HeadsSwitcherWidget, CharacterCustomization->GetHeadsAmount(InGender));
        UpdateSwitcher(EyebrowsSwitcherWidget, CharacterCustomization->GetEyebrowsAmount(InGender));
        UpdateSwitcher(HairSwitcherWidget, CharacterCustomization->GetHairsAmount(InGender));

        HairColorSelector->Init();
        ScarColorSelector->Init();
        BodyArtColorSelector->Init();
        SkinColorSelector->Init();
    }
}

void UDSNewCharacterWidget::UpdateSwitcher(UDSSwitcher* InSwitcher, const int32 OptionsAmount) const
{
    InSwitcher->ClearOptions();

    for (int32 Index = 0; Index < OptionsAmount; ++Index)
    {
        InSwitcher->AddOption(SwitcherOptionName + " " + FString::FromInt(Index + 1));
    }

    InSwitcher->SelectOption(0);
}

void UDSNewCharacterWidget::ShowFailMessage()
{
    if (!DialogWindowWidgetClass)
    {
        UE_LOG(LogDSNewCharacterWidget, Error, TEXT("DialogWindowWidgetClass is NULL"));
        return;
    }

    if (const auto HUD = GetDSHUD())
    {
        HUD->OpenDialog(this, DialogWindowWidgetClass, CreateCharacterFailMessage);
    }
}
