// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/NewCharacter/DSCharacterSlotWidget.h"

#include "DSClickBorder.h"
#include "Components/TextBlock.h"

void UDSCharacterSlotWidget::SetData(const FPlayerSaveData& Data)
{
    CharacterData = Data;
    CharacterNameText->SetText(FText::FromName(CharacterData.PlayerCharacterName));
}

void UDSCharacterSlotWidget::SetFocused(bool IsFocused)
{
    Border->SetBorderFocused(IsFocused);
    if (IsFocused)
    {
        OnCharacterSlotSelected.Broadcast(this);
    }
}

FReply UDSCharacterSlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
    if (InMouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
    {
        SetFocused(true);
    }

    return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}
