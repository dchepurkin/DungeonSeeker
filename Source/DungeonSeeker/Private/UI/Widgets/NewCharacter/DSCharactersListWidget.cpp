// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/NewCharacter/DSCharactersListWidget.h"

#include "DSButton.h"
#include "DSDialogWindowWidget.h"
#include "DSHUD.h"
#include "DSSaveGameManager.h"
#include "Components/TextBlock.h"
#include "Components/VerticalBox.h"
#include "NewCharacter/DSCharacterSlotWidget.h"
#include "NewCharacter/DSNewCharacterWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCharactersListWidget, All, All);

void UDSCharactersListWidget::NativeConstruct()
{
    Super::NativeConstruct();

    RemoveCharacterButton->OnClicked.AddDynamic(this, &ThisClass::OnRemoveButtonClicked);
    NewCharacterButton->OnClicked.AddDynamic(this, &ThisClass::OnNewCharacterButtonClicked);

    Update();
    MaxCharactersAmountText->SetText(FText::FromString(FString::FromInt(MaxCharacters)));
}

void UDSCharactersListWidget::Update()
{
    if (!CharacterSlotWidgetClass)
    {
        UE_LOG(LogDSCharactersListWidget, Error, TEXT("CharacterSlotWidgetClass is NULL"));
        return;
    }

    CurrentSelectedSlot = nullptr;
    CharactersSlots.Empty(MaxCharacters);
    //CurrentCharactersAmount = 0;
    CharactersVerticalBox->ClearChildren();

    if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
    {
        TArray<FPlayerSaveData> SavedCharacters;
        SaveGameManager->GetSavedCharacters(SavedCharacters);

        for (const auto& CharacterName : SavedCharacters)
        {
            if (const auto NewSlot = CreateCharacterSlot(CharacterName))
            {
                //++CurrentCharactersAmount;
                CharactersSlots.Add(NewSlot);
                CharactersVerticalBox->AddChildToVerticalBox(NewSlot);

                if (!CurrentSelectedSlot)
                {
                    NewSlot->SetFocused(true);
                }
            }
        }
    }

    CurrentCharactersAmountText->SetText(FText::FromString(FString::FromInt(CharactersSlots.Num())));

    RemoveCharacterButton->SetIsEnabled(!CharactersSlots.IsEmpty());
    NewCharacterButton->SetIsEnabled(CharactersSlots.Num() != MaxCharacters);

    OnCharacterListUpdated.Broadcast(CharactersSlots.Num());
}

FName UDSCharactersListWidget::GetCurrentCharacterName() const
{
    return CurrentSelectedSlot ? CurrentSelectedSlot->GetCharacterName() : FName{};
}

UDSCharacterSlotWidget* UDSCharactersListWidget::CreateCharacterSlot(const FPlayerSaveData& InCharacterData)
{
    if (const auto NewSlot = CreateWidget<UDSCharacterSlotWidget>(this, CharacterSlotWidgetClass))
    {
        NewSlot->SetData(InCharacterData);
        NewSlot->OnCharacterSlotSelected.AddDynamic(this, &ThisClass::OnCharacterSlotSelected);
        return NewSlot;
    }

    return nullptr;
}

void UDSCharactersListWidget::OnCharacterSlotSelected(UDSCharacterSlotWidget* CharacterSlot)
{
    if (CurrentSelectedSlot == CharacterSlot || !CharacterSlot) { return; }

    if (CurrentSelectedSlot)
    {
        CurrentSelectedSlot->SetFocused(false);
    }

    CurrentSelectedSlot = CharacterSlot;
    OnCharacterSelected.Broadcast(CurrentSelectedSlot->GetCharacterData());
}

void UDSCharactersListWidget::OnRemoveButtonClicked()
{
    if (!DialogWindowWidgetClass)
    {
        UE_LOG(LogDSCharactersListWidget, Error, TEXT("DialogWindowWidgetClass is NULL"));
        return;
    }

    if (const auto HUD = GetDSHUD())
    {
        if (const auto DialogWindow = HUD->OpenDialog(this, DialogWindowWidgetClass, RemoveCharacterQuestion))
        {
            DialogWindow->OnDialogClosed.AddDynamic(this, &ThisClass::OnRemoveCharacterDialogClosed);
        }
    }
}

void UDSCharactersListWidget::OnRemoveCharacterDialogClosed(UDSDialogWindowWidget* InWindow, EDialogCallback DialogCallback)
{
    if (DialogCallback == EDialogCallback::Yes)
    {
        if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
        {
            if (SaveGameManager->RemoveSaveGame(CurrentSelectedSlot->GetCharacterName()))
            {
                Update();
            }
        }
    }
}

void UDSCharactersListWidget::OnNewCharacterButtonClicked()
{
    if (!NewCharacterWidgetClass)
    {
        UE_LOG(LogDSCharactersListWidget, Error, TEXT("NewCharacterWidgetClass is NULL"));
        return;
    }

    if (const auto HUD = GetDSHUD())
    {
        if (const auto CharacterEditorWindow = HUD->CreateWindow(HUD->GetRootWindow(), NewCharacterWidgetClass))
        {
            CharacterEditorWindow->OnCloseWindow.AddDynamic(this, &ThisClass::OnCharacterEditorClosed);
            FWindowParameters Params;
            Params.BlockOtherWindows = EBlockWindows::Hide;
            CharacterEditorWindow->Show(Params);
        }
    }
}

void UDSCharactersListWidget::OnCharacterEditorClosed(UDSWindowWidget* InCharacterEditorWindow)
{
    if (const auto NewCharacterWidget = Cast<UDSNewCharacterWidget>(InCharacterEditorWindow); NewCharacterWidget && NewCharacterWidget->GetIsCharacterCreated())
    {
        Update();

        if (!CharactersSlots.IsEmpty())
        {
            CharactersSlots.Last()->SetFocused(true);
        }
    }
    else if (CurrentSelectedSlot)
    {
        OnCharacterSelected.Broadcast(CurrentSelectedSlot->GetCharacterData());
    }
}
