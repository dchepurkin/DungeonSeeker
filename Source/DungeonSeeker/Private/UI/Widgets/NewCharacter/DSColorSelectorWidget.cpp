// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/NewCharacter/DSColorSelectorWidget.h"

#include "DSClickBorder.h"
#include "Components/HorizontalBox.h"
#include "NewCharacter/DSSkinColorBorder.h"

void UDSColorSelectorWidget::InitBorders(TArray<UDSClickBorder*>& OutBorders)
{
    if (ColorBox)
    {
        const auto Buttons = ColorBox->GetAllChildren();
        for (const auto Button : Buttons)
        {
            if (const auto CategoryBorder = Cast<UDSClickBorder>(Button))
            {
                OutBorders.Add(CategoryBorder);
            }
        }
    }
}

void UDSColorSelectorWidget::OnBorderFocused(UDSClickBorder* InFocusedBorder)
{
    Super::OnBorderFocused(InFocusedBorder);
    OnColorChanged.Broadcast(InFocusedBorder ? InFocusedBorder->GetContentFocusColor() : FLinearColor::White);
}

//////////////////////////////////////////

void UDSDoubleColorSelectorWidget::InitBorders(TArray<UDSClickBorder*>& OutBorders)
{
    if (ColorBox)
    {
        const auto Buttons = ColorBox->GetAllChildren();
        for (const auto Button : Buttons)
        {
            if (const auto CategoryBorder = Cast<UDSSkinColorBorder>(Button))
            {
                OutBorders.Add(CategoryBorder);
            }
        }
    }
}

void UDSDoubleColorSelectorWidget::OnBorderFocused(UDSClickBorder* InFocusedBorder)
{
    Super::OnBorderFocused(InFocusedBorder);

    if (const auto SkinColorBorder = Cast<UDSSkinColorBorder>(InFocusedBorder))
    {
        OnColorsChanged.Broadcast(SkinColorBorder->GetContentFocusColor(), SkinColorBorder->GetStubbleColor());
    }
}
