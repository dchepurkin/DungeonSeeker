// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/Widgets/NewCharacter/DSSelectCharacterWidget.h"

#include "DSButton.h"
#include "DSGameInstance.h"
#include "DSInputModeSwitcherWidget.h"
#include "DSSaveGameManager.h"
#include "SaveGameTypes.h"
#include "Components/TextBlock.h"
#include "NewCharacter/DSCharactersListWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSSelectCharacterWidget, All, All);

void UDSSelectCharacterWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    CharactersListWidget->OnCharacterSelected.AddDynamic(this, &ThisClass::OnCharacterSelected);
    CharactersListWidget->OnCharacterListUpdated.AddDynamic(this, &ThisClass::OnCharacterListUpdated);
    StartGameButton->OnClicked.AddDynamic(this, &ThisClass::StartGame);
}

void UDSSelectCharacterWidget::OnCharacterSelected(const FPlayerSaveData& CharacterData)
{
    LoadPlayer(CharacterData.PlayerCharacterName);
}

void UDSSelectCharacterWidget::OnCharacterListUpdated(int32 InCharactersAmount)
{
    bool IsEmptyCharacters = InCharactersAmount == 0;

    if (const auto PawnOwner = GetOwningPlayerPawn())
    {
        PawnOwner->SetActorHiddenInGame(IsEmptyCharacters);
    }

    StartGameButton->SetIsEnabled(!IsEmptyCharacters);
    CharacterRotationWidget->SetVisibility(IsEmptyCharacters ? ESlateVisibility::HitTestInvisible : ESlateVisibility::Visible);
}

void UDSSelectCharacterWidget::LoadPlayer(const FName& InCharacterName)
{
    if (InCharacterName.IsNone()) { return; }

    if (const auto SaveGameManager = UDSSaveGameManager::GetSaveGameManager())
    {
        SaveGameManager->LoadPlayer(InCharacterName);
    }
}

void UDSSelectCharacterWidget::StartGame()
{
    if (const auto GameInstance = GetGameInstance<UDSGameInstance>())
    {
        GameInstance->StartGame(CharactersListWidget->GetCurrentCharacterName());
    }
}
