// Created by Dmirtiy Chepurkin. All right reserved

#include "UI/DSGameplayHUD.h"

#include "DSInteractionComponent.h"
#include "DSInventoryComponent.h"
#include "DSPlayerCharacter.h"
#include "DSUtils.h"
#include "EnhancedInputComponent.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSGameplayHUD, All, All);

ADSGameplayHUD::ADSGameplayHUD()
{
    ForEachEnum<EDSUIType>([this](EDSUIType UIType)
    {
        WidgetsClasses.Add(UIType, nullptr);
    });
}

void ADSGameplayHUD::BeginPlay()
{
    Super::BeginPlay();

    if (!WidgetsClasses.Contains(EDSUIType::Gameplay) || WidgetsClasses.IsEmpty())
    {
        UE_LOG(LogDSGameplayHUD, Error, TEXT("WidgetsClasses is Empty"));
        return;
    }

    CreateRootWindow(WidgetsClasses[UIState], true);

    PlayerCharacter = Cast<ADSPlayerCharacter>(GetOwningPawn());

    if (PlayerCharacter)
    {
        if (const auto InteractionComponent = PlayerCharacter->FindComponentByClass<UDSInteractionComponent>())
        {
            InteractionComponent->OnInteractionFailed.AddDynamic(this, &ThisClass::OnInteractionFailed);
        }

        if (const auto InventoryComponent = PlayerCharacter->FindComponentByClass<UDSInventoryComponent>())
        {
            InventoryComponent->OnDropItem.AddDynamic(this, &ThisClass::OnDropFailed);
        }

        PlayerCharacter->OnSwitchToUI.AddDynamic(this, &ThisClass::SwitchUI);
    }
}

void ADSGameplayHUD::SwitchUI(EDSUIType UIType)
{
    if (UIState == UIType)
    {
        UIState = EDSUIType::Gameplay;
        ResumeGame();
    }
    else
    {
        UIState = UIType;
        SetInputMode(EDSInputMode::UIWithPause);
    }

    CreateRootWindow(WidgetsClasses[UIState], true);
}

void ADSGameplayHUD::OnInteractionFailed(const FText& FailMessage)
{
    ShowMessage(FailMessage);
}

void ADSGameplayHUD::OnDropFailed(const FInventoryItem& InItem, bool bIsSuccess, const FText& FailMessage)
{
    if (!bIsSuccess)
    {
        ShowMessage(FailMessage);
    }
}
