// Created by Dmirtiy Chepurkin. All right reserved

#include "Data/DSCharacterCustomization.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSCharacterCustomization, All, All);

const TArray<FDSBodyPart>& UDSCharacterCustomization::GetDefaultBodyParts(EDSGender InGender) const
{
    if (!DefaultBody.Contains(InGender))
    {
        UE_LOG(LogDSCharacterCustomization, Fatal, TEXT("Default body dont set in GameInstance"));
    }

    return DefaultBody[InGender].Parts;
}

USkeletalMesh* UDSCharacterCustomization::GetHead(EDSGender InGender, int32 InIndex) const
{
    switch (InGender)
    {
    case EDSGender::Male:
        return MaleHeads.IsValidIndex(InIndex) ? MaleHeads[InIndex] : nullptr;

    case EDSGender::Female:
        return FemaleHeads.IsValidIndex(InIndex) ? FemaleHeads[InIndex] : nullptr;

    default:
        return nullptr;
    }
}

USkeletalMesh* UDSCharacterCustomization::GetHair(EDSGender InGender, int32 InIndex) const
{
    switch (InGender)
    {
    case EDSGender::Male:
        return MaleHairs.IsValidIndex(InIndex) ? MaleHairs[InIndex] : nullptr;

    case EDSGender::Female:
        return FemaleHairs.IsValidIndex(InIndex) ? FemaleHairs[InIndex] : nullptr;

    default:
        return nullptr;
    }
}

USkeletalMesh* UDSCharacterCustomization::GetEyebrow(EDSGender InGender, int32 InIndex) const
{
    switch (InGender)
    {
    case EDSGender::Male:
        return MaleEyebrows.IsValidIndex(InIndex) ? MaleEyebrows[InIndex] : nullptr;

    case EDSGender::Female:
        return FemaleEyebrows.IsValidIndex(InIndex) ? FemaleEyebrows[InIndex] : nullptr;

    default:
        return nullptr;
    }
}

USkeletalMesh* UDSCharacterCustomization::GetBeard(EDSGender InGender, int32 InIndex) const
{
    switch (InGender)
    {
    case EDSGender::Male:
        return Beards.IsValidIndex(InIndex) ? Beards[InIndex] : nullptr;

    default:
        return nullptr;
    }
}

int32 UDSCharacterCustomization::GetHeadsAmount(EDSGender InGender) const
{
    return InGender == EDSGender::Male ? MaleHeads.Num() : FemaleHeads.Num();
}

int32 UDSCharacterCustomization::GetEyebrowsAmount(EDSGender InGender) const
{
    return InGender == EDSGender::Male ? MaleEyebrows.Num() : FemaleEyebrows.Num();
}

int32 UDSCharacterCustomization::GetHairsAmount(EDSGender InSex) const
{
    return InSex == EDSGender::Male ? MaleHairs.Num() : FemaleHairs.Num();
}

