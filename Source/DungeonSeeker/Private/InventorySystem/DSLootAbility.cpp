// Created by Dmirtiy Chepurkin. All right reserved

#include "DSLootAbility.h"

UAnimMontage* UDSLootAbility::GetAbilityAnimation() const
{
    if (!AbilityUser || !GetAbilityTarget()) return nullptr;

    const auto ItemLocationZ = GetAbilityTarget()->GetActorLocation().Z;
    const auto WhoInteractLocationZ = AbilityUser->GetActorLocation().Z;

    return FMath::IsNearlyEqual(ItemLocationZ, WhoInteractLocationZ, 40.f)
               ? MiddlePickupAnimation
               : ItemLocationZ < WhoInteractLocationZ
               ? Super::GetAbilityAnimation() // FloorAnimation
               : UpPickupAnimation;
}
