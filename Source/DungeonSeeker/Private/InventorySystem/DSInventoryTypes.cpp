// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventoryTypes.h"

bool FInventoryItem::IsStackable(UObject* InWorldContext) const
{
    const auto ItemInfo = GetInfo(InWorldContext);
    return ItemInfo ? ItemInfo->bIsStackable : false;
}

bool FInventoryItem::IsDroppable(UObject* InWorldContext) const
{
    const auto ItemInfo = GetInfo(InWorldContext);
    return ItemInfo ? ItemInfo->bIsDroppable : false;
}

bool FInventoryItem::IsRemovable(UObject* InWorldContext) const
{
    const auto ItemInfo = GetInfo(InWorldContext);
    return ItemInfo ? ItemInfo->bIsRemovable : false;
}

bool FInventoryItem::IsUsable(UObject* InWorldContext) const
{
    const auto ItemInfo = GetInfo(InWorldContext);
    return ItemInfo ? ItemInfo->bIsUsable : false;
}

bool FInventoryItem::CanBeSold(UObject* InWorldContext) const
{
    const auto ItemInfo = GetInfo(InWorldContext);
    return ItemInfo ? ItemInfo->CanBeSold : false;
}
