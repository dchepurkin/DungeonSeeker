// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventoryFunctionLibrary.h"
#include "DSInventoryManager.h"

FInventoryItemInfo* UDSInventoryFunctionLibrary::GetItemInfo(const UObject* InWorldContext, const FName& InItemID)
{
    const auto InventoryManager = UDSInventoryManager::GetInventoryManager(InWorldContext);
    return InventoryManager ? InventoryManager->GetItemInfo(InItemID) : nullptr;
}
