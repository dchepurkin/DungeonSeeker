// Created by Dmirtiy Chepurkin. All right reserved

#include "DSPlayerInventoryComponent.h"

#include "DSInventoryFunctionLibrary.h"
#include "DSInventoryManager.h"
#include "DSUtils.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSPlayerInventoryComponent, All, All);

void UDSPlayerInventoryComponent::BeginPlay()
{
    Super::BeginPlay();

    Gold = 0; //For SaveGame, if default == 0 savegame does not work
}

FItemsContainer& UDSPlayerInventoryComponent::GetContainer(const EInventoryCategory InCategory) const
{
    return ItemsContainer[InCategory];
}

bool UDSPlayerInventoryComponent::AddItem(const FInventoryItem& InItem)
{
    if (const auto ItemInfo = InItem.GetInfo(this))
    {
        if (ItemInfo->Type == EInventoryItemType::Gold)
        {
            return AddGold(InItem.Amount);
        }
    }

    return Super::AddItem(InItem);
}

void UDSPlayerInventoryComponent::InitContainer()
{
    ForEachEnum<EInventoryCategory>([this](const EInventoryCategory Category)
    {
        ItemsContainer.Add(Category, FItemsContainer{DefaultSlotsAmount});
    });
}

bool UDSPlayerInventoryComponent::AddGold(const int32 InAmount)
{
    if (InAmount <= 0) { return false; }
    Gold += InAmount;

    OnGoldChanged.Broadcast(Gold);
    return true;
}

bool UDSPlayerInventoryComponent::RemoveGold(const int32 InAmount)
{
    if (InAmount <= 0 || InAmount > Gold) { return false; }
    Gold -= InAmount;

    OnGoldChanged.Broadcast(Gold);
    return true;
}

bool UDSPlayerInventoryComponent::IsIdentified(const FName& InItemID) const
{
    const auto InventoryManager = UDSInventoryManager::GetInventoryManager(this);
    if (!InventoryManager) return false;

    const auto ItemInfo = InventoryManager->GetItemInfo(InItemID);
    if (!ItemInfo) return false;

    return !ItemInfo->bIsUnidentified || IdentifiedItems.Contains(InItemID);
}

void UDSPlayerInventoryComponent::IdentifyItem(const FName& InItemID)
{
    if (InItemID.IsNone() || IsIdentified(InItemID)) return;

    const auto ItemInfo = UDSInventoryFunctionLibrary::GetItemInfo(this, InItemID);
    if (!ItemInfo) { return; }

    IdentifiedItems.AddUnique(InItemID);

    auto& Container = GetContainer(ItemInfo->Category);
    for (auto& Item : Container.Slots)
    {
        if (Item.ID == InItemID) { Item.bNotViewed = true; }
    }

    OnIdentifyItem.Broadcast(InItemID);
}

void UDSPlayerInventoryComponent::SetItemViewed(const int32 InItemIndex, EInventoryCategory InCategory)
{
    auto& Container = GetContainer(InCategory).Slots;
    if (Container.IsValidIndex(InItemIndex) && Container[InItemIndex].bNotViewed)
    {
        Container[InItemIndex].bNotViewed = false;
        OnItemViewed.Broadcast(InItemIndex, InCategory);
    }
}

bool UDSPlayerInventoryComponent::HaveNotViewedItems(EInventoryCategory InCategory) const
{
    const auto& Container = GetContainer(InCategory).Slots;

    for (const auto& Item : Container)
    {
        if (Item.bNotViewed) { return true; }
    }

    return false;
}
