// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventoryComponent.h"
#include "DSLootActorBase.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSInventoryComponent, All, All);

UDSInventoryComponent::UDSInventoryComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UDSInventoryComponent::BeginPlay()
{
    Super::BeginPlay();
    InitContainer();
}

void UDSInventoryComponent::InitContainer()
{
    ItemsContainer.Add(EInventoryCategory::Other, FItemsContainer{DefaultSlotsAmount});
}

FItemsContainer& UDSInventoryComponent::GetContainer(const EInventoryCategory InCategory) const
{
    return ItemsContainer[EInventoryCategory::Other];
}

bool UDSInventoryComponent::IsEmpty(const EInventoryCategory InCategory) const
{
    return GetContainer(InCategory).IsEmpty();
}

bool UDSInventoryComponent::IsFull(const EInventoryCategory InCategory) const
{
    return GetContainer(InCategory).IsFull();
}

bool UDSInventoryComponent::AddItem(const FInventoryItem& InItem)
{
    const auto ItemInfo = InItem.GetInfo(this);
    if (!ItemInfo)
    {
        UE_LOG(LogDSInventoryComponent, Warning, TEXT("Invalid ItemID: %s"), *InItem.ID.ToString());
        return false;
    }

    if (ItemInfo->bIsStackable ? AddStackableItem(InItem) : AddNotStackableItem(InItem))
    {
        OnAddItem.Broadcast(InItem);
        return true;
    }

    return false;
}

bool UDSInventoryComponent::DropItem(const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory)
{
    if (InItemIndex < 0 || InAmount < 1) { return false; }

    if (!LootActorClass)
    {
        UE_LOG(LogDSInventoryComponent, Error, TEXT("LootActorClass is NULL"));
        return false;
    }

    const auto& Container = GetContainer(InCategory);
    if (!Container.Slots.IsValidIndex(InItemIndex)) { return false; }

    const auto Item = Container.Slots[InItemIndex];

    const auto SpawnLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorForwardVector() * DropDistance;

    if (DropTrace(SpawnLocation))
    {
        OnDropItem.Broadcast(Item, false, FailDropMessage);
        return false;
    }

    const auto SpawnRotation = GetOwner()->GetActorRotation();
    const FTransform SpawnTransform(SpawnRotation, SpawnLocation);

    const auto DroppedItem = GetWorld()->SpawnActorDeferred<ADSLootActorBase>(LootActorClass, SpawnTransform);
    if (!DroppedItem) { return false; }

    //���� ������� ������� �� ������� �� �������� Destroy � ����� (���� �� ������ - �� ������� ����������)
    if (!RemoveItem(InItemIndex, InAmount, InCategory))
    {
        DroppedItem->Destroy();
        return false;
    }

    DroppedItem->Init(Item);
    DroppedItem->FinishSpawning(SpawnTransform);

    //TODO Move playing sound to SoundSubSystem
    if (const auto ItemInfo = Item.GetInfo(this))
    {
        UGameplayStatics::PlaySound2D(this, ItemInfo->DropSound);
    }

    OnDropItem.Broadcast(Item, true, {});
    return true;
}

bool UDSInventoryComponent::RemoveItem(const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory)
{
    if (InItemIndex < 0 || InAmount < 1) { return false; }

    if (FInventoryItem Item; GetItemByIndex(InItemIndex, InCategory, Item))
    {
        if (Item.IsStackable(this))
        {
            if (Item.Amount < InAmount) { return false; }

            Item.Amount -= InAmount;
            if (Item.Amount == 0)
            {
                GetContainer(InCategory).Slots.RemoveAt(InItemIndex);
            }
        }
        else
        {
            GetContainer(InCategory).Slots.RemoveAt(InItemIndex);
        }

        OnRemoveItem.Broadcast(InCategory);
        return true;
    }

    return false;
}

bool UDSInventoryComponent::GetItemByIndex(const int32 InItemIndex, EInventoryCategory InCategory, FInventoryItem& OutItem)
{
    auto& Container = GetContainer(InCategory);
    if (!Container.Slots.IsValidIndex(InItemIndex)) { return false; }

    OutItem = Container.Slots[InItemIndex];
    return true;
}

void UDSInventoryComponent::AddSlots(const EInventoryCategory InCategory, const int32 InAmount) const
{
    if (InAmount < 1) { return; }

    if (GetContainer(InCategory).AddSlots(InAmount))
    {
        OnSlotsAmountChanged.Broadcast(InCategory, InAmount); //TODO �������� ��� � ������� ������ (����� ���������� ��������� ::Other)
    }
}

bool UDSInventoryComponent::AddStackableItem(const FInventoryItem& InItem)
{
    const auto ItemInfo = InItem.GetInfo(this);
    auto& Container = GetContainer(ItemInfo->Category);

    const auto FoundItem = Container.Slots.FindByPredicate([&InItem](const FInventoryItem& Item) { return Item.ID == InItem.ID; });
    if (FoundItem)
    {
        FoundItem->Amount += InItem.Amount;
        return true;
    }
    else if (!Container.IsFull())
    {
        Container.Slots.Add(InItem);
        return true;
    }

    return false;
}

bool UDSInventoryComponent::AddNotStackableItem(const FInventoryItem& InItem)
{
    const auto ItemInfo = InItem.GetInfo(this);
    auto& Container = GetContainer(ItemInfo->Category);

    if (Container.IsFull()) { return false; }

    Container.Slots.Add(FInventoryItem{InItem.ID});
    return true;
}

bool UDSInventoryComponent::DropTrace(const FVector& InLocation) const
{
    FHitResult HitResult;
    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.AddIgnoredActor(GetOwner());

    auto BoxExtern = GetOwner()->GetSimpleCollisionCylinderExtent();
    BoxExtern.X = DropDistance * 0.6f;
    BoxExtern.Y *= 0.5f;

    return GetWorld()->SweepSingleByChannel(HitResult,
                                            InLocation,
                                            InLocation,
                                            GetOwner()->GetActorRotation().Quaternion(),
                                            ECC_Visibility,
                                            FCollisionShape::MakeBox(BoxExtern), CollisionQueryParams)
        && !HitResult.GetActor()->IsA(ADSLootActorBase::StaticClass());
}
