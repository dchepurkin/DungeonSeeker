// Created by Dmirtiy Chepurkin. All right reserved

#include "DSInventoryManager.h"
#include "DSInventoryTypes.h"
#include "DSPlayerInventoryComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogInventoryManager, All, All);

void UDSInventoryManager::Initialize(FSubsystemCollectionBase& Collection)
{
    Super::Initialize(Collection);

    if (!InventoryConfig.ItemsDataBase)
    {
        UE_LOG(LogInventoryManager, Error, TEXT("ItemsDataBase is NULL"));
    }
}

UDSInventoryManager* UDSInventoryManager::GetInventoryManager(const UObject* InWorldContext)
{
    if (!InWorldContext) return nullptr;

    const auto World = InWorldContext->GetWorld();
    return World ? World->GetSubsystem<UDSInventoryManager>() : nullptr;
}

UDSPlayerInventoryComponent* UDSInventoryManager::GetPlayerInventory(const UObject* InWorldContext)
{
    const auto InventoryManager = GetInventoryManager(InWorldContext);
    if (!InventoryManager) return nullptr;

    if (!InventoryManager->PlayerInventory)
    {
        if (const auto World = InWorldContext->GetWorld())
        {
            if (const auto PlayerController = World->GetFirstPlayerController())
            {
                if (const auto PlayerPawn = PlayerController->GetPawn())
                {
                    InventoryManager->PlayerInventory = PlayerPawn->FindComponentByClass<UDSPlayerInventoryComponent>();
                }
            }
        }
    }

    return InventoryManager->PlayerInventory;
}

bool UDSInventoryManager::AddItemToInventory(AActor* InActor, const FInventoryItem& InItem)
{
    const auto InventoryComponent = InActor ? InActor->GetComponentByClass<UDSInventoryComponent>() : nullptr;
    return InventoryComponent ? InventoryComponent->AddItem(InItem) : false;
}

bool UDSInventoryManager::RemoveItemFromInventory(AActor* InActor, const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory)
{
    const auto InventoryComponent = InActor ? InActor->GetComponentByClass<UDSInventoryComponent>() : nullptr;
    return InventoryComponent ? InventoryComponent->RemoveItem(InItemIndex, InAmount, InCategory) : false;
}

bool UDSInventoryManager::IsIdentified(const UObject* InWorldContext, const FName& InItemID)
{
    const auto PlayerInventoryComponent = GetPlayerInventory(InWorldContext);
    return PlayerInventoryComponent ? PlayerInventoryComponent->IsIdentified(InItemID) : false;
}

bool UDSInventoryManager::IsFullInventory(const AActor* InActor, const EInventoryCategory Category)
{
    const auto InventoryComponent = InActor ? InActor->FindComponentByClass<UDSInventoryComponent>() : nullptr;
    return InventoryComponent ? InventoryComponent->IsFull(Category) : false;
}

FText UDSInventoryManager::GetItemName(const UObject* InWorldContext, const FName& InItemID)
{
    if (const auto InventoryManager = GetInventoryManager(InWorldContext))
    {
        if (const auto ItemInfo = InventoryManager->GetItemInfo(InItemID))
        {
            return IsIdentified(InWorldContext, InItemID) ? ItemInfo->Name : ItemInfo->UnidentifiedName;
        }
    }

    return {};
}

FText UDSInventoryManager::GetItemDescription(const UObject* InWorldContext, const FName& InItemID)
{
    if (const auto InventoryManager = GetInventoryManager(InWorldContext))
    {
        if (const auto ItemInfo = InventoryManager->GetItemInfo(InItemID))
        {
            return IsIdentified(InWorldContext, InItemID) ? ItemInfo->Description : ItemInfo->UnidentifiedDescription;
        }
    }

    return {};
}

int32 UDSInventoryManager::GetItemPrice(const UObject* InWorldContext, const FName& InItemID)
{
    if (const auto InventoryManager = GetInventoryManager(InWorldContext))
    {
        if (const auto ItemInfo = InventoryManager->GetItemInfo(InItemID))
        {
            return IsIdentified(InWorldContext, InItemID) ? ItemInfo->Price : ItemInfo->UnidentifiedPrice;
        }
    }

    return 0;
}

FLinearColor UDSInventoryManager::GetQualityColor(const UObject* InWorldContext, const EEquipmentQuality InQuality)
{
    const auto Manager = GetInventoryManager(InWorldContext);
    return Manager
               ? Manager->InventoryConfig.QualityColors.Contains(InQuality)
                     ? Manager->InventoryConfig.QualityColors[InQuality]
                     : FLinearColor{}
               : FLinearColor{};
}

bool UDSInventoryManager::IsEquipment(const EInventoryCategory Category)
{
    return Category == EInventoryCategory::Armor || Category == EInventoryCategory::Jewellery || Category == EInventoryCategory::Weapon;
}

FInventoryItemInfo* UDSInventoryManager::GetItemInfo(const FName& InItemID) const
{
    if (InItemID.IsNone()) { return nullptr; }

    if (!ItemsInfoCache.Contains(InItemID))
    {
        if (const auto ItemsInfoDataTable = InventoryConfig.ItemsDataBase)
        {
            if (const auto FindedInfo = ItemsInfoDataTable->FindRow<FInventoryItemInfo>(InItemID, ""))
            {
                ItemsInfoCache.Add(InItemID, FindedInfo);
                return FindedInfo;
            }
        }
    }
    else
    {
        return ItemsInfoCache[InItemID];
    }

    return nullptr;
}
