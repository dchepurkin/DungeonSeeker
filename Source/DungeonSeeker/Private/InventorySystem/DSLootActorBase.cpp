// Created by Dmirtiy Chepurkin. All right reserved

#include "DSLootActorBase.h"

#include "DSLootAbility.h"
#include "DSAbilityFunctionLibrary.h"
#include "DSInventoryManager.h"
#include "NiagaraComponent.h"
#include "Algo/ForEach.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DSConstants.h"

#if WITH_EDITOR
#include "Components/BillboardComponent.h"
#endif

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSLootActorBase, All, All);

ADSLootActorBase::ADSLootActorBase()
{
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = false;

    Collision = CreateDefaultSubobject<USphereComponent>("Collision");
    Collision->SetCollisionProfileName("Loot");
    SetRootComponent(Collision);

    Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
    Mesh->SetupAttachment(Collision);
    Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    ParticleComponent = CreateDefaultSubobject<UNiagaraComponent>("ParticleComponent");
    ParticleComponent->SetupAttachment(Collision);

#if WITH_EDITORONLY_DATA
    BillboardComponent = CreateDefaultSubobject<UBillboardComponent>("BillboardComponent");
    BillboardComponent->SetupAttachment(Collision);
#endif
}

void ADSLootActorBase::BeginPlay()
{
    Update();
    Super::BeginPlay();

    SetActorTickEnabled(bIsEnabledRotation);
}

void ADSLootActorBase::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    Mesh->AddRelativeRotation(DeltaRotation * DeltaSeconds);
}

void ADSLootActorBase::Update()
{
    SetActorHiddenInGame(true);

    if (const auto ItemInfo = Item.GetInfo(this))
    {
        Mesh->SetStaticMesh(ItemInfo->Mesh);
        ParticleComponent->SetAsset(ItemInfo->Particle);


        TArray<UMeshComponent*> MeshComponents;
        GetComponents<UMeshComponent>(MeshComponents);
        Algo::ForEach(MeshComponents, [this, ItemInfo](UMeshComponent* MeshComponent)
        {
            MeshComponent->SetCustomDepthStencilValue(OutlineValue);

            if(UDSInventoryManager::IsEquipment(ItemInfo->Category))
            {
                MeshComponent->SetVectorParameterValueOnMaterials(MaterialColorParamName,
                                                                  FVector{UDSInventoryManager::GetQualityColor(this, ItemInfo->Quality)});
            }
        });
    }

    SnapToFloor();
}

void ADSLootActorBase::SnapToFloor()
{
    if (GetWorld())
    {
        FHitResult HitResult;
        const FVector EndTrace = GetActorLocation() - FVector{0.f, 0.f, DropZTraceDistance};

        FCollisionQueryParams CollisionQueryParams;
        CollisionQueryParams.AddIgnoredActor(this);

        FCollisionResponseParams CollisionResponseParams;
        CollisionResponseParams.CollisionResponse.SetResponse(PlayerTraceChannel, ECR_Ignore);
        CollisionResponseParams.CollisionResponse.SetResponse(LootTraceChannel, ECR_Ignore);
        CollisionResponseParams.CollisionResponse.SetResponse(InteractionTraceChannel, ECR_Ignore);

        GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), EndTrace, ECC_Visibility, CollisionQueryParams, CollisionResponseParams);
        SetActorLocation(HitResult.bBlockingHit ? HitResult.Location : EndTrace);
    }

    if (const auto StaticMesh = Mesh->GetStaticMesh())
    {
        const auto ScaledBoxExtern = StaticMesh->GetBounds().BoxExtent * Mesh->GetRelativeScale3D();
        const auto ZPosition = ScaledBoxExtern.Z + ZOffset;

        //������ ����� �������� ����� � 1.5 ���� ������ ��� ������ ����
        constexpr float CollisionScale = 1.5f;
        Collision->SetSphereRadius(ScaledBoxExtern.Z * CollisionScale);

        AddActorWorldOffset(FVector{0.f, 0.f, ZPosition});
    }

    SetActorHiddenInGame(false);
}

void ADSLootActorBase::TakeItem(AActor* WhoTake)
{
    if (UDSInventoryManager::AddItemToInventory(WhoTake, Item))
    {
        const auto ItemInfo = Item.GetInfo(this);
        UGameplayStatics::PlaySound2D(this, ItemInfo->TakeSound);
        OnItemTaken.Broadcast(WhoTake);
        Destroy();
    }
}

void ADSLootActorBase::Init(const FInventoryItem& InItem)
{
    Item = InItem;
}

FText ADSLootActorBase::GetInteractionActorName_Implementation()
{
    return UDSInventoryManager::GetItemName(this, Item.ID);
}

void ADSLootActorBase::AbilityTriggered(UDSAbilityBase* Ability, AActor* AbilityUser, AActor* AbilityTarget)
{
    TakeItem(AbilityUser);
}

void ADSLootActorBase::Interact_Implementation(AActor* Interactor)
{
    if (const auto Ability = UDSAbilityFunctionLibrary::StartAbilityDeferred(TakeItemAbility, Interactor))
    {
        Ability->OnAbilityTriggered.AddDynamic(this, &ThisClass::AbilityTriggered);
        UDSAbilityFunctionLibrary::FinishStartingAbility(Ability, this);
    }
}

bool ADSLootActorBase::CanInteract_Implementation(AActor* Interactor, FText& FailMessage)
{
    const auto ItemInfo = Item.GetInfo(this);
    const auto CanInteract = ItemInfo ? !UDSInventoryManager::IsFullInventory(Interactor, ItemInfo->Category) : false;

    if (!CanInteract) { FailMessage = FullInventoryMessage; }
    return CanInteract;
}
