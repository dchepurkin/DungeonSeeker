// Created by Dmirtiy Chepurkin. All right reserved


#include "DSPlayerController.h"

#include "EnhancedInputComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSPlayerController, All, All);

void ADSPlayerController::BeginPlay()
{
    Super::BeginPlay();
    SetInputMode(FInputModeGameOnly());
}

void ADSPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    if (const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
    {
        EnhancedInputComponent->BindAction(InventoryAction, ETriggerEvent::Started, this, &ThisClass::Test);
    }
}

void ADSPlayerController::Test()
{
    UE_LOG(LogDSPlayerController, Warning, TEXT("Inventory"));
}

void ADSPlayerController::TeleportPlayerPawn(const FTransform& InTransform)
{
    const auto PlayerPawn = GetPawn();
    if (!PlayerPawn) return;

    PlayerPawn->SetActorTransform(InTransform);
    PlayerPawn->EnableInput(this);
    SetControlRotation(InTransform.Rotator());
}

void ADSPlayerController::DisableInput(APlayerController* PlayerController)
{
    Super::DisableInput(PlayerController);

    if (const auto PlayerPawn = GetPawn())
    {
        PlayerPawn->DisableInput(this);
    }
}

void ADSPlayerController::EnableInput(APlayerController* PlayerController)
{
    Super::EnableInput(PlayerController);

    if (const auto PlayerPawn = GetPawn())
    {
        PlayerPawn->EnableInput(this);
    }
}

void ADSPlayerController::MoveTo(const FVector InTargetLocation, const float SuccessMinDistance)
{
    if (IsInSuccessDistance(InTargetLocation, SuccessMinDistance))
    {
        OnMoveFinished.Broadcast(true);
        return;
    }

    if (!GetWorld()) return;

    bIsMoving = true;

    UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, InTargetLocation);

    const auto Delegate = FTimerDelegate::CreateUObject(this, &ThisClass::OnMoving_Callback, InTargetLocation, SuccessMinDistance);
    GetWorld()->GetTimerManager().SetTimer(MoveToTimerHandle, Delegate, 0.1f, true);

    OnMovingStarted.Broadcast(InTargetLocation);
}

void ADSPlayerController::StopMovement()
{
    Super::StopMovement();

    FinishMoving(false);
}

void ADSPlayerController::OnMoving_Callback(const FVector InTargetLocation, const float TrueDistance)
{
    //��������� ����������� ���������
    if (IsInSuccessDistance(InTargetLocation, TrueDistance))
    {
        FinishMoving(true);
    }
    //���� �� ������ ���� �� �������� �������� � ����
    else if (!UAIBlueprintHelperLibrary::GetCurrentPath(this))
    {
        FinishMoving(false);
    }
}

bool ADSPlayerController::IsInSuccessDistance(const FVector& InTargetLocation, const float SuccessMinDistance) const
{
    return FVector::Distance(GetPawn()->GetActorLocation(), InTargetLocation) <= SuccessMinDistance;
}

void ADSPlayerController::FinishMoving(const bool bIsSuccess)
{
    if (!GetWorld() || !bIsMoving) return;

    GetWorld()->GetTimerManager().ClearTimer(MoveToTimerHandle);
    bIsMoving = false;
    OnMoveFinished.Broadcast(bIsSuccess);
}
