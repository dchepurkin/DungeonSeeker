// Created by Dmirtiy Chepurkin. All right reserved

#include "Controllers/DSNewCharacterController.h"

void ADSNewCharacterController::BeginPlay()
{
    Super::BeginPlay();

    SetInputMode(FInputModeUIOnly{});
    SetShowMouseCursor(true);
}
