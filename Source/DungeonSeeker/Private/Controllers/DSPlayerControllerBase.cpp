// Created by Dmirtiy Chepurkin. All right reserved

#include "Controllers/DSPlayerControllerBase.h"

void ADSPlayerControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if(!PlayerCameraManager) return;

    DisableInput(this);
    PlayerCameraManager->SetManualCameraFade(1.f, FLinearColor::Black, false);
    GetWorldTimerManager().SetTimer(CameraFadeTimerHandle, this, &ThisClass::CameraFadeTimer_Callback, CameraFadeTime);
}

void ADSPlayerControllerBase::CameraFadeTimer_Callback()
{
    EnableInput(this);
    if(!PlayerCameraManager) return;
    PlayerCameraManager->StartCameraFade(1.f, 0.f, CameraFadeOutTime, FLinearColor::Black);
}
