// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.generated.h"

namespace DS
{
}


UENUM(BlueprintType)
enum class EDSGender : uint8
{
    None,
    Male,
    Female
};

USTRUCT(BlueprintType)
struct FDSBodyPart
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    USkeletalMesh* Mesh = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    FName Tag{};
};

USTRUCT(BlueprintType)
struct FDSBody
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker", meta=(ShowOnlyInnerProperties))
    TArray<FDSBodyPart> Parts;
};

USTRUCT(BlueprintType)
struct FDSBodyPartParams
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bHideHead = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bHideEyebrow = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bHideBeard = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bHideHair = false;

    static const FDSBodyPartParams ShowAll;
    static const FDSBodyPartParams HideAll;
    static const FDSBodyPartParams HideHairOnly;
    static const FDSBodyPartParams HideBeardOnly;
    static const FDSBodyPartParams HideHairAndBeard;
};

UENUM(BlueprintType)
enum class EDSInteractionType : uint8
{
    None,
    Item,
    NPC,
    EnemyNPC
};

UENUM(BlueprintType)
enum class EDSUIType : uint8
{
    Gameplay,
    MainMenu,
    Inventory,
    Skills,
};

USTRUCT(BlueprintType)
struct FLoadingScreenInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
    UTexture2D* LoadingScreenImage = nullptr;
};
