// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"

namespace DS
{
    template <typename EnumType, typename FunctionType>
    void ForEachEnum(FunctionType InFunction)
    {
        const auto Enum = StaticEnum<EnumType>();
        for (uint8 i = 0; i < Enum->NumEnums() - 1; ++i)
        {
            InFunction(static_cast<EnumType>(Enum->GetValueByIndex(i)));
        }
    }
}
