// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"

namespace DS
{
    constexpr float DefaultMinInteractionDistance = 120.f;

    constexpr int32 MinCharacterNameLength = 2;

    //��������� ������ ��������
    constexpr auto PlayerTraceChannel = ECC_GameTraceChannel1;
    constexpr auto LootTraceChannel = ECC_GameTraceChannel2;
    constexpr auto InteractionTraceChannel = ECC_GameTraceChannel3;
    constexpr auto LaserTraceChannel = ECC_GameTraceChannel4;
    constexpr auto IKTraceChannel = ECC_GameTraceChannel5;

    const FName CostumeTag{"Costume"};
    const FName HelmTag{"Helm"};
    const FName HeadTag{"Head"};
    const FName HairTag{"Hair"};
    const FName BeardTag{"Beard"};
    const FName EyebrowTag{"Eyebrow"};
    const FName HandsTag{"Hands"};
    const FName TorsTag{"Tors"};
    const FName LegsTag{"Legs"};
    const FName ShouldersTag{"Shoulders"};

    const FName HairColorParamName{"Color_Hair"};
    const FName ScarColorParamName{"Color_Scar"};
    const FName BodyArtColorParamName{"Color_BodyArt"};
    const FName SkinColorParamName{"Color_Skin"};
    const FName StubbleColorParamName{"Color_Stubble"};
}
