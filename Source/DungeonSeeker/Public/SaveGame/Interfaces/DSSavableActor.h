// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableObject.h"
#include "DSSavableActor.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Savable Actor")
class UDSSavableActor : public UDSSavableObject
{
    GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSSavableActor : public IDSSavableObject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
    void OnActorLoaded();

private:
    virtual void OnActorLoaded_Implementation() {}
};
