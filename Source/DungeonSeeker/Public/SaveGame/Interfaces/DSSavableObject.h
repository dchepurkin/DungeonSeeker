// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DSSavableObject.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Savable Object")
class UDSSavableObject : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSSavableObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
	void OnGameLoaded();

private:
	virtual void OnGameLoaded_Implementation() { }
};
