// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableObject.h"
#include "DSSavableComponent.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Savable Component")
class UDSSavableComponent : public UDSSavableObject
{
    GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSSavableComponent : public IDSSavableObject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
    void OnComponentLoaded();

private:
    virtual void OnComponentLoaded_Implementation() {}
};