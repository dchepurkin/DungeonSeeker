// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableObject.h"
#include "DSSavablePlayer.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Savable Player")
class UDSSavablePlayer : public UDSSavableObject
{
    GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSSavablePlayer : public IDSSavableObject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
    void OnPlayerLoaded();

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
    FName GetCharacterName();

private:
    virtual void OnPlayerLoaded_Implementation() {}

    virtual FName GetCharacterName_Implementation() = 0;
};
