// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableObject.h"
#include "DSAccountSavableActor.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Account Savable Actor")
class UDSAccountSavableActor : public UDSSavableObject
{
    GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSAccountSavableActor : public IDSSavableObject
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|SaveGame")
    void OnActorLoaded();

private:
    virtual void OnActorLoaded_Implementation() {}
};