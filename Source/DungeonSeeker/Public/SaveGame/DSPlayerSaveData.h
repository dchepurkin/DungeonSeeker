// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.h"
#include "GameFramework/SaveGame.h"
#include "DSPlayerSaveData.generated.h"

UCLASS()
class DUNGEONSEEKER_API UDSPlayerSaveData : public USaveGame
{
    GENERATED_BODY()

public:
    bool Save(const FPlayerSaveData& InPlayerData);

    bool GetData(const FName& InCharacterName, FPlayerSaveData& OutData) const;

    bool Remove(const FName& InCharacterName);

    bool IsCharacterExist(const FName& InCharacterName) const;

    void GetSavedCharacters(TArray<FPlayerSaveData>& OutCharactersNames);

protected:
    UPROPERTY()
    TMap<FName, FPlayerSaveData> CharactersData;
};
