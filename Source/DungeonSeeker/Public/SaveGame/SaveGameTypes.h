//Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.generated.h"

const FString DefaultHomeDataSlotName{"HomeSlot"};
const FString DefaultCharacterDataSlotName{"CharacterSlot"};

USTRUCT(BlueprintType)
struct FSaveData
{
    GENERATED_BODY()

    UPROPERTY()
    FName Name{};

    UPROPERTY()
    TArray<uint8> ByteData;
};

USTRUCT(BlueprintType)
struct FActorSaveData : public FSaveData
{
    GENERATED_BODY()

    UPROPERTY()
    FTransform Transform = FTransform::Identity;

    UPROPERTY()
    TSubclassOf<AActor> Class = nullptr;

    UPROPERTY()
    TArray<FSaveData> ComponentsData{};
};

USTRUCT(BlueprintType)
struct FPlayerSaveData : public FSaveData
{
    GENERATED_BODY()

    UPROPERTY()
    FName PlayerCharacterName{};

    UPROPERTY()
    TArray<FSaveData> ComponentsData{};

    /*
     ���� �������� ���� ��� ���������� ������������ ���������
     */
};

USTRUCT(BlueprintType)
struct FHomeSaveData
{
    GENERATED_BODY()

    UPROPERTY()
    TArray<FActorSaveData> Data;
};

USTRUCT(BlueprintType)
struct FAccountSaveData
{
    GENERATED_BODY()

    UPROPERTY()
    TArray<FActorSaveData> Data;
};
