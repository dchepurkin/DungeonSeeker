// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableComponent.h"
#include "DSHomeSaveData.h"
#include "DSPlayerSaveData.h"
#include "SaveGameTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "DSSaveGameManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameSaved);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameLoaded);

UCLASS(DisplayName="Save Game Manager")
class DUNGEONSEEKER_API UDSSaveGameManager : public UGameInstanceSubsystem
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnGameLoaded OnGameLoaded;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnGameSaved OnGameSaved;

    //USubsystem interface
    virtual void Initialize(FSubsystemCollectionBase& Collection) override;
    //~USubsystem interface

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool SaveGame();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool RemoveSaveGame(const FName& InCharacterName);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool SavePlayer();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void LoadGame(const FName& InCharacterName);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void LoadPlayer(const FName& InCharacterName);

    UFUNCTION(Blueprintable, BlueprintPure, Category="DungeonSeeker")
    void GetSavedCharacters(TArray<FPlayerSaveData>& OutCharactersData) const;

    UFUNCTION(Blueprintable, BlueprintPure, Category="DungeonSeeker")
    bool IsCharacterExist(const FName& InCharacterName) const;

    UFUNCTION(Blueprintable, BlueprintPure, Category="DungeonSeeker")
    static UDSSaveGameManager* GetSaveGameManager() { return Manager; }

private:
    inline static UDSSaveGameManager* Manager = nullptr;

    UPROPERTY()
    TArray<UObject*> LoadedObjects;

    UPROPERTY()
    mutable UDSHomeSaveData* HomeSaveData;

    UPROPERTY()
    mutable UDSPlayerSaveData* PlayerSaveData;

    FORCEINLINE UDSPlayerSaveData* GetPlayerSaveData() const
    {
        if (!PlayerSaveData)
        {
            PlayerSaveData = GetSaveGameObject<UDSPlayerSaveData>(DefaultCharacterDataSlotName);
        }
        return PlayerSaveData;
    }

    FORCEINLINE UDSHomeSaveData* GetHomeSaveData() const
    {
        if (!HomeSaveData)
        {
            HomeSaveData = GetSaveGameObject<UDSHomeSaveData>(DefaultHomeDataSlotName);
        }
        return HomeSaveData;
    }

    void GetPlayerData(APawn* InPawn, FPlayerSaveData& OutData);

    void GetActorData(AActor* InActor, FActorSaveData& OutData);

    bool SavePlayerData();

    bool SaveHomeData();

    void LoadPlayerData(const FName& InCharacterName);

    void LoadHomeData(const FName& InCharacterName);

    void LoadActors(TArray<AActor*>& AllActors, const TArray<FActorSaveData>& SaveData);

    void LoadActorData(AActor* InActor, const FActorSaveData& InData);

    void GetByteData(UObject* InObject, FSaveData& OutData) const;

    void LoadByteData(UObject* InObject, const FSaveData& InData) const;

    void DispatchOnGameLoaded();

    template <class SaveGameObjectClass>
    SaveGameObjectClass* GetSaveGameObject(const FString& InSlotName) const
    {
        return UGameplayStatics::DoesSaveGameExist(InSlotName, 0)
                   ? Cast<SaveGameObjectClass>(UGameplayStatics::LoadGameFromSlot(InSlotName, 0))
                   : Cast<SaveGameObjectClass>(UGameplayStatics::CreateSaveGameObject(SaveGameObjectClass::StaticClass()));
    }

    template <typename FunctionType>
    void ForEachSavableComponent(AActor* InActor, FunctionType InFunc) const
    {
        for (UActorComponent* Component : InActor->GetComponents())
        {
            if (Component && Component->Implements<UDSSavableComponent>())
            {
                InFunc(Component);
            }
        }
    }

    template <typename DataType>
    void GetComponentsData(AActor* InActor, DataType& InData)
    {
        FActorSaveData ComponentSaveData;
        this->ForEachSavableComponent(InActor, [&](UActorComponent* InComponent)
        {
            this->GetByteData(InComponent, ComponentSaveData);
            ComponentSaveData.Name = InComponent->GetFName();

            InData.ComponentsData.Add(ComponentSaveData);
        });
    }

    template <typename DataType>
    void LoadComponentsData(AActor* InActor, const DataType& InData)
    {
        this->ForEachSavableComponent(InActor, [&](UActorComponent* InComponent)
        {
            const FSaveData* FoundComponentData = InData.ComponentsData.FindByPredicate(
                [InComponent](const FSaveData& ComponentData)
                {
                    return ComponentData.Name == InComponent->GetFName();
                });
            if (!FoundComponentData) return;

            this->LoadByteData(InComponent, *FoundComponentData);
            this->LoadedObjects.Add(InComponent);
            IDSSavableComponent::Execute_OnComponentLoaded(InComponent);
        });
    }

    APawn* GetPlayerPawn() const;
};
