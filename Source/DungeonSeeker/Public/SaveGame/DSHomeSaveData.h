// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.h"
#include "GameFramework/SaveGame.h"
#include "DSHomeSaveData.generated.h"

UCLASS()
class DUNGEONSEEKER_API UDSHomeSaveData : public USaveGame
{
    GENERATED_BODY()

public:
    bool Save(const FName& InCharacterName, const FHomeSaveData& InHomeData, const FAccountSaveData& InAccountData);

    bool Remove(const FName& InCharacterName);

    bool GetHomeData(const FName& InCharacterName, FHomeSaveData& OutData);

    const FAccountSaveData& GetAccountData() const { return AccountData; }

protected:
    UPROPERTY()
    TMap<FName, FHomeSaveData> HomesData;

    //����� ������ �� ���� ����������
    UPROPERTY()
    FAccountSaveData AccountData;
};
