// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSPlayerControllerBase.h"
#include "DSNewCharacterController.generated.h"

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSNewCharacterController : public ADSPlayerControllerBase
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;
};
