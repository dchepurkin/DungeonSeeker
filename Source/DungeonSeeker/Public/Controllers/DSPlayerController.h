// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSPlayerControllerBase.h"
#include "DSPlayerController.generated.h"

class UInputMappingContext;
class UInputAction;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMoveFinished, const bool, IsSuccsses);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMovingStarted, FVector, InTargetLocation);

UCLASS()
class DUNGEONSEEKER_API ADSPlayerController : public ADSPlayerControllerBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnMovingStarted OnMovingStarted;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnMoveFinished OnMoveFinished;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|UIInput")
    UInputMappingContext* UIInputMapping;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|UIInput")
    UInputAction* InventoryAction;

    void TeleportPlayerPawn(const FTransform& InTransform);

    virtual void DisableInput(APlayerController* PlayerController) override;

    virtual void EnableInput(APlayerController* PlayerController) override;

    //SuccessMinDistance - ��������� �� ������� ��������� ��� �������� ��������� �������
    void MoveTo(const FVector InTargetLocation, const float SuccessMinDistance);

    virtual void StopMovement() override;

protected:
    virtual void BeginPlay() override;

    virtual void SetupInputComponent() override;

    void Test();

private:
    bool bIsMoving = false;

    FTimerHandle MoveToTimerHandle;

    void FinishMoving(const bool bIsSuccess);

    void OnMoving_Callback(const FVector InTargetLocation, const float TrueDistance);

    bool IsInSuccessDistance(const FVector& InTargetLocation, const float SuccessMinDistance) const;
};
