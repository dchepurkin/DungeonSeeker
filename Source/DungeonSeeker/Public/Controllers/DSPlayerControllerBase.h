// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "DSPlayerControllerBase.generated.h"

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSPlayerControllerBase : public APlayerController
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditDefaultsOnly, Category="DungeonSeeker|Camera")
    float CameraFadeTime = 0.5f;

    UPROPERTY(EditDefaultsOnly, Category="DungeonSeeker|Camera")
    float CameraFadeOutTime = 3.f;

    FTimerHandle CameraFadeTimerHandle;

    virtual void OnPossess(APawn* InPawn) override;

    virtual void CameraFadeTimer_Callback();
};
