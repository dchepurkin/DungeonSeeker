// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSBorderSwitcherBase.generated.h"

class UDSClickBorder;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSelectionChanged, UDSClickBorder*, SelectedBorder);

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSBorderSwitcherBase : public UUserWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnSelectionChanged OnSelectionChanged;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void Init();

protected:
    UPROPERTY()
    TArray<UDSClickBorder*> Borders;

    UPROPERTY()
    UDSClickBorder* CurrentFocusedBorder;

    UFUNCTION()
    virtual void OnBorderFocused(UDSClickBorder* InFocusedBorder);

    virtual void NativeConstruct() override;

private:
    virtual void InitBorders(TArray<UDSClickBorder*>& OutBorders);

    void InitBindings();
};
