// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSBorderSwitcherBase.h"
#include "DSInventoryTypes.h"
#include "DSItemsCategorySwitcherWidget.generated.h"

class UDSClickBorder;
class UDSInventoryCategoryBorder;
class UHorizontalBox;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInventorySwitch, EInventoryCategory, Category);

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSItemsCategorySwitcherWidget : public UDSBorderSwitcherBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnInventorySwitch OnInventorySwitch;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    EInventoryCategory GetCategory() const;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void UpdateCategoryViewedIcon(const EInventoryCategory InCategory);

    void ChangeCategory(UDSInventoryCategoryBorder* InItemTypeBorder) const;

protected:
    UPROPERTY(meta=(BindWidget))
    UHorizontalBox* ButtonsBox;

    virtual void NativeConstruct() override;

    virtual void NativeDestruct() override;

    virtual void OnBorderFocused(UDSClickBorder* InFocusedBorder) override;

private:
    virtual void InitBorders(TArray<UDSClickBorder*>& OutBorders) override;

    UFUNCTION()
    void ItemViewed_Callback(int32 InItemIndex, EInventoryCategory InCategory);
};
