// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryTypes.h"
#include "Widgets/DSWindowWidget.h"
#include "DSInventoryWidget.generated.h"

class ADSPlayerRenderActor;
class ADSCharacterRender;
class UTextBlock;
class UScrollBox;
class UDSInventorySlotWidget;
class UDSPlayerInventoryComponent;
class UUniformGridPanel;
class UDSItemsCategorySwitcherWidget;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSInventoryWidget : public UDSWindowWidget
{
    GENERATED_BODY()

public:
    virtual void NativeConstruct() override;

    virtual void Close() override;

protected:
    UPROPERTY(meta=(BindWidget))
    UDSItemsCategorySwitcherWidget* CategorySwitcher;

    UPROPERTY(meta=(BindWidget))
    UUniformGridPanel* InventorySlotsGrid;

    UPROPERTY(meta=(BindWidget))
    UScrollBox* InventorySlotsScrollBox;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* GoldTextBlock;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<UDSInventorySlotWidget> InventorySlotWidgetClass;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    int32 SlotsInRow = 5;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<ADSPlayerRenderActor> CharacterRenderClass;

private:
    UPROPERTY()
    UDSPlayerInventoryComponent* InventoryComponent;

    UPROPERTY()
    ADSPlayerRenderActor* CharacterRender;

    UPROPERTY()
    TArray<UDSInventorySlotWidget*> SlotsWidgets;

    void InitInventory() const;

    void UpdateInventory(EInventoryCategory InCategory);

    int32 GetRowByIndex(const int32 InIndex) const;

    int32 GetColumnByIndex(const int32 InIndex) const;

    UFUNCTION()
    void OnRemoveItem_Callback(EInventoryCategory InCategory);

    UFUNCTION()
    void OnAddItem_Callback(const FInventoryItem& InItem);

    UFUNCTION()
    void InventorySwitch_Callback(EInventoryCategory InCategory);

    UFUNCTION()
    void OnSlotsAmountChanged_Callback(EInventoryCategory Category, int Amount);

    UFUNCTION()
    void UpdateGold(const int32 InGoldAmount);

    UFUNCTION()
    void OnIdentifyItem_Callback(const FName& InItemID);

    UDSInventorySlotWidget* CreateSlot(const int32 InIndex);
};
