// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSInventoryTypes.h"
#include "DSInventorySlotWidget.generated.h"

class UDSItemToolTipWidget;
class UDSHoverBorder;
class UImage;
class UOverlay;
class UTextBlock;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSInventorySlotWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    virtual void NativeOnInitialized() override;

    const FInventoryItem& GetItem() const { return Item; }

    int32 GetSlotIndex() const { return SlotIndex; }

    void RemoveItem() const;

    void DropItem() const;

    void InitSlot(const int32 InSlotIndex, const FInventoryItem& InSlotInfo = {});

protected:
    UPROPERTY(meta=(BindWidget))
    UDSHoverBorder* FocusBorder;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* AmountTextBlock;

    UPROPERTY(meta=(BindWidget))
    UOverlay* AmountOverlay;

    UPROPERTY(meta=(BindWidget))
    UImage* ItemIcon;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* UnidentifiedIcon;

    /*UPROPERTY(EditAnywhere, Category="WrongSide")
    TSubclassOf<UDSItemToolTipWidget> ToolTipWidgetClass;*/

    UPROPERTY(EditAnywhere, Category="DungeonSeeker")
    FLinearColor NotViewedColor = FLinearColor::Yellow;

    virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

    virtual FReply NativeOnMouseButtonDoubleClick(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

    virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

private:
    int32 SlotIndex = INDEX_NONE;

    FInventoryItem Item;

    void UpdateSlot();

    bool IsEmpty() const { return SlotIndex == INDEX_NONE; }

    bool CanInteract();

    UFUNCTION()
    UWidget* GetToolTipWidget();
};
