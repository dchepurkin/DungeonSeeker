// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSTooltipWidget.generated.h"

class UTextBlock;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSTooltipWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetText(const FText& InText) const;

protected:
    UPROPERTY(Transient, meta=(BindWidgetAnimOptional))
    UWidgetAnimation* ShowAnimation;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* TooltipTextBlock;

    virtual void NativeConstruct() override;
};
