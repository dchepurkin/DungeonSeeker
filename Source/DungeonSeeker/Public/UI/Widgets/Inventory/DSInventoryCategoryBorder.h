// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryTypes.h"
#include "PaletteComponents/DSClickBorder.h"
#include "DSInventoryCategoryBorder.generated.h"

class UTextBlock;
class UDSTooltipWidget;

UCLASS()
class DUNGEONSEEKER_API UDSInventoryCategoryBorder : public UDSClickBorder
{
    GENERATED_BODY()

public:
    EInventoryCategory GetCategory() const { return InventoryCategory; }

    void UpdateNotViewedIconVisibility() const;

    virtual void SynchronizeProperties() override;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    EInventoryCategory InventoryCategory;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Tooltip")
    TSubclassOf<UDSTooltipWidget> TooltipWidgetClass;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Tooltip", DisplayName="Tooltip Text")
    FText DSTooptipText;

    virtual void OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent) override;

private:
    UPROPERTY()
    UTextBlock* NotViewedIcon;

    UWidget* GetTooltipWidget();
};
