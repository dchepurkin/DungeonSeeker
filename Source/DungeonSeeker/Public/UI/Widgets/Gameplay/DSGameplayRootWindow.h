// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "DSWindowWidget.h"
#include "DSGameplayRootWindow.generated.h"

class UDSAimWidget;
class UDSInteractionWidget;
class UDSTakeItemBoxWidget;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSGameplayRootWindow : public UDSWindowWidget
{
    GENERATED_BODY()

public:
    virtual void UpdateWidget() override;

protected:
    /*UPROPERTY(meta=(BindWidget))
    UDSTakeItemBoxWidget* TakeItemBox;*/

    UPROPERTY(meta=(BindWidget))
    UDSAimWidget* AimWidget;

    UPROPERTY(meta=(BindWidget))
    UDSInteractionWidget* InteractionWidget;

    virtual void NativeOnInitialized() override;

    virtual void NativeConstruct() override;

private:
    UFUNCTION()
    void OnVisibilityChanged_Callback(ESlateVisibility InVisibility);

    UFUNCTION()
    void OnActorBeginFocused_Callback(AActor* FocusedActor, EDSInteractionType InteractionType);

    UFUNCTION()
    void OnClearFocus_Callback();
};
