// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSInteractionWidget.generated.h"

class UTextBlock;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSInteractionWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    void Show(AActor* InActor);

    void Hide();

    void Update();

protected:
    UPROPERTY(Transient, meta=(BindWidgetAnim))
    UWidgetAnimation* ShowAnimation;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* ActorNameTextBlock;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* InteractionNameTextBlock;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* InteractionKeyName;

private:
    UPROPERTY()
    AActor* FocusedActor = nullptr;

    FTimerHandle UpdateTimerHandle;

    const float UpdateTimerRate = 0.1f;

    void OnChangeKey_Callback(const FName& InActionName, const FKey& InNewKey);

    void SetUpdateTimerEnabled(const bool IsEnabled);
};
