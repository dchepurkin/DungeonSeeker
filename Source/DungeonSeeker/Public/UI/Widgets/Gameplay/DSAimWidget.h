// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSAimWidget.generated.h"

class UBorder;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSAimWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    void SetNormalColor() const;

    void SetBattleColor() const;

    void SetAimFocus();

    void ClearAimFocus();

protected:
    UPROPERTY(Transient, meta=(BindWidgetAnim))
    UWidgetAnimation* FocusAnimation;

    UPROPERTY(meta=(BindWidget))
    UBorder* AimBox;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    FLinearColor NormalColor = FLinearColor::White;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    FLinearColor BattleColor = FLinearColor::Red;

private:
    bool bIsFocused = false;
};
