// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Widgets/DSWindowWidget.h"
#include "DSSelectCharacterWidget.generated.h"

class UDSButton;
class UDSInputModeSwitcherWidget;
struct FPlayerSaveData;

class UTextBlock;
class UDSCharactersListWidget;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSSelectCharacterWidget : public UDSWindowWidget
{
    GENERATED_BODY()

protected:
    UPROPERTY(meta=(BindWidget))
    UDSCharactersListWidget* CharactersListWidget;

    UPROPERTY(meta=(BindWidget))
    UDSInputModeSwitcherWidget* CharacterRotationWidget;

    UPROPERTY(meta=(BindWidget))
    UDSButton* StartGameButton;

    virtual void NativeOnInitialized() override;

private:
    UFUNCTION()
    void OnCharacterSelected(const FPlayerSaveData& CharacterData);

    UFUNCTION()
    void OnCharacterListUpdated(int32 InCharactersAmount);

    UFUNCTION()
    void StartGame();

    void LoadPlayer(const FName& InCharacterName);
};
