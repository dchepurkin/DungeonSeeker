// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "SaveGameTypes.h"
#include "Blueprint/UserWidget.h"
#include "DSCharacterSlotWidget.generated.h"

class UDSClickBorder;
class UTextBlock;
class UDSCharacterSlotWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterSlotSelected, UDSCharacterSlotWidget*, CharacterSlot);

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSCharacterSlotWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnCharacterSlotSelected OnCharacterSlotSelected;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetData(const FPlayerSaveData& Data);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetFocused(bool IsFocused);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FPlayerSaveData& GetCharacterData() const { return CharacterData; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FName& GetCharacterName() const { return CharacterData.PlayerCharacterName; }

protected:
    UPROPERTY(meta=(BindWidget))
    UDSClickBorder* Border;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* CharacterNameText;

    virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

private:
    FPlayerSaveData CharacterData;
};
