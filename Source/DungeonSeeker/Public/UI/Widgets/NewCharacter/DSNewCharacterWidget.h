// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "Widgets/DSWindowWidget.h"
#include "DSNewCharacterWidget.generated.h"

class UDSDialogWindowWidget;
class UEditableText;
class UDSButton;
class UDSDoubleColorSelectorWidget;
class UDSColorSelectorWidget;
class UHorizontalBox;
class ADSSavableModularCharacter;
class UDSInputModeSwitcherWidget;
class UDSSwitcher;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSNewCharacterWidget : public UDSWindowWidget
{
    GENERATED_BODY()

public:
    virtual void NativeConstruct() override;

    bool GetIsCharacterCreated() const { return bIsCharacterCreated; }

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<UDSDialogWindowWidget> DialogWindowWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    FText CreateCharacterFailMessage{};

    UPROPERTY(meta=(BindWidget))
    UDSInputModeSwitcherWidget* CharacterRotationWidget;

    UPROPERTY(meta=(BindWidget))
    UDSSwitcher* GenderSwitcherWidget;

    UPROPERTY(meta=(BindWidget))
    UDSSwitcher* HairSwitcherWidget;

    UPROPERTY(meta=(BindWidget))
    UDSSwitcher* EyebrowsSwitcherWidget;

    UPROPERTY(meta=(BindWidget))
    UDSSwitcher* HeadsSwitcherWidget;

    UPROPERTY(meta=(BindWidget))
    UHorizontalBox* BeardBox;

    UPROPERTY(meta=(BindWidget))
    UDSSwitcher* BeardsSwitcherWidget;

    UPROPERTY(meta=(BindWidget))
    UDSColorSelectorWidget* HairColorSelector;

    UPROPERTY(meta=(BindWidget))
    UDSColorSelectorWidget* ScarColorSelector;

    UPROPERTY(meta=(BindWidget))
    UDSColorSelectorWidget* BodyArtColorSelector;

    UPROPERTY(meta=(BindWidget))
    UDSDoubleColorSelectorWidget* SkinColorSelector;

    UPROPERTY(meta=(BindWidget))
    UEditableText* CharacterNameText;

    UPROPERTY(meta=(BindWidget))
    UDSButton* CreateCharacterButton;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Switchers")
    FString SwitcherOptionName{"SetSwitcherNameProperty"};

private:
    bool bIsCharacterCreated = false;

    UPROPERTY()
    ADSSavableModularCharacter* ModularCharacter = nullptr;

    UFUNCTION()
    void SwitchGender(int32 OptionIndex);

    UFUNCTION()
    void OnCreateCharacter();

    UFUNCTION()
    void OnCharacterNameChanged(const FText& Text);

    void UpdateCharacterEditor(EDSGender InGender) const;

    void UpdateSwitcher(UDSSwitcher* InSwitcher, const int32 OptionsAmount) const;

    void ShowFailMessage();
};
