// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "UI/Widgets/DSBorderSwitcherBase.h"
#include "DSColorSelectorWidget.generated.h"

class UHorizontalBox;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnColorChanged, const FLinearColor&, Color);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnColorsChanged, const FLinearColor&, FirstColor, const FLinearColor&, SecondColor);

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSColorSelectorWidget : public UDSBorderSwitcherBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnColorChanged OnColorChanged;

protected:
    UPROPERTY(meta=(BindWidget))
    UHorizontalBox* ColorBox;

    virtual void OnBorderFocused(UDSClickBorder* InFocusedBorder) override;

private:
    virtual void InitBorders(TArray<UDSClickBorder*>& OutBorders) override;
};

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSDoubleColorSelectorWidget : public UDSBorderSwitcherBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnColorsChanged OnColorsChanged;

protected:
    UPROPERTY(meta=(BindWidget))
    UHorizontalBox* ColorBox;

    virtual void OnBorderFocused(UDSClickBorder* InFocusedBorder) override;

private:
    virtual void InitBorders(TArray<UDSClickBorder*>& OutBorders) override;
};
