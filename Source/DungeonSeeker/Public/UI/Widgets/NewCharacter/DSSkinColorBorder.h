// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "PaletteComponents/DSClickBorder.h"
#include "DSSkinColorBorder.generated.h"

UCLASS()
class DUNGEONSEEKER_API UDSSkinColorBorder : public UDSClickBorder
{
    GENERATED_BODY()

public:
    const FLinearColor& GetStubbleColor() const { return StubbleColor; }

protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    FLinearColor StubbleColor;
};
