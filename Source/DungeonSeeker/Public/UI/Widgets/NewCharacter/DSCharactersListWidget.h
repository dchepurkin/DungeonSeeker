// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSUITypes.h"
#include "DSWidget.h"
#include "DSWindowWidget.h"
#include "DSCharactersListWidget.generated.h"

class UTextBlock;
class UVerticalBox;
class UDSCharacterSlotWidget;
class UDSDialogWindowWidget;
class UDSNewCharacterWidget;
class UDSButton;

struct FPlayerSaveData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterSelected, const FPlayerSaveData&, CharacterData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterListUpdated, int32, CharactersAmount);

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSCharactersListWidget : public UDSWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnCharacterSelected OnCharacterSelected;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnCharacterListUpdated OnCharacterListUpdated;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void Update();

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    FName GetCurrentCharacterName() const;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<UDSNewCharacterWidget> NewCharacterWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<UDSCharacterSlotWidget> CharacterSlotWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Dialogs")
    TSubclassOf<UDSDialogWindowWidget> DialogWindowWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Dialogs")
    FText RemoveCharacterQuestion{};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    int32 MaxCharacters = 5;

    UPROPERTY(meta=(BindWidget))
    UVerticalBox* CharactersVerticalBox;

    UPROPERTY(meta=(BindWidget))
    UDSButton* NewCharacterButton;

    UPROPERTY(meta=(BindWidget))
    UDSButton* RemoveCharacterButton;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* CurrentCharactersAmountText;

    UPROPERTY(meta=(BindWidget))
    UTextBlock* MaxCharactersAmountText;

    virtual void NativeConstruct() override;

private:
    //int32 CurrentCharactersAmount = 0;

    UPROPERTY()
    TArray<UDSCharacterSlotWidget*> CharactersSlots;

    UPROPERTY()
    UDSCharacterSlotWidget* CurrentSelectedSlot;

    UFUNCTION()
    void OnCharacterSlotSelected(UDSCharacterSlotWidget* CharacterSlot);

    UFUNCTION()
    void OnRemoveCharacterDialogClosed(UDSDialogWindowWidget* InWindow, EDialogCallback DialogCallback);

    UFUNCTION()
    void OnRemoveButtonClicked();

    UFUNCTION()
    void OnCharacterEditorClosed(UDSWindowWidget* InCharacterEditorWindow);

    UFUNCTION()
    void OnNewCharacterButtonClicked();

    UDSCharacterSlotWidget* CreateCharacterSlot(const FPlayerSaveData& InCharacterData);
};
