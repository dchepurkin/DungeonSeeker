// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Core/DSHUD.h"
#include "DSNewCharacterHUD.generated.h"

class UInputAction;
class UInputMappingContext;

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSNewCharacterHUD : public ADSHUD
{
    GENERATED_BODY()

public:


protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    TSubclassOf<UDSWindowWidget> CharacterEditorWindowClass;

    virtual void BeginPlay() override;
};
