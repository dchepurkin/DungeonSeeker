// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryTypes.h"
#include "DSTypes.h"
#include "Core/DSHUD.h"
#include "DSGameplayHUD.generated.h"

class UInputAction;
class UInputMappingContext;
class ADSPlayerCharacter;

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSGameplayHUD : public ADSHUD
{
    GENERATED_BODY()

public:
    ADSGameplayHUD();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    TMap<EDSUIType, TSubclassOf<UDSWindowWidget>> WidgetsClasses;

    virtual void BeginPlay() override;

private:
    UPROPERTY()
    ADSPlayerCharacter* PlayerCharacter = nullptr;

    EDSUIType UIState = EDSUIType::Gameplay;

    UFUNCTION()
    void SwitchUI(EDSUIType UIType);

    UFUNCTION()
    void OnInteractionFailed(const FText& FailMessage);

    UFUNCTION()
    void OnDropFailed(const FInventoryItem& InItem, bool bIsSuccess, const FText& FailMessage);
};
