// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "Components/SphereComponent.h"
#include "DSInteractionComponent.generated.h"

class ADSPlayerController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActorFocused, AActor*, FocusedActor, EDSInteractionType, InteractionType);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractionFailed, const FText&, FailMessage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractionStarted, AActor*, InterctedActor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClearFocus);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DUNGEONSEEKER_API UDSInteractionComponent : public USphereComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnActorFocused OnActorFocused;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnClearFocus OnClearFocus;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnInteractionFailed OnInteractionFailed;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnInteractionStarted OnInteractionStarted;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool Interact();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void StopInteraction();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetInteractionEnabled(const bool IsEnabled);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    AActor* GetFocusedActor() const { return FocusedActor; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    AActor* GetInteractedActor() const { return InteractedActor; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsFocused() const { return FocusedActor != nullptr; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsInteracted() { return InteractedActor != nullptr; }

    UDSInteractionComponent();

    virtual void BeginPlay() override;

    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    bool bEnableOutlineOnFocusedActor = true;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    float TraceDistance = 5000.f;

private:
    UPROPERTY()
    AActor* FocusedActor = nullptr;

    UPROPERTY()
    AActor* InteractedActor = nullptr;

    UPROPERTY()
    APawn* PawnOwner = nullptr;

    UPROPERTY()
    ADSPlayerController* Controller = nullptr;

    void SetActorFocused(AActor* InActor);

    void SetOutlineEnable(AActor* InActor, const bool IsEnable) const;

    bool CanFocused(AActor* InActor) const;

    void ClearFocus();

    void GetCameraInfo(FVector& OutCameraLocation, FVector& OutCameraForwardVector) const;

    AActor* GetFirstOverlapedActor();

    bool CanPlayerSee(AActor* InActor) const; //���������� ��� �������� ����

    UFUNCTION(/*��� ��������*/)
    void OnMoveFinished_Callback(const bool IsSuccess);
};
