// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "DSCameraComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnCameraBeginOverlapSignature)
DECLARE_MULTICAST_DELEGATE(FOnCameraEndOverlapSignature)

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DUNGEONSEEKER_API UDSCameraComponent : public UCameraComponent
{
	GENERATED_BODY()

public:
	FOnCameraBeginOverlapSignature OnCameraBeginOverlap;

	FOnCameraEndOverlapSignature OnCameraEndOverlap;

	UDSCameraComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker", meta=(ClampMin = 0.1f))
	float FadeDistance = 10.f;

private:
	bool bIsOverlaped = false;

	void CheckCameraCollision();
};
