// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DSRotationComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRotationComponentDelegate);

/**
 *RotationComponent is the component that rotate Actor (Only Yaw).
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONSEEKER_API UDSRotationComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDSRotationComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
	FRotationComponentDelegate OnRotationStarted;

	UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
	FRotationComponentDelegate OnRotationFinished;

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	void RotateToActor(AActor* RotateTo);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	void RotateToPoint(const FVector& RotetTo);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	void StopRotation();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
	bool IsRotated() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
	float RotationRate = 400.f;

private:
	FRotator TargetRotation;

	bool IsRotationEqualTarget() const;
};
