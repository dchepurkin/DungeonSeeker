// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DSCharacterMovementComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DUNGEONSEEKER_API UDSCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UDSCharacterMovementComponent();

	virtual void BeginPlay() override;
};
