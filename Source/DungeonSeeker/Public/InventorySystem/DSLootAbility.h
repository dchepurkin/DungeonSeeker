// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityBase.h"
#include "DSLootAbility.generated.h"

UCLASS(Abstract, Blueprintable)
class DUNGEONSEEKER_API UDSLootAbility : public UDSAbilityBase
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker", meta=(DisplayAfter="AbilityAnimation"))
    UAnimMontage* MiddlePickupAnimation = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker", meta=(DisplayAfter="AbilityAnimation"))
    UAnimMontage* UpPickupAnimation = nullptr;

    virtual UAnimMontage* GetAbilityAnimation() const override;
};
