// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryTypes.h"
#include "DSSavableComponent.h"
#include "Components/ActorComponent.h"
#include "DSInventoryComponent.generated.h"

class ADSLootActorBase;

DEFINE_LOG_CATEGORY_STATIC(LogInventoryComponentBase, All, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddItem, const FInventoryItem&, InItem);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveItem, EInventoryCategory, Category);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSlotsAmountChanged, EInventoryCategory, Category, int32, Amount);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDropItem, const FInventoryItem&, InItem, bool, IsSuccess, const FText&, FailMessage);

UCLASS(ClassGroup=(Custom), DisplayName="Inventory Component", meta=(BlueprintSpawnableComponent))
class DUNGEONSEEKER_API UDSInventoryComponent : public UActorComponent, public IDSSavableComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="Inventory")
    FOnRemoveItem OnRemoveItem;

    UPROPERTY(BlueprintAssignable, Category="Inventory")
    FOnAddItem OnAddItem;

    UPROPERTY(BlueprintAssignable, Category="Inventory")
    FOnDropItem OnDropItem;

    UPROPERTY(BlueprintAssignable, Category="Inventory")
    FOnSlotsAmountChanged OnSlotsAmountChanged;

    UDSInventoryComponent();

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    virtual FItemsContainer& GetContainer(const EInventoryCategory InCategory) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsEmpty(const EInventoryCategory InCategory) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool GetItemByIndex(const int32 InItemIndex, EInventoryCategory InCategory, FInventoryItem& OutItem);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsFull(const EInventoryCategory InCategory) const;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual bool AddItem(const FInventoryItem& InItem);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool DropItem(const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual bool RemoveItem(const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void AddSlots(const EInventoryCategory InCategory, const int32 InAmount) const;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker", meta=(ClampMin=10))
    int32 DefaultSlotsAmount = 20;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    TSubclassOf<ADSLootActorBase> LootActorClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    FText FailDropMessage = FText::FromString("Set Fail Drop Message Property In Inventory Component");

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop", meta=(ClampMin = 10.f, UIMin = 10.f))
    float DropDistance = 70.f;

    UPROPERTY(SaveGame)
    mutable TMap<EInventoryCategory, FItemsContainer> ItemsContainer;

    virtual void BeginPlay() override;

    virtual void InitContainer();

    virtual bool AddStackableItem(const FInventoryItem& InItem);

    virtual bool AddNotStackableItem(const FInventoryItem& InItem);

    bool DropTrace(const FVector& InLocation) const;
};
