// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "DSInventoryManager.generated.h"

enum class EEquipmentQuality : uint8;
enum class EInventoryCategory : uint8;
struct FInventoryItemInfo;
struct FInventoryItem;

class UDSPlayerInventoryComponent;

USTRUCT(BlueprintType)
struct FInventoryConfig
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Inventory")
    UDataTable* ItemsDataBase = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Inventory")
    TMap<EEquipmentQuality, FLinearColor> QualityColors;
};

UCLASS(Config="Inventory", DefaultConfig, DisplayName="Inventory Manager")
class DUNGEONSEEKER_API UDSInventoryManager : public UWorldSubsystem
{
    GENERATED_BODY()

public:
    virtual void Initialize(FSubsystemCollectionBase& Collection) override;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static UDSInventoryManager* GetInventoryManager(const UObject* InWorldContext);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static UDSPlayerInventoryComponent* GetPlayerInventory(const UObject* InWorldContext);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker|Inventory")
    static bool AddItemToInventory(AActor* InActor, const FInventoryItem& InItem);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker|Inventory")
    static bool RemoveItemFromInventory(AActor* InActor, const int32 InItemIndex, const int32 InAmount, EInventoryCategory InCategory);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static bool IsIdentified(const UObject* InWorldContext, const FName& InItemID);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory")
    static bool IsFullInventory(const AActor* InActor, const EInventoryCategory Category);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static FText GetItemName(const UObject* InWorldContext, const FName& InItemID);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static FText GetItemDescription(const UObject* InWorldContext, const FName& InItemID);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static int32 GetItemPrice(const UObject* InWorldContext, const FName& InItemID);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static FLinearColor GetQualityColor(const UObject* InWorldContext, const EEquipmentQuality InQuality);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker|Inventory", meta=(WorldContext=InWorldContext))
    static bool IsEquipment(const EInventoryCategory Category);

    FInventoryItemInfo* GetItemInfo(const FName& InItemID) const;

protected:
    UPROPERTY(Config, EditDefaultsOnly, BlueprintReadOnly, Category="Inventory", meta=(ShowOnlyInnerProperties))
    FInventoryConfig InventoryConfig;

private:
    UPROPERTY()
    mutable UDSPlayerInventoryComponent* PlayerInventory; //PlayerInventory Cache. Don't use this, Use GetPlayerInventory()

    mutable TMap<FName, FInventoryItemInfo*> ItemsInfoCache;
};
