// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityBase.h"
#include "DSInventoryItemAbility.generated.h"

UCLASS(Abstract, Blueprintable)
class DUNGEONSEEKER_API UDSInventoryItemAbility : public UDSAbilityBase
{
	GENERATED_BODY()
	
};
