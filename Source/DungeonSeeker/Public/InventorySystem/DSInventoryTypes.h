// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "DSInventoryTypes.generated.h"

class UDSInventoryItemAbility;
class UNiagaraSystem;

//��������� ���������, ������������ ��� UI (� ����� ������� ����� ������������)
UENUM(BlueprintType)
enum class EInventoryCategory : uint8
{
    Armor,
    Weapon,
    Jewellery,
    Food,
    Craft,
    Quest,
    Other
};

//��� �������� (����, �����, ���������� ���....)
UENUM(BlueprintType)
enum class EInventoryItemType : uint8
{
    Helmet,
    Tors,
    Shoulders,
    Legs,
    Hands,
    OneHandSword,
    TwoHandSword,
    Staff,
    Bow,
    Shield,
    Ring,
    Amulet,
    NewQuest, //�������� ����� �����
    ForQuest, //��� ���������� �����
    Food,
    CraftResource,
    CraftInstrument,
    Teleport,
    Container,
    IdentifyBook,
    Gold,
    Other
};

UENUM(BlueprintType)
enum class EEquipmentQuality : uint8
{
    Normal,
    Fine,
    Superior,
    Epic,
    Legendary,
    Unique
};

UENUM(BlueprintType)
enum EEquipmentSlot
{
    Costume,
    Helm,
    Hands,
    Shoulders,
    Tors,
    Legs
};

USTRUCT(BlueprintType)
struct FInventoryItemInfo : public FTableRowBase
{
    GENERATED_BODY()

    //Main
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main")
    EInventoryCategory Category = EInventoryCategory::Other;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main")
    EInventoryItemType Type = EInventoryItemType::Other;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main")
    FText Name{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main")
    FText Description{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main") //�������� ���� �������� (�������� ��� �������� �����, �������� - "�������� ����� �������")
    FText TypeDescription{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main", DisplayName="Stackable")
    bool bIsStackable = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main", DisplayName="Droppable")
    bool bIsDroppable = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main", DisplayName="Removable")
    bool bIsRemovable = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main")
    bool CanBeSold = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Main", meta=(ClampMin=1, EditCondition="CanBeSold"))
    int32 Price = 1;

    //Unidentified Properties
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Unidentified", DisplayName="Unidentified")
    bool bIsUnidentified = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Unidentified", meta=(EditCondition="bIsUnidentified"))
    FText UnidentifiedName{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Unidentified", meta=(EditCondition="bIsUnidentified"))
    FText UnidentifiedDescription{};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Unidentified", meta=(EditCondition="bIsUnidentified && CanBeSold", ClampMin=1))
    int32 UnidentifiedPrice = 1;

    //Equipment
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Equipment",
        meta=(EditCondition="Category == EInventoryCategory::Armor || Category == EInventoryCategory::Weapon || Category == EInventoryCategory::Jewellery"))
    EEquipmentQuality Quality = EEquipmentQuality::Normal;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Equipment",
        meta=(EditCondition="Category == EInventoryCategory::Armor || Category == EInventoryCategory::Weapon || Category == EInventoryCategory::Jewellery"))
    FName EquipmentSlotTag{};

    //Ability
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Ability", DisplayName="Usable")
    bool bIsUsable = false;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Ability", meta=(EditCondition="bIsUsable"))
    TSubclassOf<UDSInventoryItemAbility> Ability;

    //UI
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="UI")
    UTexture2D* Icon = nullptr;

    //Loot
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Loot")
    UStaticMesh* Mesh = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Loot")
    UNiagaraSystem* Particle = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Loot")
    USoundBase* TakeSound = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Loot")
    USoundBase* DropSound = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Loot")
    USoundBase* RemoveSound = nullptr;
};

USTRUCT(BlueprintType)
struct FInventoryItem
{
    GENERATED_BODY()

    FInventoryItem() {}

    explicit FInventoryItem(const FName& InItemID, const int32 InItemAmount = 1)
        : ID(InItemID),
          Amount(InItemAmount >= 1 ? InItemAmount : 1) {}

    UPROPERTY(SaveGame, EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    FName ID{};

    UPROPERTY(SaveGame, EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    int32 Amount = 1;

    UPROPERTY(SaveGame, BlueprintReadOnly, Category="DungeonSeeker")
    bool bNotViewed = true;

    FInventoryItemInfo* GetInfo(UObject* InWorldContext) const
    {
        if (!Info)
        {
            Info = UDSInventoryFunctionLibrary::GetItemInfo(InWorldContext, ID);
        }
        return Info;
    }

    bool IsStackable(UObject* InWorldContext) const;

    bool IsDroppable(UObject* InWorldContext) const;

    bool IsRemovable(UObject* InWorldContext) const;

    bool IsUsable(UObject* InWorldContext) const;

    bool CanBeSold(UObject* InWorldContext) const;

private:
    mutable FInventoryItemInfo* Info = nullptr;
};

USTRUCT(BlueprintType)
struct FItemsContainer
{
    GENERATED_BODY()

    FItemsContainer() {}

    explicit FItemsContainer(const int32 InSlotsAmount)
        : SlotsAmount(InSlotsAmount)
    {
        //Slots.SetNum(InSlotsAmount); TODO Maybe i must use this method
        //Slots.BulkSerialize() TODO try this to save empty array
    }

    FORCEINLINE bool IsEmpty() const { return Slots.IsEmpty(); };

    FORCEINLINE bool IsFull() const { return Slots.Num() == SlotsAmount; }

    bool AddSlots(const int32 InAmount)
    {
        if (InAmount < 1) { return false; }

        SlotsAmount += InAmount;
        return true;
    }

    UPROPERTY(SaveGame, EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker")
    TArray<FInventoryItem> Slots;

    UPROPERTY(SaveGame, BlueprintReadOnly, Category="DungeonSeeker")
    int32 SlotsAmount = 0;
};
