// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryComponent.h"
#include "DSPlayerInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGoldChanged, int32, InGold);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnIdentifyItem, const FName&, InItemID);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemViewed, int32, InItemIndex, EInventoryCategory, InCategory);

UCLASS(ClassGroup=(Custom), DisplayName="Player Inventory Component", meta=(BlueprintSpawnableComponent))
class DUNGEONSEEKER_API UDSPlayerInventoryComponent : public UDSInventoryComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnGoldChanged OnGoldChanged;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnIdentifyItem OnIdentifyItem;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnItemViewed OnItemViewed;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsIdentified(const FName& InItemID) const;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void IdentifyItem(const FName& InItemID);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetItemViewed(const int32 InItemIndex, EInventoryCategory InCategory);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool HaveNotViewedItems(EInventoryCategory InCategory) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    FORCEINLINE int32 GetGold() const { return Gold; }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool RemoveGold(const int32 InAmount);

    virtual FItemsContainer& GetContainer(const EInventoryCategory InCategory) const override;

    virtual bool AddItem(const FInventoryItem& InItem) override;

protected:
    virtual void InitContainer() override;

    virtual void BeginPlay() override;

private:
    UPROPERTY(SaveGame)
    TArray<FName> IdentifiedItems;

    UPROPERTY(SaveGame)
    int32 Gold = INDEX_NONE;

    bool AddGold(const int32 InAmount);
};
