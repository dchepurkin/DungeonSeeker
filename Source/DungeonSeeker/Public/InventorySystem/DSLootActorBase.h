// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSInventoryTypes.h"
#include "DSInteractionInterface.h"
#include "GameFramework/Actor.h"
#include "DSLootActorBase.generated.h"

class USphereComponent;
class UNiagaraComponent;
class UStaticMeshComponent;
class UBillboardComponent;
class UDSAbilityBase;
class UDSLootAbility;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemTaken, AActor*, WhoTake);

UCLASS(Abstract, Blueprintable, BlueprintType)
class DUNGEONSEEKER_API ADSLootActorBase : public AActor, public IDSInteractionInterface
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnItemTaken OnItemTaken;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void TakeItem(AActor* WhoTake);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void Init(const FInventoryItem& InItem);

    ADSLootActorBase();

    virtual void Tick(float DeltaSeconds) override;

protected:
    UPROPERTY(SaveGame, EditInstanceOnly, BlueprintReadOnly, Category="DungeonSeeker")
    FInventoryItem Item;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    float ZOffset = 10.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    float DropZTraceDistance = 100.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    int32 OutlineValue = 3;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Drop")
    FName MaterialColorParamName{"Color"};

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Interaction")
    float MinInteractionDistance = 150.f;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Interaction")
    float MinFocusDistance = 250.f;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Interaction")
    FText InteractionName = FText::FromString("Set Interaction Name Property");

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Interaction")
    FText FullInventoryMessage = FText::FromString("Set FullInventoryMessage Property");

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|Interaction")
    TSubclassOf<UDSLootAbility> TakeItemAbility;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Rotation")
    bool bIsEnabledRotation = true;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Rotation", meta=(EditCondition="bIsEnabledRotation"))
    FRotator DeltaRotation{0.f, 50.f, 0.f};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
    USphereComponent* Collision;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
    UStaticMeshComponent* Mesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
    UNiagaraComponent* ParticleComponent;

#if WITH_EDITORONLY_DATA
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
    UBillboardComponent* BillboardComponent;
#endif

    virtual void BeginPlay() override;

    void SnapToFloor();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker", meta=(BlueprintProtected))
    void Update();

private:
    virtual void Interact_Implementation(AActor* Interactor) override;

    virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) override;

    virtual bool CanFocus_Implementation() override { return true; }

    virtual float GetMinFocusDistance_Implementation() override { return MinFocusDistance; }

    virtual float GetMinInteractionDistance_Implementation() override { return MinInteractionDistance; }

    virtual FVector GetInteractionLocation_Implementation() override { return GetActorLocation(); }

    virtual FText GetInteractionName_Implementation() override { return InteractionName; }

    virtual EDSInteractionType GetInteractionType_Implementation() override { return EDSInteractionType::Item; }

    virtual FText GetInteractionActorName_Implementation() override;

    UFUNCTION()
    void AbilityTriggered(UDSAbilityBase* Ability, AActor* AbilityUser, AActor* AbilityTarget);
};
