// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DSInventoryFunctionLibrary.generated.h"

struct FInventoryItem;
struct FInventoryItemInfo;

UCLASS()
class DUNGEONSEEKER_API UDSInventoryFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    static FInventoryItemInfo* GetItemInfo(const UObject* InWorldContext, const FName& InItemID);
};
