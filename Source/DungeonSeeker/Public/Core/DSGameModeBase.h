//Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DSGameModeBase.generated.h"

UCLASS(minimalapi)
class ADSGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SaveGame(bool SavePlayerOnly);

    ADSGameModeBase();

    virtual void Tick(float DeltaSeconds) override;

protected:
    UPROPERTY(EditDefaultsOnly, Category="DungeonSeeker")
    float SaveGameDelay = 0.05f;

    FTimerManager TimerManager;

    FTimerHandle SaveGameTimerHandle;
};
