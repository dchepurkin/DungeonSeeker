// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Core/DSGameModeBase.h"
#include "DSHomeGameMode.generated.h"

UCLASS()
class DUNGEONSEEKER_API ADSHomeGameMode : public ADSGameModeBase
{
	GENERATED_BODY()

public:
	ADSHomeGameMode();
};
