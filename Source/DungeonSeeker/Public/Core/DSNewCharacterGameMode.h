// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Core/DSGameModeBase.h"
#include "DSNewCharacterGameMode.generated.h"

UCLASS()
class DUNGEONSEEKER_API ADSNewCharacterGameMode : public ADSGameModeBase
{
    GENERATED_BODY()

public:

protected:
    virtual void BeginPlay() override;
};
