// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "Engine/GameInstance.h"
#include "DSGameInstance.generated.h"

class UDSCharacterCustomization;
class ADSHUD;

UCLASS(Abstract)
class DUNGEONSEEKER_API UDSGameInstance : public UGameInstance
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void StartGame(const FName& InCharacterName);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    UDSCharacterCustomization* GetCharacterCustomization() const { return CharacterCustomization; }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetCharacterName(const FName& InName);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void OpenLevel(const FName& InLevelName);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FName& GetCharacterName() const { return CharacterName; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    UTexture2D* GetLoadingScreenImage(const FName& MapName) const;

    virtual void Init() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    UDSCharacterCustomization* CharacterCustomization;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Maps")
    UDataTable* LoadingScreensInfoDataTable;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Maps")
    FName HomeLevelName{"HomeMap"};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Maps")
    FName LobbyLevelName{"NewCharacterMap"};

private:
    FName CharacterName{};

    void PreLoadMap_Callback(const FString& String);

    void PostLoadMap_Callback(UWorld* World);

    ADSHUD* GetHUD() const;
};
