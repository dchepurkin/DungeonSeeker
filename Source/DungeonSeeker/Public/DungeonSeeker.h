//Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"

class FDungeonSeeker : public IModuleInterface
{
public:
    virtual void StartupModule() override;

    virtual void ShutdownModule() override;
};