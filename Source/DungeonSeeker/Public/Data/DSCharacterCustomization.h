// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "Engine/DataAsset.h"
#include "DSCharacterCustomization.generated.h"

UCLASS(DisplayName="CharacterCustomization")
class DUNGEONSEEKER_API UDSCharacterCustomization : public UDataAsset
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    UMaterialInterface* GetDefaultMaterial() const { return DefaultMaterial; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const TArray<FDSBodyPart>& GetDefaultBodyParts(EDSGender InGender) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    USkeletalMesh* GetHead(EDSGender InGender, int32 InIndex) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    USkeletalMesh* GetHair(EDSGender InGender, int32 InIndex) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    USkeletalMesh* GetEyebrow(EDSGender InGender, int32 InIndex) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    USkeletalMesh* GetBeard(EDSGender InGender, int32 InIndex) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    int32 GetHeadsAmount(EDSGender InGender) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    int32 GetEyebrowsAmount(EDSGender InGender) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    int32 GetHairsAmount(EDSGender InGender) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    int32 GetBeardsAmount() const { return Beards.Num(); }

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    TMap<EDSGender, FDSBody> DefaultBody;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    TArray<USkeletalMesh*> Beards;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Heads")
    TArray<USkeletalMesh*> MaleHeads;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Heads")
    TArray<USkeletalMesh*> FemaleHeads;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Eyebrows")
    TArray<USkeletalMesh*> MaleEyebrows;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Eyebrows")
    TArray<USkeletalMesh*> FemaleEyebrows;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Hairs")
    TArray<USkeletalMesh*> MaleHairs;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Hairs")
    TArray<USkeletalMesh*> FemaleHairs;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    UMaterialInterface* DefaultMaterial;
};
