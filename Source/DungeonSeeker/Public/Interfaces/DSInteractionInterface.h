// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DSTypes.h"
#include "DSConstants.h"
#include "DSInteractionInterface.generated.h"

UINTERFACE(MinimalAPI, DisplayName="Interaction Interface")
class UDSInteractionInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONSEEKER_API IDSInteractionInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	bool CanInteract(AActor* Interactor, FText& FailMessage);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	bool CanFocus();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	EDSInteractionType GetInteractionType();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	float GetMinInteractionDistance();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	float GetMinFocusDistance();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	FVector GetInteractionLocation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	FText GetInteractionActorName();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="DungeonSeeker|Interaction")
	FText GetInteractionName();

	UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|Interaction")
	void Interact(AActor* Interactor);

private:
	virtual bool CanInteract_Implementation(AActor* Interactor, FText& FailMessage) { return true; };

	virtual bool CanFocus_Implementation() { return true; }

	virtual EDSInteractionType GetInteractionType_Implementation() { return EDSInteractionType::None; }

	virtual float GetMinInteractionDistance_Implementation() { return DS::DefaultMinInteractionDistance; }

	virtual float GetMinFocusDistance_Implementation() { return DS::DefaultMinInteractionDistance; }

	virtual FVector GetInteractionLocation_Implementation() { return FVector::ZeroVector; }

	virtual FText GetInteractionActorName_Implementation();

	virtual FText GetInteractionName_Implementation();

	virtual void Interact_Implementation(AActor* Interactor)	{};
};
