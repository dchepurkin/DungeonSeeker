// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "DSAnimInstanceBase.generated.h"

UCLASS()
class DUNGEONSEEKER_API UDSAnimInstanceBase : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;

	virtual void NativeThreadSafeUpdateAnimation(float DeltaSeconds) override;

protected:
	UPROPERTY()
	APawn* PawnOwner = nullptr;

	UPROPERTY(BlueprintReadOnly, Category="DungeonSeeker")
	float Speed = 0.f;

	UPROPERTY(BlueprintReadOnly, Category="DungeonSeeker")
	float LeanAxis = 0.f;

	UPROPERTY(BlueprintReadOnly, Category="DungeonSeeker")
	float Direction = 0.f;

	UPROPERTY(BlueprintReadOnly, Category="DungeonSeeker")
	bool bIsFalling = false;

	bool GetIsFalling() const;
};
