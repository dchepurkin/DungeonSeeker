// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "DSFootstepNotify.generated.h"

UCLASS(Abstract, DisplayName="Footstep")
class DUNGEONSEEKER_API UDSFootstepNotify : public UAnimNotify
{
    GENERATED_BODY()

public:
    virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

    virtual FString GetNotifyName_Implementation() const override;

protected:
    UPROPERTY(EditInstanceOnly, Category="DungeonSeeker")
    FName BoneName{};

    UPROPERTY(EditAnywhere, Category="DungeonSeeker")
    float TraceLength = 100.f;

    UPROPERTY(EditDefaultsOnly, Category="DungeonSeeker")
    TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> SoundsMap;

private:
    bool BoneTrace(USkeletalMeshComponent* MeshComp, FHitResult& OutResult) const;
};
