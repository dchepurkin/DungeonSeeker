// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableModularCharacter.h"
#include "DSCustomizeCharacterActor.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSCustomizeCharacterActor : public ADSSavableModularCharacter
{
    GENERATED_BODY()

public:
    ADSCustomizeCharacterActor(const FObjectInitializer& Initializer);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    USpringArmComponent* SpringArm;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    UCameraComponent* Camera;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Input")
    UInputMappingContext* InputMapping;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Input")
    UInputAction* RotateAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|Input")
    UInputAction* SetUIInputModeAction;

    virtual void BeginPlay() override;

    void Rotate(const FInputActionValue& Value);

    void SwitchInputMode(const FInputActionValue& Value);

    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

private:
    FVector2D CursorPosition;
};
