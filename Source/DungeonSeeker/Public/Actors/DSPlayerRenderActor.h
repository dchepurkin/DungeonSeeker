// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSModularCharacter.h"
#include "DSPlayerRenderActor.generated.h"

class USpotLightComponent;
class USpringArmComponent;

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSPlayerRenderActor : public ADSModularCharacter
{
    GENERATED_BODY()

public:
    ADSPlayerRenderActor(const FObjectInitializer& Initializer);

protected:
    UPROPERTY()
    ADSModularCharacter* PlayerCharacter;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USpringArmComponent* SpringArm;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USceneCaptureComponent2D* CaptureComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USpotLightComponent* SpotLight;

    void UpdateBodyParts(const TArray<USkeletalMeshComponent*>& PlayerBodyParts);

    UFUNCTION()
    void OnChangeBodyPart_Callback(const FDSBodyPart& InBodyPart, const FDSBodyPartParams& Params);

    virtual void BeginPlay() override;
};
