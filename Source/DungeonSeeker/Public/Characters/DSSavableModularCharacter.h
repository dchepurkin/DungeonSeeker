// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavablePlayer.h"
#include "Characters/DSModularCharacter.h"
#include "DSSavableModularCharacter.generated.h"

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSSavableModularCharacter : public ADSModularCharacter, public IDSSavablePlayer
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetName(const FName& InName) { CharacterName = InName; }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetGender(EDSGender InGender);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetHead(int32 HeadIndex);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetEyebrow(int32 EyebrowIndex);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetHair(int32 HairIndex);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetHairColor(const FLinearColor& InColor);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetScarColor(const FLinearColor& InColor);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetBodyArtColor(const FLinearColor& InColor);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetSkinColor(const FLinearColor& InSkinColor, const FLinearColor& InStubbleColor);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetBeard(int32 BeardIndex);

protected:
    UPROPERTY(SaveGame)
    EDSGender Gender = EDSGender::None;

    UPROPERTY(SaveGame)
    FLinearColor DefaultHairColor;

    UPROPERTY(SaveGame)
    FLinearColor DefaultSkinColor;

    UPROPERTY(SaveGame)
    FLinearColor DefaultStubbleColor; //������

    UPROPERTY(SaveGame)
    FLinearColor DefaultScarColor; //�����

    UPROPERTY(SaveGame)
    FLinearColor DefaultBodyArtColor;

    UPROPERTY(SaveGame)
    int32 DefaultHeadIndex = INDEX_NONE;

    UPROPERTY(SaveGame)
    int32 DefaultBeardIndex = INDEX_NONE;

    UPROPERTY(SaveGame)
    int32 DefaultEyebrowIndex = INDEX_NONE;

    UPROPERTY(SaveGame)
    int32 DefaultHairIndex = INDEX_NONE;

    UPROPERTY(SaveGame)
    FName CharacterName{};

private:
    virtual FName GetCharacterName_Implementation() override { return CharacterName; };

    virtual void OnPlayerLoaded_Implementation() override;
};
