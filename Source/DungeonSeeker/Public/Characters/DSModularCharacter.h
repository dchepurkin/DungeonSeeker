// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSTypes.h"
#include "InputMappingContext.h"
#include "Characters/DSCharacterBase.h"
#include "DSModularCharacter.generated.h"

class UDSCharacterCustomization;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeBodyPart, const FDSBodyPart&, BodyPart, const FDSBodyPartParams&, Params);

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSModularCharacter : public ADSCharacterBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnChangeBodyPart OnChangeBodyPart;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const TArray<USkeletalMeshComponent*>& GetBodyParts() const { return BodyPartsMeshes; }

    /*UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool GetInCostume() const { return bInCostume; }*/

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetBodyPart(const FDSBodyPart& InBodyPart, const FDSBodyPartParams& Params);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetInputMapping(UInputMappingContext* InMappingContext) const;

    ADSModularCharacter(const FObjectInitializer& Initializer);

protected:
    UPROPERTY()
    TArray<USkeletalMeshComponent*> BodyPartsMeshes;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* HelmMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* HeadMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* HairMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* BeardMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* EyebrowMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* HandsMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* TorsMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* LegsMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker")
    USkeletalMeshComponent* ShouldersMesh;

    //bool bInCostume = false;

    FDSBodyPartParams HelmParams;

    FDSBodyPartParams CostumeParams;

    //Tag ������������ � ���������
    USkeletalMeshComponent* CreateBodyPartMesh(const FName& InBodyPartName, const FName& InTag);

    virtual void BeginPlay() override;

    void SetCostume(USkeletalMesh* CostumeMesh, const FDSBodyPartParams& Params);

    void SetHelm(USkeletalMesh* InMesh, const FDSBodyPartParams& Params);

    void UpdateHeadHidden();

    bool IsCostumeComponent(USkeletalMeshComponent* InMeshComp);

    bool IsHeadComponent(USkeletalMeshComponent* InMeshComp);

    UDSCharacterCustomization* GetCharacterCustomization() const;

private:
    //Use GetCharacterCustomization member function
    UPROPERTY()
    mutable UDSCharacterCustomization* CharacterCustomization_Cache;
};
