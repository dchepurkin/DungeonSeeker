//Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DSCharacterBase.generated.h"

class UDSAbilityComponent;
class UDSRotationComponent;

UCLASS(Abstract)
class ADSCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	ADSCharacterBase(const FObjectInitializer& Initializer);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Components")
	UDSAbilityComponent* AbilityComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Components")
	UDSRotationComponent* RotationComponent;

	virtual void BeginPlay() override;
};
