// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSSavableModularCharacter.h"
#include "DSPlayerCharacter.generated.h"

class USpringArmComponent;
class UDSCameraComponent;
class UInputMappingContext;
class UDSInteractionComponent;
class UNavigationInvokerComponent;
class UDSPlayerInventoryComponent;
class UInputAction;
class UDSAbilityBase;

struct FInputActionValue;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSwitchToUI, EDSUIType, UIType);

UCLASS(Abstract)
class DUNGEONSEEKER_API ADSPlayerCharacter : public ADSSavableModularCharacter
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnSwitchToUI OnSwitchToUI;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetCharacterName(const FName& InName);

    ADSPlayerCharacter(const FObjectInitializer& Initializer);

    virtual void Jump() override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Components")
    UNavigationInvokerComponent* NavigationInvokerComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Components")
    UDSInteractionComponent* InteractionComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Components")
    UDSPlayerInventoryComponent* InventoryComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Camera")
    USpringArmComponent* CameraBoom;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Camera")
    UDSCameraComponent* FollowCamera;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Mappings")
    UInputMappingContext* DefaultMappingContext;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* JumpAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* MoveAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* LookAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* InteractAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* InventoryAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DungeonSeeker|Input|Actions")
    UInputAction* CancelAction;

    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

    virtual void BeginPlay() override;

private:
    void StartMoving();

    void Move(const FInputActionValue& Value);

    void Look(const FInputActionValue& Value);

    void InteractionInput();

    void OnCameraBeginOverlap_Callback();

    void OnCameraEndOverlap_Callback();

    bool StopAllActions() const;

    void CancelInput();

    void InventoryInput();

    bool CanInteract() const;

    UFUNCTION()
    void OnAbilityStarted(UDSAbilityBase* InAbility, AActor* InAbilityTarget);

    virtual FName GetCharacterName_Implementation() override { return CharacterName; }
};
