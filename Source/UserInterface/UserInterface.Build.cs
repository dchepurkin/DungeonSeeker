//Created by Dmirtiy Chepurkin. All right reserved

using UnrealBuildTool;

public class UserInterface : ModuleRules
{
	public UserInterface(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG", "SlateCore", "Slate" });

		PublicIncludePaths.AddRange(new string[]
		{
			"UserInterface/",
			"UserInterface/Public/",
			"UserInterface/Public/PaletteComponents/",
			"UserInterface/Public/Core/",
			"UserInterface/Public/Widgets/",
		});
	}
}