// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSHUD.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DSUIFunctionLibrary.generated.h"

UCLASS()
class USERINTERFACE_API UDSUIFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker", meta=(WorldContext=InWorldContext))
    static void ShowMessage(UObject* InWorldContext, const FText InMessageText, const FLinearColor InMessageColor = FLinearColor::White);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker", meta=(WorldContext=InWorldContext))
    static ADSHUD* GetDSHUD(UObject* InWorldContext);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker", meta=(WorldContext=InWorldContext))
    static UDSWindowWidget* GetRootWindow(UObject* InWorldContext);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker", meta=(WorldContext=InWorldContext))
    static UDSWindowWidget* CreateWindow(UObject* InWorldContext, UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass);

    template <typename WindowClass>
    static WindowClass* CreateWindow(UObject* InWorldContext, UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass)
    {
        const auto HUD = GetDSHUD(InWorldContext);
        return HUD ? HUD->CreateWindow<WindowClass>(InParent, InWindowClass) : nullptr;
    }
};
