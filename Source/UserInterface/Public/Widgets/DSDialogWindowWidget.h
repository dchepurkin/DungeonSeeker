// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSWindowWidget.h"
#include "DSUITypes.h"
#include "DSDialogWindowWidget.generated.h"

class UTextBlock;
class UDSDialogWindowWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDialogWindowCallback, UDSDialogWindowWidget*, InWindow, EDialogCallback, DialogCallback);

UCLASS(Abstract)
class USERINTERFACE_API UDSDialogWindowWidget : public UDSWindowWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FDialogWindowCallback OnDialogClosed;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetDialogParameters(const FDialogWindowParameters& InParameters);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetDialogMessage(const FText& InMessage) const;

protected:
    UPROPERTY(meta=(BindWidgetOptional))
    UButton* YesButton;

    UPROPERTY(meta=(BindWidgetOptional))
    UButton* NoButton;

    UPROPERTY(meta=(BindWidgetOptional))
    UButton* CancelButton;

    UPROPERTY(meta=(BindWidgetOptional))
    UTextBlock* DialogMessageTextBlock;

    virtual void NativeOnInitialized() override;

private:
    FDialogWindowParameters DialogWindowParameters;

    UFUNCTION()
    void YesButton_Callback();

    UFUNCTION()
    void NoButton_Callback();

    UFUNCTION()
    void CancelButton_Callback();
};
