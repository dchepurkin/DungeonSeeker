// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Widgets/DSSwitcherBase.h"
#include "DSSwitcher.generated.h"

class UButton;

UCLASS(Abstract)
class USERINTERFACE_API UDSSwitcher : public UDSSwitcherBase
{
    GENERATED_BODY()

public:
    virtual void SetCurrentOption(const FString& InOption) override;

    virtual void SetCurrentOption(const int32 InOptionIndex) override;

protected:
    UPROPERTY(meta=(BindWidget))
    UButton* LeftSwitchButton;

    UPROPERTY(meta=(BindWidget))
    UButton* RightSwitchButton;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ultimate")
    bool bIsCircleSwitch = false;

    virtual void NativeOnInitialized() override;

private:
    UFUNCTION()
    void OnLeftClicked_Callback();

    UFUNCTION()
    void OnRightClicked_Callback();

    void SetEnabled() const;
};
