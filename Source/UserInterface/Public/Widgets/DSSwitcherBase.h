// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSSwitcherBase.generated.h"

class UTextBlock;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOptionChanged, int32, OptionIndex);

UCLASS(Abstract)
class USERINTERFACE_API UDSSwitcherBase : public UUserWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnOptionChanged OnOptionChanged;

    void ClearOptions();

    void AddOption(const FString& InOption);

    virtual void SetCurrentOption(const FString& InOption);

    virtual void SetCurrentOption(const int32 InOptionIndex);

    void SelectOption(const FString& InOption);

    void SelectOption(const int32 InOptionIndex);

    const FString& GetSelectedOption() const { return SelectedOption; }

    int32 GetSelectedOptionIndex() const { return DefaultOptions.IndexOfByKey(SelectedOption); }

    UFUNCTION()
    const TArray<FString>& GetOptions() const;

protected:
    UPROPERTY(meta=(BindWidget))
    UTextBlock* SelectedOptionTextBlock;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    TArray<FString> DefaultOptions;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker", meta=(GetOptions="GetOptions"))
    FString SelectedOption;

    virtual void SynchronizeProperties() override;
};
