// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSWidget.h"
#include "DSUITypes.h"
#include "DSWindowWidget.generated.h"

class UButton;
class UDSWindowWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWindowSignature, UDSWindowWidget*, InWindow);

UCLASS(Abstract)
class USERINTERFACE_API UDSWindowWidget : public UDSWidget
{
    GENERATED_BODY()

    friend class ADSHUD;

public:
    UPROPERTY(BlueprintCallable, BlueprintAssignable, Category="DungeonSeeker")
    FWindowSignature OnShowWindow;

    UPROPERTY(BlueprintCallable, BlueprintAssignable, Category="DungeonSeeker")
    FWindowSignature OnCloseWindow;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual void Show(const FWindowParameters& InWindowParameters);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual void Close();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual void Block(const EBlockWindows InParam);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual void Unblock();

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetPosition(const FVector2D& InPosition, const FVector2D& AlignmentRatio);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsRootWindow() const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsFree() const { return GetVisibility() == InitVisibility; }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetState(const EBlockWindows InParam);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetMouseInScreenCenter();

    /*UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool ExecuteKeyEvent(const FKey& InKey);*/

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    ESlateVisibility GetInitVisibility() const { return InitVisibility; }

    virtual void RemoveFromParent() override;

protected:
    FWindowParameters WindowParameters;

    UPROPERTY(meta=(BindWidgetOptional))
    UButton* CloseButton;

    UPROPERTY(Transient, meta=(BindWidgetAnimOptional))
    UWidgetAnimation* ShowAnimation;

    UFUNCTION()
    virtual void OnShowAnimationFinished_Callback() {}

    UFUNCTION()
    virtual void OnShowAnimationStarted_Callback() {}

    virtual void NativeOnInitialized() override;

    void SetParent(UDSWindowWidget* InWindow);

    void AddChild(UDSWindowWidget* InWindow);

private:
    ESlateVisibility InitVisibility;

    UPROPERTY()
    UDSWindowWidget* ParentWindow = nullptr;

    UPROPERTY()
    TArray<UDSWindowWidget*> ChildWindows;

    FWidgetAnimationDynamicEvent OnShowAnimationFinished;

    FWidgetAnimationDynamicEvent OnShowAnimationStarted;
};
