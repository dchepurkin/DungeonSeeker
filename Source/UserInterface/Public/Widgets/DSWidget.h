// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DSWidget.generated.h"

class ADSHUD;

UCLASS(Abstract)
class USERINTERFACE_API UDSWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    ADSHUD* GetDSHUD() const;

    template <typename UltimateHUDSubclass>
    UltimateHUDSubclass* GetUltimateHUD() const
    {
        const auto Controller = GetOwningPlayer();
        return Controller ? Controller->GetHUD<UltimateHUDSubclass>() : nullptr;
    }

    template <typename ParentType = UDSWidget>
    ParentType* GetParentWidget()
    {
        return GetParentWidget<ParentType>(this);
    }

    template <typename ParentType = UDSWidget>
    static ParentType* GetParentWidget(UWidget* InWidget)
    {
        for (auto Parent = InWidget->GetOuter(); Parent; Parent = Parent->GetOuter())
        {
            if (const auto Result = Cast<ParentType>(Parent))
            {
                return Result;
            }
        }
        return nullptr;
    }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual void UpdateWidget() {}
};
