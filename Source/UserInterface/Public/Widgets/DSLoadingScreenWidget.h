// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSWindowWidget.h"
#include "DSLoadingScreenWidget.generated.h"

class UImage;

UCLASS(Abstract)
class USERINTERFACE_API UDSLoadingScreenWidget : public UDSWindowWidget
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetBackground(UTexture2D* InTexture);

protected:
    UPROPERTY(meta=(BindWidget))
    UImage* BackgroundImage;
};
