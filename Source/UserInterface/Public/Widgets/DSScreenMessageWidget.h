// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "DSWidget.h"
#include "DSScreenMessageWidget.generated.h"

class UTextBlock;

UCLASS(Abstract)
class USERINTERFACE_API UDSScreenMessageWidget : public UDSWidget, public FTickableGameObject
{
    GENERATED_BODY()

public:
    virtual void Tick(float DeltaTime) override;

    virtual bool IsTickableWhenPaused() const override { return true; }

    virtual TStatId GetStatId() const override { return TStatId(); }

    void Init(const FText& InText, const FLinearColor& InTextColor, const float InLifeTime);

protected:
    UPROPERTY(meta=(BindWidget))
    UTextBlock* MessageTextBlock;

    UPROPERTY(Transient, meta=(BindWidgetAnim))
    UWidgetAnimation* FadeOutAnimation;

    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

private:
    FTimerManager TimerManager;

    FTimerHandle LifeTimerHandle;

    void LifeTimer_Callback();
};
