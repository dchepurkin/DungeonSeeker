// Created by Dmitriy Chepurkin. All rights reserved

#pragma once

#include "DSUITypes.generated.h"

UENUM(BlueprintType)
enum class EDialogCallback : uint8
{
    Yes,
    No,
    Cancel
};

UENUM(BlueprintType)
enum class EBlockWindows : uint8
{
    None,
    Block,
    Hide
};

UENUM(BlueprintType)
enum class EDSInputMode : uint8
{
    Game,
    UI,
    UIWithPause UMETA(DisplayName="UI With Pause"),
    LoadingScreen
};

USTRUCT(BlueprintType)
struct FWindowParameters
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    EBlockWindows BlockOtherWindows = EBlockWindows::None;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bClosedWithParentWindow = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bNotHiddable = false;

    /*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bCloseAllWhenShow = false;*/

    /*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bCanOpenWhenPlayerIsDead = false;*/

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bSetCursorOnScreenCenter = false;
};

USTRUCT(BlueprintType)
struct FDialogWindowParameters
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bClosedWithParentWindowOnYes = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bClosedWithParentWindowOnNo = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bClosedWithParentWindowOnCancel = false;
};


/*USTRUCT()
struct FGameSettings
{
    GENERATED_BODY()

    int32 VisualEffectQuality = 0;

    int32 ShadowQuality = 0;

    int32 TextureQuality = 0;

    EWindowMode::Type FullscreenMode = EWindowMode::Fullscreen;

    FIntPoint Resolution{};
};*/
