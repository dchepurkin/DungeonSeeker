// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "DSUITypes.h"
#include "DSHUD.generated.h"

class UDSDialogWindowWidget;
class UDSWidget;
class UDSWindowWidget;
class UDSScreenMessageWidget;
class UDSLoadingScreenWidget;

DECLARE_MULTICAST_DELEGATE(FOnLoadingScreenFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLoadingScreenFinishedBP);

UCLASS(Abstract)
class USERINTERFACE_API ADSHUD : public AHUD
{
    GENERATED_BODY()

public:
    FOnLoadingScreenFinished OnLoadingScreenFinished;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker", DisplayName="OnLoadingScreenFinished")
    FOnLoadingScreenFinishedBP OnLoadingScreenFinishedBP;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    UDSWindowWidget* CreateRootWindow(const TSubclassOf<UDSWindowWidget> InRootWindowClass, const bool bNotHiddable);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    UDSWindowWidget* CreateWindow(UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    UDSDialogWindowWidget* OpenDialog(UDSWidget* InParent, const TSubclassOf<UDSDialogWindowWidget> InDialogClass, const FText& InDialogText);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetInputMode(const EDSInputMode InMode);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void ShowLoadingScreen(UTexture2D* InLoadingScreenImage);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const TArray<UDSWindowWidget*>& GetAllUltimateWindows() { return WindowsArray; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    UDSWindowWidget* GetRootWindow() const { return RootWindow; }

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void CLoseAll() const;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void ResumeGame();

    /*UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool ExecuteKeyEvents(const FKey& InKey);*/

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void ShowMessage(const FText& InMessageText, const FLinearColor& InMessageColor = FLinearColor::White);

    ADSHUD();

    virtual void Tick(float DeltaSeconds) override;

    template <typename WindowClass>
    WindowClass* CreateWindow(UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass)
    {
        return Cast<WindowClass>(CreateWindow(InParent, InWindowClass));
    }

protected:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|ScreenMessage")
    TSubclassOf<UDSScreenMessageWidget> ScreenMessageWidgetClass;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|ScreenMessage", meta=(ClampMin = 0.5f, UIMin = 0.5f))
    float ScreenMessageLifeTime = 2.f;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="DungeonSeeker|LoadingScreen")
    TSubclassOf<UDSLoadingScreenWidget> LoadingScreenWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|LoadingScreen", meta=(ClampMin = 0.1f, UIMin = 0.1f))
    float LoadingScreenTime = 3.f;

    UFUNCTION()
    void OnShowWindow_Callback(UDSWindowWidget* InWindow);

    UFUNCTION()
    void OnCloseWindow_Callback(UDSWindowWidget* InWindow);

    virtual void BeginPlay() override;

private:
    FTimerManager HUDTimerManager;

    FTimerHandle LoadingScreenTimerHandle;

    UPROPERTY()
    UDSWindowWidget* RootWindow = nullptr;

    UPROPERTY()
    TArray<UDSWindowWidget*> WindowsArray;

    UPROPERTY()
    UDSScreenMessageWidget* ScreenMessageWidget = nullptr;

    UFUNCTION()
    void LoadingScreenFinished_Callback();
};
