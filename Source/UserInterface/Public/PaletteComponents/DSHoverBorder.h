// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/Border.h"
#include "DSHoverBorder.generated.h"

UCLASS()
class USERINTERFACE_API UDSHoverBorder : public UBorder
{
    GENERATED_BODY()

public:
#if WITH_EDITOR
    virtual const FText GetPaletteCategory() override;
#endif

    virtual void SynchronizeProperties() override;

    void SetBorderColor(const FLinearColor& InColor);

    void SetDefaultColor();

    void SetCanHover(const bool CanHover) { bCanHover = CanHover; }

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style")
    FLinearColor NormalColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style")
    FLinearColor HoverColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style")
    FLinearColor ContentNormalColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style")
    FLinearColor ContentHoverColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Sounds")
    USoundBase* HoverSound;

    virtual void OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent);

    virtual void OnMouseLeave_Callback(const FPointerEvent& InEvent);

    bool bCanHover = true;
};
