// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "PaletteComponents/DSHoverBorder.h"
#include "DSClickBorder.generated.h"

class UDSClickBorder;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBorderFocused, UDSClickBorder*, InFocusedBorder);

UCLASS()
class USERINTERFACE_API UDSClickBorder : public UDSHoverBorder
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnBorderFocused OnBorderFocused;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    void SetBorderFocused(const bool IsFocused);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FLinearColor& GetContentNormalColor() const { return ContentNormalColor; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FLinearColor& GetContentHoverColor() const { return ContentHoverColor; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    const FLinearColor& GetContentFocusColor() const { return ContentFocusColor; }

    virtual void SynchronizeProperties() override;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style", meta=(DisplayAfter="HoverColor"))
    FLinearColor FocusColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style", meta=(DisplayAfter="ContentHoverColor"))
    FLinearColor ContentFocusColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Sounds", meta=(DisplayAfter="HoverSound"))
    USoundBase* FocusSound;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker")
    bool bIsHandledMouseClick = true;

    FReply OnMouseButtonDown_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent);

    virtual void OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent) override;

    virtual void OnMouseLeave_Callback(const FPointerEvent& InEvent) override;

private:
    bool bIsFocused = false;
};
