// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "DSButton.generated.h"

UCLASS()
class USERINTERFACE_API UDSButton : public UButton
{
    GENERATED_BODY()

public:
#if WITH_EDITOR
    virtual const FText GetPaletteCategory() override;
#endif

    virtual void SynchronizeProperties() override;

    void SetContentColorAndOpacity(const FLinearColor& InColor);

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style", meta=(DisplayAfter="ContentHoverColor"))
    FLinearColor ContentNormalColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style", meta=(DisplayAfter="ContentHoverColor"))
    FLinearColor ContentHoverColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DungeonSeeker|Style", meta=(DisplayAfter="ContentHoverColor"))
    FLinearColor ContentPressedColor = FLinearColor{1.f, 1.f, 1.f, 1.f};

    virtual TSharedRef<SWidget> RebuildWidget() override;

    FReply OnClicked_Callback();

    void OnHovered_Callback();

    void OnUnhovered_Callback();

    void OnPressed_Callback();

    void OnReleased_Callback();
};
