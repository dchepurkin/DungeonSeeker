// Created by Dmirtiy Chepurkin. All right reserved

#include "DSHUD.h"
#include "DSWidget.h"
#include "DSWindowWidget.h"
#include "DSDialogWindowWidget.h"
#include "DSLoadingScreenWidget.h"
#include "DSScreenMessageWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSHUD, All, All);

ADSHUD::ADSHUD()
{
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bTickEvenWhenPaused = true;
    PrimaryActorTick.bStartWithTickEnabled = false;
}

void ADSHUD::BeginPlay()
{
    Super::BeginPlay();
}

void ADSHUD::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    HUDTimerManager.Tick(DeltaSeconds);
}

UDSWindowWidget* ADSHUD::CreateRootWindow(const TSubclassOf<UDSWindowWidget> InRootWindowClass, const bool bNotHiddable)
{
    if (!InRootWindowClass)
    {
        UE_LOG(LogDSHUD, Warning, TEXT("RootWindowClass is NULL"));
        return nullptr;
    }

    CLoseAll();
    if (RootWindow) { RootWindow->RemoveFromParent(); }

    RootWindow = CreateWindow(nullptr, InRootWindowClass);
    if (RootWindow)
    {
        RootWindow->Show({EBlockWindows::None, false, bNotHiddable});
    }

    return RootWindow;
}

UDSWindowWidget* ADSHUD::CreateWindow(UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass)
{
    const auto NewWindow = InParent
                               ? CreateWidget<UDSWindowWidget>(InParent, InWindowClass)
                               : CreateWidget<UDSWindowWidget>(GetOwningPlayerController(), InWindowClass);

    if (NewWindow)
    {
        NewWindow->SetParent(Cast<UDSWindowWidget>(InParent));
        NewWindow->OnShowWindow.AddDynamic(this, &ThisClass::OnShowWindow_Callback);
        NewWindow->OnCloseWindow.AddDynamic(this, &ThisClass::OnCloseWindow_Callback);
    }

    return NewWindow;
}

UDSDialogWindowWidget* ADSHUD::OpenDialog(UDSWidget* InParent, const TSubclassOf<UDSDialogWindowWidget> InDialogClass, const FText& InDialogText)
{
    if (const auto Dialog = CreateWindow<UDSDialogWindowWidget>(InParent, InDialogClass))
    {
        Dialog->SetDialogMessage(InDialogText);

        FWindowParameters Params;
        Params.bNotHiddable = true;
        Params.BlockOtherWindows = EBlockWindows::Block;
        Dialog->Show(Params);
        return Dialog;
    }

    return nullptr;
}

void ADSHUD::SetInputMode(const EDSInputMode InMode)
{
    if (const auto Controller = GetOwningPlayerController())
    {
        switch (InMode)
        {
        case EDSInputMode::Game:
            {
                Controller->SetInputMode(FInputModeGameOnly{});
                Controller->SetShowMouseCursor(false);
                Controller->SetPause(false);
                break;
            }
        case EDSInputMode::UIWithPause:
            {
                Controller->SetInputMode(FInputModeUIOnly{});
                Controller->SetShowMouseCursor(true);
                Controller->SetPause(true);
                break;
            }
        case EDSInputMode::UI:
            {
                Controller->SetInputMode(FInputModeGameAndUI{});
                Controller->SetShowMouseCursor(true);
                break;
            }
        case EDSInputMode::LoadingScreen:
            {
                Controller->SetInputMode(FInputModeUIOnly{});
                Controller->SetShowMouseCursor(false);
                Controller->SetPause(true);
                break;
            }
        }
    }
}

void ADSHUD::CLoseAll() const
{
    if (!IsValid(RootWindow)) { return; }

    RootWindow->Close();

    const auto ClosedWindows = WindowsArray;
    for (const auto Window : ClosedWindows)
    {
        if (IsValid(Window))
        {
            Window->Close();
        }
    }
}

void ADSHUD::ResumeGame()
{
    CLoseAll();
    SetInputMode(EDSInputMode::Game);
}

void ADSHUD::ShowLoadingScreen(UTexture2D* InLoadingScreenImage)
{
    if (LoadingScreenWidgetClass)
    {
        if (const auto LoadingScreen = CreateWindow<UDSLoadingScreenWidget>(nullptr, LoadingScreenWidgetClass))
        {
            RootWindow = LoadingScreen;
            LoadingScreen->SetBackground(InLoadingScreenImage);
            LoadingScreen->Show({EBlockWindows::Hide});
        }

        CLoseAll();
        SetInputMode(EDSInputMode::LoadingScreen);
        SetActorTickEnabled(true);

        HUDTimerManager.SetTimer(LoadingScreenTimerHandle, this, &ThisClass::LoadingScreenFinished_Callback, LoadingScreenTime);
    }
    else
    {
        UE_LOG(LogDSHUD, Error, TEXT("LoadingScreenWidgetClass is NULL"));
        LoadingScreenFinished_Callback();
    }
}

void ADSHUD::OnShowWindow_Callback(UDSWindowWidget* InWindow)
{
    if (!IsValid(InWindow)) return;

    for (const auto& Window : WindowsArray)
    {
        if (!IsValid(Window)) continue;
        Window->Block(InWindow->WindowParameters.BlockOtherWindows);
    }

    if (!WindowsArray.Contains(InWindow))
    {
        WindowsArray.Add(InWindow);
    }
}

void ADSHUD::OnCloseWindow_Callback(UDSWindowWidget* InWindow)
{
    if (!IsValid(InWindow)) return;

    InWindow->OnShowWindow.RemoveDynamic(this, &ThisClass::OnShowWindow_Callback);
    InWindow->OnCloseWindow.RemoveDynamic(this, &ThisClass::OnCloseWindow_Callback);
    WindowsArray.RemoveSingle(InWindow);

    EBlockWindows BlockParam = EBlockWindows::None;

    for (auto i = WindowsArray.Num() - 1; i >= 0; --i)
    {
        if (!WindowsArray.IsValidIndex(i)) continue;

        const auto Window = WindowsArray[i];

        BlockParam == EBlockWindows::None
            ? Window->Unblock()
            : Window->Block(BlockParam);

        if (BlockParam == EBlockWindows::None)
        {
            BlockParam = Window->WindowParameters.BlockOtherWindows;
        }
        else if (BlockParam == EBlockWindows::Block)
        {
            BlockParam = Window->WindowParameters.BlockOtherWindows == EBlockWindows::Hide ? EBlockWindows::Hide : BlockParam;
        }
    }
}

void ADSHUD::LoadingScreenFinished_Callback()
{
    SetActorTickEnabled(false);
    OnLoadingScreenFinished.Broadcast();
}

void ADSHUD::ShowMessage(const FText& InMessageText, const FLinearColor& InMessageColor)
{
    if (!ScreenMessageWidgetClass)
    {
        UE_LOG(LogDSHUD, Warning, TEXT("ScreenMessageWidgetClass is NULL"));
    }

    if (InMessageText.IsEmpty()) return;

    if (!IsValid(ScreenMessageWidget))
    {
        ScreenMessageWidget = CreateWidget<UDSScreenMessageWidget>(GetOwningPlayerController(), ScreenMessageWidgetClass);
    }

    if (!ScreenMessageWidget) return;

    ScreenMessageWidget->Init(InMessageText, InMessageColor, ScreenMessageLifeTime);
}

/*bool ADSHUD::ExecuteKeyEvents(const FKey& InKey)
{
	bool Result = false;

	const auto Windows = WindowsArray;
	for(const auto Window : Windows)
	{
		if(Window && Window->ExecuteKeyEvent(InKey))
		{
			Result = true;
		}
	}

	return Result;
}*/
