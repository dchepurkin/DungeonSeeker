// Created by Dmitriy Chepurkin. All rights reserved

#include "DSUIFunctionLibrary.h"
#include "DSHUD.h"

void UDSUIFunctionLibrary::ShowMessage(UObject* InWorldContext, const FText InMessageText, const FLinearColor InMessageColor)
{
    if (const auto UltimateHUD = GetDSHUD(InWorldContext))
    {
        UltimateHUD->ShowMessage(std::move(InMessageText), std::move(InMessageColor));
    }
}

ADSHUD* UDSUIFunctionLibrary::GetDSHUD(UObject* InWorldContext)
{
    if (const auto World = InWorldContext->GetWorld())
    {
        if (const auto PlayerController = World->GetFirstPlayerController())
        {
            return PlayerController->GetHUD<ADSHUD>();
        }
    }

    return nullptr;
}

UDSWindowWidget* UDSUIFunctionLibrary::GetRootWindow(UObject* InWorldContext)
{
    const auto HUD = GetDSHUD(InWorldContext);
    return HUD ? HUD->GetRootWindow() : nullptr;
}

UDSWindowWidget* UDSUIFunctionLibrary::CreateWindow(UObject* InWorldContext, UDSWidget* InParent, const TSubclassOf<UDSWindowWidget> InWindowClass)
{
    const auto HUD = GetDSHUD(InWorldContext);
    return HUD ? HUD->CreateWindow(InParent, InWindowClass) : nullptr;
}
