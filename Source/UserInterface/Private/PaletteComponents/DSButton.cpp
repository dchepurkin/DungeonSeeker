// Created by Dmirtiy Chepurkin. All right reserved

#include "PaletteComponents/DSButton.h"
#include "Components/ButtonSlot.h"

#define LOCTEXT_NAMESPACE "DungeonSeeker"

#if WITH_EDITOR
const FText UDSButton::GetPaletteCategory()
{
    return LOCTEXT("DungeonSeeker", "DungeonSeeker");
}
#endif

void UDSButton::SynchronizeProperties()
{
    Super::SynchronizeProperties();

    SetContentColorAndOpacity(ContentNormalColor);
}

TSharedRef<SWidget> UDSButton::RebuildWidget()
{
    MyButton = SNew(SButton)
        .OnClicked(BIND_UOBJECT_DELEGATE(FOnClicked, OnClicked_Callback))
        .OnPressed(BIND_UOBJECT_DELEGATE(FSimpleDelegate, OnPressed_Callback))
        .OnReleased(BIND_UOBJECT_DELEGATE(FSimpleDelegate, OnReleased_Callback))
        .OnHovered_UObject(this, &ThisClass::OnHovered_Callback)
        .OnUnhovered_UObject(this, &ThisClass::OnUnhovered_Callback)
        .ButtonStyle(&GetStyle())
        .ClickMethod(GetClickMethod())
        .TouchMethod(GetTouchMethod())
        .PressMethod(GetPressMethod())
        .IsFocusable(false);

    if (GetChildrenCount() > 0)
    {
        Cast<UButtonSlot>(GetContentSlot())->BuildSlot(MyButton.ToSharedRef());
    }

    return MyButton.ToSharedRef();
}

void UDSButton::SetContentColorAndOpacity(const FLinearColor& InColor)
{
    SetColorAndOpacity(InColor);
}

FReply UDSButton::OnClicked_Callback()
{
    OnClicked.Broadcast();
    return FReply::Handled();
}

void UDSButton::OnHovered_Callback()
{
    if (!IsPressed())
    {
        SetContentColorAndOpacity(ContentHoverColor);
    }
    OnHovered.Broadcast();
}

void UDSButton::OnUnhovered_Callback()
{
    if (!IsPressed())
    {
        SetContentColorAndOpacity(ContentNormalColor);
    }
    OnUnhovered.Broadcast();
}

void UDSButton::OnPressed_Callback()
{
    SetContentColorAndOpacity(ContentPressedColor);
    OnPressed.Broadcast();
}

void UDSButton::OnReleased_Callback()
{
    SetContentColorAndOpacity(IsHovered() ? ContentHoverColor : ContentNormalColor);
    OnReleased.Broadcast();
}

#undef LOCTEXT_NAMESPACE
