// Created by Dmirtiy Chepurkin. All right reserved

#include "PaletteComponents/DSClickBorder.h"
#include "Kismet/GameplayStatics.h"

void UDSClickBorder::SynchronizeProperties()
{
    Super::SynchronizeProperties();

    MyBorder->SetOnMouseButtonDown(BIND_UOBJECT_DELEGATE(FPointerEventHandler, OnMouseButtonDown_Callback));
}

FReply UDSClickBorder::OnMouseButtonDown_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
    if (InEvent.GetEffectingButton() == EKeys::LeftMouseButton)
    {
        if (!bIsFocused)
        {
            SetBorderFocused(true);
            UGameplayStatics::PlaySound2D(this, FocusSound);
        }
    }

    return bIsHandledMouseClick ? FReply::Handled() : FReply::Unhandled();
}

void UDSClickBorder::OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
    if (bIsFocused) return;

    Super::OnMouseEnter_Callback(InGeometry, InEvent);
}

void UDSClickBorder::OnMouseLeave_Callback(const FPointerEvent& InEvent)
{
    if (bIsFocused) return;

    Super::OnMouseLeave_Callback(InEvent);
}

void UDSClickBorder::SetBorderFocused(const bool IsFocused)
{
    bIsFocused = IsFocused;

    if (bIsFocused)
    {
        OnBorderFocused.Broadcast(this);
    }

    SetBorderColor(bIsFocused ? FocusColor : NormalColor);
    SetContentColorAndOpacity(bIsFocused ? ContentFocusColor : ContentNormalColor);
}
