// Created by Dmirtiy Chepurkin. All right reserved

#include "PaletteComponents/DSHoverBorder.h"
#include "Kismet/GameplayStatics.h"

#define LOCTEXT_NAMESPACE "DungeonSeeker"

#if WITH_EDITOR
const FText UDSHoverBorder::GetPaletteCategory()
{
    return LOCTEXT("DungeonSeeker", "DungeonSeeker");
}
#endif

void UDSHoverBorder::SynchronizeProperties()
{
    Super::SynchronizeProperties();

    MyBorder->SetOnMouseEnter(BIND_UOBJECT_DELEGATE(FNoReplyPointerEventHandler, OnMouseEnter_Callback));
    MyBorder->SetOnMouseLeave(BIND_UOBJECT_DELEGATE(FSimpleNoReplyPointerEventHandler, OnMouseLeave_Callback));

    SetBrushColor(NormalColor);
    SetContentColorAndOpacity(ContentNormalColor);
}

void UDSHoverBorder::SetBorderColor(const FLinearColor& InColor)
{
    auto NewBrush = Background;
    NewBrush.OutlineSettings.Color = InColor;
    SetBrush(NewBrush);
    SetBrushColor(InColor);
}

void UDSHoverBorder::SetDefaultColor()
{
    SetBorderColor(NormalColor);
}

void UDSHoverBorder::OnMouseEnter_Callback(const FGeometry& InGeometry, const FPointerEvent& InEvent)
{
    if(!bCanHover) return;

    SetBorderColor(HoverColor);
    SetContentColorAndOpacity(ContentHoverColor);
    UGameplayStatics::PlaySound2D(this, HoverSound);
}

void UDSHoverBorder::OnMouseLeave_Callback(const FPointerEvent& InEvent)
{
    if(!bCanHover) return;

    SetBorderColor(NormalColor);
    SetContentColorAndOpacity(ContentNormalColor);
}

#undef LOCTEXT_NAMESPACE