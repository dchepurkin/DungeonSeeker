// Created by Dmitriy Chepurkin. All rights reserved

#include "DSLoadingScreenWidget.h"
#include "Components/Image.h"

void UDSLoadingScreenWidget::SetBackground(UTexture2D* InTexture)
{
    BackgroundImage->SetBrushFromTexture(InTexture);
}
