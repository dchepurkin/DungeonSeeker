// Created by Dmirtiy Chepurkin. All right reserved

#include "DSSwitcherBase.h"
#include "Components/TextBlock.h"

void UDSSwitcherBase::SynchronizeProperties()
{
    Super::SynchronizeProperties();

    if(SelectedOptionTextBlock)
    {
        if(!DefaultOptions.Contains(SelectedOption))
        {
            SelectedOption = DefaultOptions.IsValidIndex(0) ? DefaultOptions[0] : "";
        }
        SelectedOptionTextBlock->SetText(FText::FromString(SelectedOption.IsEmpty() ? "Add options" : SelectedOption));
    }
}

void UDSSwitcherBase::ClearOptions()
{
    DefaultOptions.Empty();
}

void UDSSwitcherBase::AddOption(const FString& InOption)
{
    if(InOption.IsEmpty()) return;

    DefaultOptions.Add(InOption);
}

void UDSSwitcherBase::SetCurrentOption(const FString& InOption)
{
    if(!DefaultOptions.Contains(InOption)) return;

    SelectedOption = InOption;
    SelectedOptionTextBlock->SetText(FText::FromString(InOption));
}

void UDSSwitcherBase::SetCurrentOption(const int32 InOptionIndex)
{
    if(!DefaultOptions.IsValidIndex(InOptionIndex)) return;

    SetCurrentOption(DefaultOptions[InOptionIndex]);
}

void UDSSwitcherBase::SelectOption(const FString& InOption)
{
    if(!DefaultOptions.Contains(InOption)) return;

    SetCurrentOption(InOption);
    OnOptionChanged.Broadcast(DefaultOptions.Find(InOption));
}

void UDSSwitcherBase::SelectOption(const int32 InOptionIndex)
{
    if(!DefaultOptions.IsValidIndex(InOptionIndex)) return;

    SelectOption(DefaultOptions[InOptionIndex]);
}

const TArray<FString>& UDSSwitcherBase::GetOptions() const
{
    return DefaultOptions;
}
