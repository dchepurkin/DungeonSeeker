// Created by Dmitriy Chepurkin. All rights reserved

#include "DSWindowWidget.h"
#include "DSHUD.h"
#include "Components/Button.h"

DEFINE_LOG_CATEGORY_STATIC(LogUltimateWindowWidget, All, All);

#define LOCTEXT_NAMESPACE "Ultimate"

void UDSWindowWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	InitVisibility = GetVisibility();
	SetVisibility(ESlateVisibility::Hidden);

	if(ShowAnimation)
	{
		OnShowAnimationStarted.BindDynamic(this, &ThisClass::OnShowAnimationStarted_Callback);
		OnShowAnimationFinished.BindDynamic(this, &ThisClass::OnShowAnimationFinished_Callback);

		BindToAnimationStarted(ShowAnimation, OnShowAnimationStarted);
		BindToAnimationFinished(ShowAnimation, OnShowAnimationFinished);
	}

	if(CloseButton)
	{
		CloseButton->OnClicked.AddDynamic(this, &ThisClass::Close);
	}
}

void UDSWindowWidget::SetParent(UDSWindowWidget* InWindow)
{
	if(!InWindow) return;

	ParentWindow = InWindow;
	ParentWindow->AddChild(this);
}

void UDSWindowWidget::AddChild(UDSWindowWidget* InWindow)
{
	if(!InWindow) return;
	ChildWindows.Add(InWindow);
}

void UDSWindowWidget::Show(const FWindowParameters& InWindowParameters)
{
	if(GetVisibility() == ESlateVisibility::Hidden)
	{
		WindowParameters = InWindowParameters;

		PlayAnimation(ShowAnimation);
		SetState(EBlockWindows::None);

		if(!IsInViewport())
		{
			AddToViewport();
		}

		OnShowWindow.Broadcast(this);

		if(InWindowParameters.bSetCursorOnScreenCenter)
		{
			SetMouseInScreenCenter();
		}
	}
}

void UDSWindowWidget::Close()
{
	const auto Windows = ChildWindows;
	for(const auto Child : Windows)
	{
		if(!IsValid(Child) || !Child->IsInViewport()) continue;
		Child->Close();
	}

	if(IsRootWindow()) return;

	OnCloseWindow.Broadcast(this);

	RemoveFromParent();

	if(WindowParameters.bClosedWithParentWindow && ParentWindow)
	{
		ParentWindow->Close();
	}
}

void UDSWindowWidget::Block(const EBlockWindows InParam)
{
	if(InParam == EBlockWindows::None) return;

	SetState(InParam);
}

void UDSWindowWidget::Unblock()
{
	SetState(EBlockWindows::None);
}

//AlignmentRation �������� ��� AlignmentRation.X = 3, ���� ���������� ������� ������ 2\3 ������ �� Pivot ������� ����� ������������� ������
void UDSWindowWidget::SetPosition(const FVector2D& InPosition, const FVector2D& AlignmentRatio)
{
	if(const auto HUD = GetDSHUD(); const auto Controller = HUD->GetOwningPlayerController())
	{
		FIntPoint ViewportSize;
		Controller->GetViewportSize(ViewportSize.X, ViewportSize.Y);

		SetPositionInViewport(InPosition);

		FVector2D Alignment{0.f, 0.f};
		if(AlignmentRatio.X > 1)
			Alignment.X = InPosition.X > (AlignmentRatio.X - 1) * ViewportSize.X / AlignmentRatio.X ? 1.f : 0.f;

		if(AlignmentRatio.Y > 1)
			Alignment.Y = InPosition.Y > (AlignmentRatio.Y - 1) * ViewportSize.Y / AlignmentRatio.Y ? 1.f : 0.f;

		SetAlignmentInViewport(Alignment);
	}
}

bool UDSWindowWidget::IsRootWindow() const
{
	const auto HUD = GetDSHUD();
	return HUD && HUD->GetRootWindow() == this;
}

void UDSWindowWidget::RemoveFromParent()
{
	Super::RemoveFromParent();

	if(ParentWindow)
	{
		ParentWindow->ChildWindows.RemoveSingle(this);
	}
}

void UDSWindowWidget::SetState(const EBlockWindows InParam)
{
	ESlateVisibility NewVisibility;

	switch(InParam)
	{
	case EBlockWindows::Block:
		//if window also hidden, we dont change visibility
		if(GetVisibility() != ESlateVisibility::Hidden)
		{
			NewVisibility = ESlateVisibility::HitTestInvisible;
			break;
		}
	case EBlockWindows::Hide:
		{
			NewVisibility = WindowParameters.bNotHiddable ? ESlateVisibility::HitTestInvisible : ESlateVisibility::Hidden;
			break;
		}
	case EBlockWindows::None:
	default: NewVisibility = InitVisibility;
	}

	SetVisibility(NewVisibility);
}

void UDSWindowWidget::SetMouseInScreenCenter()
{
	if(const auto Controller = GetOwningPlayer())
	{
		FIntPoint ScreenSize;
		Controller->GetViewportSize(ScreenSize.X, ScreenSize.Y);
		Controller->SetMouseLocation(ScreenSize.X / 2, ScreenSize.Y / 2);
	}
}

#undef LOCTEXT_NAMESPACE
