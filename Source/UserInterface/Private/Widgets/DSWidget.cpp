// Created by Dmirtiy Chepurkin. All right reserved

#include "Widgets/DSWidget.h"
#include "DSHUD.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSWidget, All, All);

ADSHUD* UDSWidget::GetDSHUD() const
{
    const auto Controller = GetOwningPlayer();
    return Controller ? Controller->GetHUD<ADSHUD>() : nullptr;
}
