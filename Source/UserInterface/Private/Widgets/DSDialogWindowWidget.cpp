// Created by Dmitriy Chepurkin. All rights reserved

#include "DSDialogWindowWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#define LOCTEXT_NAMESPACE "Ultimate"

void UDSDialogWindowWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (YesButton)
    {
        YesButton->OnClicked.AddDynamic(this, &ThisClass::YesButton_Callback);
    }

    if (NoButton)
    {
        NoButton->OnClicked.AddDynamic(this, &ThisClass::NoButton_Callback);
    }

    if (CancelButton)
    {
        CancelButton->OnClicked.AddDynamic(this, &ThisClass::CancelButton_Callback);
    }
}

void UDSDialogWindowWidget::YesButton_Callback()
{
    WindowParameters.bClosedWithParentWindow = DialogWindowParameters.bClosedWithParentWindowOnYes;
    OnDialogClosed.Broadcast(this, EDialogCallback::Yes);
    Close();
}

void UDSDialogWindowWidget::NoButton_Callback()
{
    WindowParameters.bClosedWithParentWindow = DialogWindowParameters.bClosedWithParentWindowOnNo;
    OnDialogClosed.Broadcast(this, EDialogCallback::No);
    Close();
}

void UDSDialogWindowWidget::CancelButton_Callback()
{
    WindowParameters.bClosedWithParentWindow = DialogWindowParameters.bClosedWithParentWindowOnCancel;
    OnDialogClosed.Broadcast(this, EDialogCallback::Cancel);
    Close();
}

void UDSDialogWindowWidget::SetDialogParameters(const FDialogWindowParameters& InParameters)
{
    DialogWindowParameters = InParameters;
}

void UDSDialogWindowWidget::SetDialogMessage(const FText& InMessage) const
{
    if (!DialogMessageTextBlock || InMessage.IsEmpty()) return;
    DialogMessageTextBlock->SetText(InMessage);
}

#undef LOCTEXT_NAMESPACE
