// Created by Dmirtiy Chepurkin. All right reserved

#include "Widgets/DSSwitcher.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UDSSwitcher::SetCurrentOption(const FString& InOption)
{
    Super::SetCurrentOption(InOption);

    SetEnabled();
}

void UDSSwitcher::SetCurrentOption(const int32 InOptionIndex)
{
    Super::SetCurrentOption(InOptionIndex);
}

void UDSSwitcher::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    LeftSwitchButton->OnClicked.AddDynamic(this, &ThisClass::OnLeftClicked_Callback);
    RightSwitchButton->OnClicked.AddDynamic(this, &ThisClass::OnRightClicked_Callback);

    SetEnabled();
}

void UDSSwitcher::OnLeftClicked_Callback()
{
    if (DefaultOptions.IsEmpty() || !DefaultOptions.Contains(SelectedOption)) return;

    auto Index = DefaultOptions.Find(SelectedOption);
    RightSwitchButton->SetIsEnabled(true);

    if (!bIsCircleSwitch)
    {
        Index = FMath::Clamp(--Index, 0, Index);
        if (!Index)
        {
            LeftSwitchButton->SetIsEnabled(false);
        }
    }
    else
    {
        Index = Index > 0 ? --Index : DefaultOptions.Num() - 1;
    }

    if (!DefaultOptions.IsValidIndex(Index)) return;

    SelectOption(DefaultOptions[Index]);
}

void UDSSwitcher::OnRightClicked_Callback()
{
    if (DefaultOptions.IsEmpty() || !DefaultOptions.Contains(SelectedOption)) return;

    auto Index = DefaultOptions.Find(SelectedOption);
    LeftSwitchButton->SetIsEnabled(true);

    if (!bIsCircleSwitch)
    {
        Index = FMath::Clamp(++Index, 0, DefaultOptions.Num() - 1);
        if (Index == DefaultOptions.Num() - 1)
        {
            RightSwitchButton->SetIsEnabled(false);
        }
    }
    else
    {
        Index = Index < DefaultOptions.Num() - 1 ? ++Index : 0;
    }

    if (!DefaultOptions.IsValidIndex(Index)) return;

    SelectOption(DefaultOptions[Index]);
}

void UDSSwitcher::SetEnabled() const
{
    if (!bIsCircleSwitch)
    {
        const bool IsSelectedOptionValid = DefaultOptions.Contains(SelectedOption);
        const auto SelectedOptionIndex = DefaultOptions.Find(SelectedOption);

        LeftSwitchButton->SetIsEnabled(IsSelectedOptionValid && SelectedOptionIndex != 0);
        RightSwitchButton->SetIsEnabled(IsSelectedOptionValid && SelectedOptionIndex != DefaultOptions.Num() - 1);
    }
}
