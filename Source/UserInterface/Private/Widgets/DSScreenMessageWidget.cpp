// Created by Dmitriy Chepurkin. All rights reserved

#include "DSScreenMessageWidget.h"
#include "Components/TextBlock.h"

void UDSScreenMessageWidget::Tick(float DeltaTime)
{
    TimerManager.Tick(DeltaTime);
}

void UDSScreenMessageWidget::Init(const FText& InText, const FLinearColor& InTextColor, const float InLifeTime)
{
    StopAnimation(FadeOutAnimation);
    SetRenderOpacity(1.f);

    MessageTextBlock->SetText(InText);
    MessageTextBlock->SetColorAndOpacity(InTextColor);

    TimerManager.SetTimer(LifeTimerHandle, this, &ThisClass::LifeTimer_Callback, InLifeTime);

    if(!IsInViewport())
    {
        AddToViewport();
    }
}

void UDSScreenMessageWidget::LifeTimer_Callback()
{
    PlayAnimation(FadeOutAnimation);
}

void UDSScreenMessageWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if(Animation == FadeOutAnimation)
    {
        RemoveFromParent();
    }
}
