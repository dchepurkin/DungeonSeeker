// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityTypes.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DSAbilityFunctionLibrary.generated.h"

class UDSAbilityComponent;
class UDSAbilityBase;

UCLASS()
class ABILITYSYSTEM_API UDSAbilityFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	static bool StartAbility(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser, AActor* InAbilityTarget = nullptr);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	static UDSAbilityBase* StartAbilityDeferred(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	static bool FinishStartingAbility(UDSAbilityBase* InAbility, AActor* InAbilityTarget = nullptr);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	static EInterruptAbilityResult InterruptAbility(AActor* InAbilityUser);

	UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
	static UDSAbilityComponent* GetAbilityComponent(AActor* InActor);
};
