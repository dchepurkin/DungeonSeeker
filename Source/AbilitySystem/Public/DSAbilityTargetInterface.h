// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DSAbilityTargetInterface.generated.h"

class UDSAbilityBase;

UINTERFACE(MinimalAPI)
class UDSAbilityTargetInterface : public UInterface
{
    GENERATED_BODY()
};

class ABILITYSYSTEM_API IDSAbilityTargetInterface
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|Ability")
    void AbilityStarted(UDSAbilityBase* Ability, AActor* AbilityUser);

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|Ability")
    void AbilityTriggered(UDSAbilityBase* Ability, AActor* AbilityUser);

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker|Ability")
    void AbilityFinished(UDSAbilityBase* Ability, AActor* AbilityUser, bool bInterrupted);

private:
    virtual void AbilityStarted_Implementation(UDSAbilityBase* Ability, AActor* AbilityUser) {}

    virtual void AbilityTriggered_Implementation(UDSAbilityBase* Ability, AActor* AbilityUser) {}

    virtual void AbilityFinished_Implementation(UDSAbilityBase* Ability, AActor* AbilityUser, bool bInterrupted) {}
};
