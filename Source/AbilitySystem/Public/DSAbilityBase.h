// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityTypes.h"
#include "DSAbilityBase.generated.h"

class UDSAbilityAnimNotify;
class UDSAbilityBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAbility, UDSAbilityBase*, Ability, AActor*, AbilityUser, AActor*, AbilityTarget);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnAbilityFinished, UDSAbilityBase*, Ability, AActor*, AbilityUser, AActor*, AbilityTarget, bool, bInterrupted);

UCLASS(Abstract, Blueprintable)
class ABILITYSYSTEM_API UDSAbilityBase : public UObject
{
    GENERATED_BODY()

    friend class UDSAbilityComponent;

public:
    UDSAbilityBase();

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnAbility OnAbilityStarted;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnAbility OnAbilityTriggered;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnAbilityFinished OnAbilityFinished;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    virtual EInterruptAbilityResult Interrupt();

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsActive() const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    FORCEINLINE AActor* GetUser() const { return AbilityUser; }

    virtual UWorld* GetWorld() const override { return AbilityUser ? AbilityUser->GetWorld() : nullptr; }

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    bool bCanInterrupt = true;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker")
    UAnimMontage* AbilityAnimation = nullptr;

    UPROPERTY(BlueprintReadOnly, Category="DungeonSeeker")
    AActor* AbilityUser = nullptr;

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    void OnStart();

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    void OnTrigger();

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    void OnFinish(bool bInterrupted);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker", meta=(BlueprintProtected))
    virtual UAnimMontage* GetAbilityAnimation() const { return AbilityAnimation; }

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker", meta=(BlueprintProtected))
    virtual AActor* GetAbilityTarget() const { return AbilityTarget; }

    UFUNCTION()
    virtual void MontageEnded(UAnimMontage* Montage, bool bInterrupted);

    UFUNCTION()
    virtual void Trigger();

    UFUNCTION()
    virtual void Finish(bool bInterrupted);

    virtual bool Start(AActor* InTarget, FText& FailMessage);

private:
    bool bIsActive = false;

    UPROPERTY()
    AActor* AbilityTarget = nullptr;

    UPROPERTY()
    UAnimInstance* AnimInstance = nullptr;

    UPROPERTY()
    UDSAbilityAnimNotify* AbilityNotify = nullptr;

    void ClearBindings();
};
