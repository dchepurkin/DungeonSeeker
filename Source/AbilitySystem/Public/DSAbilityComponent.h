// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityTypes.h"
#include "Components/ActorComponent.h"
#include "DSAbilityComponent.generated.h"

class UDSAbilityBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAbilityStarted, UDSAbilityBase*, Ability, AActor*, AbilityTarget);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAbilityFailed, UDSAbilityBase*, Ability, FText, FailMessage);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ABILITYSYSTEM_API UDSAbilityComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UDSAbilityComponent();

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnAbilityStarted OnAbilityStarted;

    UPROPERTY(BlueprintAssignable, Category="DungeonSeeker")
    FOnAbilityFailed OnAbilityFailed;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category="DungeonSeeker")
    bool IsActiveAbility() const;

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    UDSAbilityBase* SpawnAbility(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    bool StartAbility(UDSAbilityBase* InAbility, AActor* InAbilityTarget);

    UFUNCTION(BlueprintCallable, Category="DungeonSeeker")
    EInterruptAbilityResult InterruptAbility();

protected:
    virtual void BeginPlay() override;

private:
    UPROPERTY()
    UDSAbilityBase* CurrentAbility = nullptr;

    UFUNCTION()
    void OnAbilityFinished(UDSAbilityBase* Ability, AActor* AbilityUser, AActor* AbilityTarget, bool bIsInterrupted);

    bool ExecuteCurrentAbility(AActor* InAbilityTarget) const;
};
