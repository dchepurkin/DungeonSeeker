// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "DSAbilityBase.h"
#include "DSRangeAbility.generated.h"

UCLASS(Abstract, Blueprintable)
class ABILITYSYSTEM_API UDSRangeAbility : public UDSAbilityBase
{
    GENERATED_BODY()

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker", meta=(ClampMin=0.f))
    float MinDistance = 500.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|FailMessages")
    FText NotValidTargetMessage{};

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="DungeonSeeker|FailMessages")
    FText TargetTooFarMessage{};

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    float GetMinDistance() const;

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    FText GetNotValidTargetMessage() const;

    UFUNCTION(BlueprintNativeEvent, Category="DungeonSeeker")
    FText GetTargetTooFarMessage() const;

    FORCEINLINE virtual float GetMinDistance_Implementation() const { return MinDistance; }

    FORCEINLINE virtual FText GetNotValidTargetMessage_Implementation() const { return NotValidTargetMessage; }

    FORCEINLINE virtual FText GetTargetTooFarMessage_Implementation() const { return TargetTooFarMessage; }

    virtual bool Start(AActor* InTarget, FText& FailMessage) override;
};
