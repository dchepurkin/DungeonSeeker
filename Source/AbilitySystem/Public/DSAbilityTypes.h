// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EInterruptAbilityResult : uint8
{
	NotActiveAbility,
	CantInterrupt,
	Interrupted
};
