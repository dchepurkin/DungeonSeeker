// Created by Dmirtiy Chepurkin. All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "DSAbilityAnimNotify.generated.h"

DECLARE_DYNAMIC_DELEGATE(FOnAbilityTriggered);

UCLASS(DisplayName="Ability Notify")
class ABILITYSYSTEM_API UDSAbilityAnimNotify : public UAnimNotify
{
    GENERATED_BODY()

public:
    FOnAbilityTriggered OnAbilityTriggered;

    virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

    virtual FString GetNotifyName_Implementation() const override;
};
