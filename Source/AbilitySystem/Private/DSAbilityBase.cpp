// Created by Dmirtiy Chepurkin. All right reserved

#include "DSAbilityBase.h"

#include "DSAbilityAnimNotify.h"
#include "DSAbilityConfig.h"
#include "DSAbilityTargetInterface.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSAbilityBase, All, All)

UDSAbilityBase::UDSAbilityBase()
{
    AbilityUser = Cast<AActor>(GetOuter());
}

bool UDSAbilityBase::Start(AActor* InTarget, FText& FailMessage)
{
    if (IsActive()) { return false; }

    bIsActive = true;
    AbilityTarget = InTarget;

    OnStart();
    if (IsValid(GetAbilityTarget()) && GetAbilityTarget()->Implements<UDSAbilityTargetInterface>())
    {
        IDSAbilityTargetInterface::Execute_AbilityStarted(GetAbilityTarget(), this, AbilityUser);
    }
    OnAbilityStarted.Broadcast(this, AbilityUser, GetAbilityTarget());

    if (const auto Animation = GetAbilityAnimation())
    {
        for (const auto& Notify : Animation->Notifies)
        {
            AbilityNotify = Cast<UDSAbilityAnimNotify>(Notify.Notify);
            if (AbilityNotify)
            {
                AbilityNotify->OnAbilityTriggered.BindDynamic(this, &ThisClass::Trigger);
                break;
            }
        }

        if (const auto SkeletalMeshComponent = AbilityUser->FindComponentByTag<USkeletalMeshComponent>(MainSkeletalMeshComponentTag))
        {
            AnimInstance = SkeletalMeshComponent->GetAnimInstance();
            if (AnimInstance)
            {
                AnimInstance->OnMontageEnded.AddDynamic(this, &ThisClass::MontageEnded);
                AnimInstance->Montage_Play(Animation);
            }
        }
    }
    else
    {
        Trigger();
        Finish(false);
    }

    return true;
}

EInterruptAbilityResult UDSAbilityBase::Interrupt()
{
    if (!bCanInterrupt) { return EInterruptAbilityResult::CantInterrupt; }

    if (AnimInstance && AnimInstance->IsAnyMontagePlaying())
    {
        constexpr float BlendOutTime = 0.3f;
        AnimInstance->Montage_Stop(BlendOutTime);
    }
    else
    {
        Finish(true);
    }

    return EInterruptAbilityResult::Interrupted;
}

bool UDSAbilityBase::IsActive() const
{
    return bIsActive && AbilityUser != nullptr;
}


void UDSAbilityBase::MontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
    Finish(bInterrupted);
}

void UDSAbilityBase::Trigger()
{
    //����� ������������ ������ ��������
    bCanInterrupt = false;

    OnTrigger();
    if (IsValid(GetAbilityTarget()) && GetAbilityTarget()->Implements<UDSAbilityTargetInterface>())
    {
        IDSAbilityTargetInterface::Execute_AbilityTriggered(GetAbilityTarget(), this, AbilityUser);
    }
    OnAbilityTriggered.Broadcast(this, AbilityUser, GetAbilityTarget());
}

void UDSAbilityBase::Finish(bool bInterrupted)
{
    OnFinish(bInterrupted);

    bIsActive = false;
    AbilityUser = nullptr;
    if (AbilityNotify) { AbilityNotify->OnAbilityTriggered.Clear(); }
    if (AnimInstance) { AnimInstance->OnMontageEnded.RemoveAll(this); }

    if (IsValid(GetAbilityTarget()) && GetAbilityTarget()->Implements<UDSAbilityTargetInterface>())
    {
        IDSAbilityTargetInterface::Execute_AbilityFinished(GetAbilityTarget(), this, AbilityUser, bInterrupted);
    }
    OnAbilityFinished.Broadcast(this, AbilityUser, GetAbilityTarget(), bInterrupted);

    ClearBindings();
    ConditionalBeginDestroy();
}

void UDSAbilityBase::OnStart_Implementation()
{
}

void UDSAbilityBase::OnTrigger_Implementation()
{
}

void UDSAbilityBase::OnFinish_Implementation(bool bInterrupted)
{
}

void UDSAbilityBase::ClearBindings()
{
    OnAbilityStarted.Clear();
    OnAbilityTriggered.Clear();
    OnAbilityFinished.Clear();
}
