// Created by Dmirtiy Chepurkin. All right reserved

#include "DSAbilityFunctionLibrary.h"
#include "DSAbilityBase.h"
#include "DSAbilityComponent.h"

bool UDSAbilityFunctionLibrary::StartAbility(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser, AActor* InAbilityTarget)
{
	return FinishStartingAbility(StartAbilityDeferred(InAbilityClass, InAbilityUser), InAbilityTarget);
}

UDSAbilityBase* UDSAbilityFunctionLibrary::StartAbilityDeferred(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser)
{
	if (!InAbilityUser) { return nullptr; }

	const auto AbilityComponent = GetAbilityComponent(InAbilityUser);
	return AbilityComponent ? AbilityComponent->SpawnAbility(InAbilityClass, InAbilityUser) : nullptr;
}

bool UDSAbilityFunctionLibrary::FinishStartingAbility(UDSAbilityBase* InAbility, AActor* InAbilityTarget)
{
	if (!InAbility) { return false; }

	const auto AbilityComponent = GetAbilityComponent(InAbility->GetUser());
	return AbilityComponent ? AbilityComponent->StartAbility(InAbility, InAbilityTarget) : false;
}

EInterruptAbilityResult UDSAbilityFunctionLibrary::InterruptAbility(AActor* InAbilityUser)
{
	const auto AbilityComponent = GetAbilityComponent(InAbilityUser);
	return AbilityComponent ? AbilityComponent->InterruptAbility() : EInterruptAbilityResult::NotActiveAbility;
}

UDSAbilityComponent* UDSAbilityFunctionLibrary::GetAbilityComponent(AActor* InActor)
{
	return InActor ? InActor->FindComponentByClass<UDSAbilityComponent>() : nullptr;
}
