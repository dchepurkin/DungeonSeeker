// Created by Dmirtiy Chepurkin. All right reserved

#include "DSRangeAbility.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSRangeAbility, All, All);

bool UDSRangeAbility::Start(AActor* InTarget, FText& FailMessage)
{
    if (!IsValid(InTarget))
    {
        FailMessage = GetNotValidTargetMessage();
        return false;
    }

    if (const auto Distance = GetMinDistance();
        !FMath::IsNearlyZero(Distance)
        && FVector::Distance(InTarget->GetActorLocation(), AbilityUser->GetActorLocation()) > GetMinDistance())
    {
        FailMessage = GetTargetTooFarMessage();
        return false;
    }

    return Super::Start(InTarget, FailMessage);
}
