// Created by Dmirtiy Chepurkin. All right reserved

#include "DSAbilityAnimNotify.h"

DEFINE_LOG_CATEGORY_STATIC(LogDSAbilityAnimNotify, All, All);

void UDSAbilityAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	Super::Notify(MeshComp, Animation, EventReference);

	OnAbilityTriggered.ExecuteIfBound();
}

FString UDSAbilityAnimNotify::GetNotifyName_Implementation() const
{
	return "Ability Notify";
}
