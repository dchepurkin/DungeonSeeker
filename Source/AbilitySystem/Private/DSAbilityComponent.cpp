// Created by Dmirtiy Chepurkin. All right reserved

#include "DSAbilityComponent.h"

#include "DSAbilityBase.h"
#include "DSAbilityConfig.h"

using namespace DS;

DEFINE_LOG_CATEGORY_STATIC(LogDSAbilityComponent, All, All);

UDSAbilityComponent::UDSAbilityComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UDSAbilityComponent::BeginPlay()
{
    Super::BeginPlay();

    //Add Tag to main skeletal mesh component (Used in DSAbilityBase)
    if (const auto Owner = GetOwner())
    {
        TArray<USkeletalMeshComponent*> SkeletalMeshComponents;
        Owner->GetComponents(USkeletalMeshComponent::StaticClass(), SkeletalMeshComponents);

        const auto FoundComponent = SkeletalMeshComponents.FindByPredicate([](USkeletalMeshComponent* SKComponent)
        {
            return SKComponent && SKComponent->GetAnimInstance();
        });

        if (FoundComponent)
        {
            (*FoundComponent)->ComponentTags.Add(MainSkeletalMeshComponentTag);
        }
    }
}

bool UDSAbilityComponent::IsActiveAbility() const
{
    return IsValid(CurrentAbility) && CurrentAbility->IsActive();
}

UDSAbilityBase* UDSAbilityComponent::SpawnAbility(const TSubclassOf<UDSAbilityBase> InAbilityClass, AActor* InAbilityUser)
{
    if (!InAbilityClass)
    {
        UE_LOG(LogDSAbilityComponent, Error, TEXT("Spawn ability with emty class"));
        return nullptr;
    }

    return NewObject<UDSAbilityBase>(InAbilityUser, InAbilityClass);
}

bool UDSAbilityComponent::StartAbility(UDSAbilityBase* InAbility, AActor* InAbilityTarget)
{
    if (!IsValid(InAbility) || IsActiveAbility()) { return false; }

    CurrentAbility = InAbility;
    CurrentAbility->OnAbilityFinished.AddDynamic(this, &ThisClass::OnAbilityFinished);

    return ExecuteCurrentAbility(InAbilityTarget);
}

EInterruptAbilityResult UDSAbilityComponent::InterruptAbility()
{
    if (!IsActiveAbility()) { return EInterruptAbilityResult::NotActiveAbility; }
    return CurrentAbility->Interrupt();
}

void UDSAbilityComponent::OnAbilityFinished(UDSAbilityBase* Ability, AActor* AbilityUser, AActor* AbilityTarget, bool bIsInterrupted)
{
    CurrentAbility = nullptr;
}

bool UDSAbilityComponent::ExecuteCurrentAbility(AActor* InAbilityTarget) const
{
    if (FText FailMessage; !CurrentAbility->Start(InAbilityTarget, FailMessage))
    {
        OnAbilityFailed.Broadcast(CurrentAbility, FailMessage);
        CurrentAbility->Interrupt();
        return false;
    }

    OnAbilityStarted.Broadcast(CurrentAbility, InAbilityTarget);
    return true;
}
