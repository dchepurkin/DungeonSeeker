//Created by Dmirtiy Chepurkin. All right reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class DungeonSeekerEditorTarget : TargetRules
{
	public DungeonSeekerEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_3;
		ExtraModuleNames.Add("DungeonSeeker");
	}
}
